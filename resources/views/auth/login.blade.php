@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
   <div class="login-page">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="login-inner">
                    <div class="card-header">{{ __('Login') }}</div>
                    <?php 
                    //  if(Session::get('error')){
                    //     echo Session::get('error');die;
                    // }
                    ?>
                    <div class="card-body">
                        <form method="POST" action="{{url('login')}}" autocomplete="off">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-12"> 
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{(Cookie::get('email'))?Cookie::get('email'):''}}" required  autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 

                            <div class="form-group row"> 
                                <label for="password" class="">{{ __('Password') }}</label>

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{(Cookie::get('password'))?Cookie::get('password'):''}}" required >

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row1">
                                <div class="">
                                    <div class="form-check">
                                        <input class="form-check-input" @if(Cookie::get('email')) checked @endif type="checkbox" name="remember_me" id="remember">
                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="">
                                    <button type="submit" class="btn btn-primary logBtn">
                                        {{ __('Login') }}
                                    </button>
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ url('forgot-password') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>

                            <div class="no-s">Don’t have an account? <a href="{{url('signup')}}">Signup</a></div>
                        </form>
                    </div>
                </div><!--login-inner-->
            </div>
        </div>
   </div><!--login-page-->
</div>
@endsection
