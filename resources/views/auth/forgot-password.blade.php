@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
   <div class="login-page">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="login-inner">
                    <div class="card-header">Forgot Password</div>
                    <div class="card-body">
                        <form method="POST" action="{{url('reset-password-mail')}}" autocomplete="off">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-12"> 
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required  autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div> 

                            <div class="form-group row mb-0">
                                <div class="">
                                    <button type="submit" class="btn btn-primary logBtn">
                                      Forgot
                                    </button>
                                </div>
                            </div>

                            <div class="no-s">Don’t have an account? <a href="{{url('login')}}">Login</a></div>
                        </form>
                    </div>
                </div><!--login-inner-->
            </div>
        </div>
   </div><!--login-page-->
</div>
@endsection
