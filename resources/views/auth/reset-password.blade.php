@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
   <div class="login-page">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="login-inner">
                    <div class="card-header">Reset Password</div>
                    <div class="card-body">
                        <form method="POST" action="{{url('reset-password')}}" autocomplete="off">
                            @csrf

                            <div class="form-group row">
                                    <label for="password" class="">{{ __('Password') }}</label>
                                    <input type="hidden" name="token" value="{{$user->reset_password_token}}">
                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter password" autocomplete="password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row"> 
                                    <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-12">
                                        <input id="confirm-password" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" placeholder="Enter confirm password" autocomplete="confirm-password">
                                        @error('confirm_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                            <div class="form-group row mb-0">
                                <div class="">
                                    <button type="submit" class="btn btn-primary logBtn">
                                      Reset
                                    </button>
                                </div>
                            </div>
   
                        </form>
                    </div>
                </div><!--login-inner-->
            </div>
        </div>
   </div><!--login-page-->
</div>
@endsection
