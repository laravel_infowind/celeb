@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
    <div class="login-page">
        <div class="justify-content-center">
            <div class="login-inner">
                <div class="col-md-12">
                    <div class="">
                        <div class="card-header">Signup</div>

                        <div class="card-body">
                            <form method="POST" action="{{ url('user-signup')}}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="">{{ __('Name') }}</label>

                                    <div class="col-md-12">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Enter name"  autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror 
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="">{{ __('E-Mail Address') }}</label>
                                    <div class="col-md-12">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Enter email address"  autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="">{{ __('Password') }}</label>

                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter password" autocomplete="password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row"> 
                                    <label for="password-confirm" class="">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-12">
                                        <input id="confirm-password" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" placeholder="Enter confirm password" autocomplete="confirm-password">
                                        @error('confirm_password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary logBtn">
                                            signup
                                        </button>
                                    </div>
                                </div>
                                <div class="no-s">Already have an account? <a href="{{url('login')}}">Login</a></div><!-- no-s -->
                            </form>
                        </div><!--card-body-->
                    </div>
                </div>
            </div><!--login-inner-->
        </div>
    </div><!--login-page-->
</div>
@endsection
