<div class="tab-pane fade in active" id="nav-dashboard" role="tabpanel" aria-labelledby="nav-dashboard-tab">
    <div class="card-body">
    <article>
    <h1>On Page Seo Tool for {{ Setting::get('site_name') }}</h1>
        <p class="mb-lg-5 mt-lg-3 lead">
            By using this tool you can easily about to insert on page seo tags like title, meta tags, scripts, webmaster
            validation, robot.txt file content. It also generate site map of your site with one click.
        </p>
    </article>

    <div class="row">
        <div class='col-md-4'>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-8">
                            <a href="{{route('seo::meta-tags.index')}}">Meta Tags</a>
                        </div>
                        <div class="col-sm-4 text-right">
                            <a href="{{route('seo::meta-tags.create')}}"> <i class="fa fa-plus"></i> Create </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <p class="card-text lead">
                        <span style="font-size: 60px"><b>{{isset($meta_tag_total)?$meta_tag_total: 0 }}</b></span>
                        Meta Tags
                    </p>
                </div>
            </div>
        </div>

    </div>
    <h3 class="mt-lg-3">Social Media</h3>
    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">Facebook</div>
                        <div class="col-sm-8 text-right">
                            <a href="https://developers.facebook.com/tools/debug">Validation</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">Twitter</div>
                        <div class="col-sm-8 text-right">
                            <a href=" https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started">Guide</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="card bg-light mt-lg-3">
        <div class="card-body">
            <p>Do not have knowledge about On Page SEO. Thats no problem. <a href="https://moz.com/learn/seo">
                    Here is an awesome tutorial series on moz.com</a>. That teaches you all
            </p>
        </div>

    </div>
    </div>
</div>