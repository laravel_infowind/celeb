@extends('layouts.admin')

@section('title', 'New Page')

@section('content-header', 'New Page')
@section('tools')
    <input type="submit" class="btn btn-primary" form="SeoFormPage" value="Save"/>

@endsection
@section('content')
    <div class="row">
        <div class='col-md-12'>
            <div class='panel panel-default'>
                <div class="panel-body">
                    @include('seo::forms.page',['showPageUrl'=>true])
                </div>
            </div>
        </div>
    </div>
@endSection