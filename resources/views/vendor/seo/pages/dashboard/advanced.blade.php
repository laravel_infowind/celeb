@extends('layouts.admin')
@section('title', 'Site Configuration')

@section('content-header', 'Site Configuration')
@section('content')

<style>
    
/*  streamview tab */
.s3_setting_section, .do_setting_section {
    display: none;
}
div.streamview-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  margin-top: 20px;
  margin-left: 50px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.streamview-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.streamview-tab-menu div.list-group{
  margin-bottom: 0;
}
div.streamview-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.streamview-tab-menu div.list-group>a .glyphicon,
div.streamview-tab-menu div.list-group>a .fa {
  color: #1e5780;
}
div.streamview-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.streamview-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.streamview-tab-menu div.list-group>a.active,
div.streamview-tab-menu div.list-group>a.active .glyphicon,
div.streamview-tab-menu div.list-group>a.active .fa{
  background-color: #1e5780;
  background-image: #1e5780;
  color: #ffffff;
}
div.streamview-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid #1e5780;
}

div.streamview-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
}

.box-body {
    padding: 0px;
}

div.streamview-tab div.streamview-tab-content:not(.active){
  display: none;
}

.sub-title {
    width: fit-content;
    color: #2c648c;
    font-size: 18px;
    padding-bottom: 5px;
}

hr {
    margin-top: 15px;
    margin-bottom: 15px;
}
.streamview-tab .card-body h1 {
    margin-top: 0;
    font-size: 20px;
    color: #8a1818;
    font-weight: 700;
    text-transform: uppercase;
}
</style>


<div class="row">
    <div class="col-md-12"></div>
    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 streamview-tab-container row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 streamview-tab-menu">
            <div class="list-group">
                <a class="list-group-item text-left active" id="nav-social-tab" data-toggle="tab" href="#nav-robot-txt" role="tab"
                aria-controls="nav-social" aria-selected="true"> Robots.txt </a>
                <a class="list-group-item text-left" id="nav-htaccess-tab" data-toggle="tab" href="#nav-htaccess" role="tab"
                aria-controls="nav-facebook" aria-selected="false"> .htaccess </a>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 streamview-tab">
            <div class="tab-content mt-3" id="nav-tabContent">
                @include('seo::tabs.robot')
                <div class="tab-pane fade" id="nav-htaccess" role="tabpanel" aria-labelledby="nav-htaccess-tab">
                    @include('seo::tabs.htaccess')
                </div>
            </div>
        </div>
    </div>
</div>
@endSection