@extends('layouts.admin')

@section('title', tr('add_video'))
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/wizard.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/jquery.jbswizard.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet"
    href="{{asset('admin-css/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/imgareaselect-default.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/jquery.awesome-cropper.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .container-narrow {
        margin: 150px auto 50px auto;
        max-width: 728px;
    }

    canvas {
        width: 100%;
        height: auto;
    }

    span.select2-container {
        width: 100% !important;
    }

    .example-wizard.panel {
        border: 0px solid white;
        padding: 20px;
        display: inline-block;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        margin-bottom: 10px;
        width: 30%;
    }

    .select2-container .select2-search--inline {
        border: 1px solid #d2d6df !important;
        width: 30%;
    }
</style>
@endsection
@section('content')
<div class="main-content">
    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="display: none"
        id="error_popup">popup</button>
    <!-- popup -->
    <div class="modal fade error-popup" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="media">
                        <div class="media-left">
                            <img src="{{asset('images/warning.jpg')}}" class="media-object" style="width:60px">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Information</h4>
                            <p id="error_messages_text"></p>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary top" data-dismiss="modal">Okay</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="example">
        <div class="example-wizard panel-primary">
            <div class="">
                <!-- Example Wizard START -->
                <div id="j-bs-wizard-example">
                    <form method="post" enctype="multipart/form-data" id="save_people_form"
                        action="{{route('admin.videos.save')}}">
                        <input type="hidden" name="person_id" id="person_id" value="">
                        <input type="hidden" name="profile_pic" id="celeb_profile_pic">
                        <input type="hidden" name="gender" id="celeb_gender">
                        <input type="hidden" name="popularity" id="celeb_popularity">
                        <input type="hidden" name="adult" id="celeb_adult">
                        <input type="hidden" name="imdb_id" id="celeb_imdb_id">
                        <input type="hidden" name="cast_credits" id="cast_credits">
                        <input type="hidden" name="crew_credits" id="crew_credits">
                        <input type="hidden" name="homepage" id="homepage">
                        <input type="hidden" name="deathday" id="deathday">

                        <!-- tab1 -->
                        <div class="row">
                            <div class="col-md-8">

                                <div class="boxx">
                                    <div class="box-bodyy">
                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="title">{{tr('name')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span> </label>
                                            </div>
                                            <div class="card-body">
                                                <div class="publish_card">
                                                    <div class="publish">
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" class="search-type" checked id="tmdb"
                                                                value="tmdb" name="tmdb">
                                                            <label for="tmdb" onclick="checkType('tmdb')">
                                                                {{tr('tmdb')}} </label>
                                                        </div>
                                                        <div class="radio radio-primary radio-inline">
                                                            <input type="radio" class="search-type form-control"
                                                                onclick="checkType('custom')" id="custom" value="custom"
                                                                name="tmdb">
                                                            <label for="custom"> {{tr('custom')}} </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="publish_card">
                                                    <input class="form-control"
                                                        style="background-color: #fff !important;" required type="text"
                                                        name="people" value="" id='people' autocomplete="off">

                                                    <div id="tmdb_btn">
                                                        <button style="margin-top:5px; height: 34px; border-radius:0"
                                                            title="Click to search on {{tr('tmdb')}}" type="button"
                                                            class="btn btn-primary btn-sm" id="seacrh_tmdb"
                                                            onclick="getCelebrityList()">Search
                                                            {{tr('tmdb')}}</button>
                                                    </div>

                                                </div>


                                            </div>
                                        </div>


                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="description">{{tr('description')}} <span class="asterisk"><i
                                                            class="fa fa-asterisk"></i></span></label>
                                            </div>
                                            <div class="card-body">
                                                <div class="detail_description_editor">
                                                    <textarea class="overview" id="biography" name="biography" rows="4"
                                                        required=""></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <script src="{{ url('ckeditor-full/ckeditor.js')}}"></script>
                                        <script>
                                            CKEDITOR.replace( 'biography' , {
                                                extraPlugins: 'colorbutton,colordialog',
                                                allowedContent: true
                                            }); 
                                        </script>

                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="gender">{{tr('gender')}}</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="">
                                                    <input id="male_radio" type="radio" value="2" name="gender" /> Male
                                                    <input id="female_radio" type="radio" value="1" name="gender" />
                                                    Female
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="celeb_birthday">Birthday</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="publish_card mb-20">
                                                    <input required="" type="date" class="form-control"
                                                        id="celeb_birthday" name="celeb_birthday" value="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="celeb_place_of_birth">Place of Birth</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="publish_card mb-20">
                                                    <input class="form-control" type="text" name="celeb_place_of_birth"
                                                        value="" id='celeb_place_of_birth' required="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="department">Department</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="publish_card mb-20">
                                                    <input class="form-control" type="text" name="known_for_department"
                                                        value="" id='known_for_department' required="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card mb-20">
                                            <div class="card-header">
                                                <label for="alternative_titles">Also Known As</label>
                                            </div>
                                            <div class="card-body">
                                                <div class="detail_description_editor">
                                                    <textarea id="celeb_known_as" class="form-control celeb_known_as"
                                                        name="celeb_known_as" rows="4"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label for="">Publish</label>
                                    </div>
                                    <div class="card-body">
                                        <div class="publish_card">
                                            {!! App\Helpers\Helper::published_block($model) !!}

                                            @if($model->id)
                                            <div class="publish_item mb-20">
                                                <label>Modified on: <span
                                                        class="published_on">{{ date('Y-m-d', strtotime($model->updated_at)). ' at '. date('H:i', strtotime($model->updated_at)) }}</span></label>
                                            </div>
                                            <div class="publish_item mb-20">
                                                <label>Created on: <span
                                                        class="published_on">{{ date('Y-m-d', strtotime($model->created_at)). ' at '. date('H:i', strtotime($model->created_at)) }}</span></label>
                                            </div>

                                            @endif
                                        </div>


                                    </div>
                                    <div class="card-footer">
                                        <div class="publish_item">
                                            <div class="box-tools pull-left" style="width: 30%">
                                                @if(Setting::get('admin_delete_control') == 1)
                                                <button style="width: 100%" disabled
                                                    class="btn  btn-primary save_people" type="button" id="save_people">

                                                    {{ (!$model->id) ? 'Publish' : 'Update' }}</button>
                                                @else
                                                <button class="btn  btn-primary save_people" type="button"
                                                    id="save_people">{{ (!$model->id) ? 'Publish' : 'Update' }}</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label for="">Upload Profile Image <span class="asterisk"><i
                                                    class="fa fa-asterisk"></i></span></label>
                                    </div>
                                    <div class="card-body">
                                        <div class="image-upload">
                                            <div class="image-upload-item">
                                                <input type="hidden" name="poster" id="poster">
                                                <input type="file" id="default_image"
                                                    accept="image/png, image/jpeg, image/jpg" name="default_image"
                                                    placeholder="{{tr('profile_image')}}" style="display:none"
                                                    onchange="loadFile(this,'default_img')">
                                                <input type="hidden" name="old_profile_image" id="old_profile_image"
                                                    value="{{ $model->profile_image }}">
                                                <img src="{{$model->profile_image ? $model->profile_image : asset('images/default.png')}}"
                                                    onclick="$('#default_image').click();return false;" id="default_img"
                                                    style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="overlay">
    <div id="loading-img"></div>
</div>

<div class="modal fade" id="data-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Peoples</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumbnail</th>
                            <th>Celebrity Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="celebrity-data">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="">
                    <button class="tmdb_btn_prev pagination_btn btn btn-primary" onclick="getCelebrityList('prev')"
                        prev_page="0">Prev</button>
                    <button class="tmdb_btn_next pagination_btn btn btn-primary" onclick="getCelebrityList('next')"
                        next_page="2">Next</button>
                    <button type="button" class="pagination_btn btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('scripts')
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/moment.min.js')}}"></script>
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    var page = 1;
    var total_page;
    var TMDB_KEY = "{{env('TMDB_API_KEY')}}";
    
    function checkType(type) {
        if (type == 'custom') {
            $('#tmdb_btn').hide();
        } else if (type == 'tmdb') {
            $('#tmdb_btn').show();
        }
    }
    
    function getCelebrityList(type) {
    
        if (!$('#people').val()) {
            $('#people').closest('span').addClass('help-block error-help-block alert-danger').html('The field is required.');
        }
    
        if (type == 'prev') {
            page = page - 1;
        } else if (type == 'next') {
            page = page + 1;
        }
    
        var celeb_name = $('#people').val();
        if (celeb_name != '') {
            $('#seacrh_tmdb').prop('disabled', true);
            $('#seacrh_tmdb').text('Searching...');
            $.ajax({
                type: 'get',
                url: "https://api.themoviedb.org/3/search/person?api_key=" + TMDB_KEY + "&query=" + celeb_name + "&page=" + page,
                dataType: 'json',
                success: function(data) {
                    total_page = data.total_pages;
                    if (page == 1) {
                        $('.tmdb_btn_prev').prop('disabled', true);
                    } else {
                        $('.tmdb_btn_prev').prop('disabled', false);
                    }
                    if (page == total_page) {
                        $('.tmdb_btn_next').prop('disabled', true);
                    } else {
                        $('.tmdb_btn_next').prop('disabled', false);
                    }
                    var str = '';
                    if (data.results.length > 0) {
                        for (var i = 0; i < data.results.length; i++) {
                            if (data.results[i].profile_path) {
                                var image = "https://image.tmdb.org/t/p/w300" + data.results[i].profile_path;
                            } else {
                                var image = "{{url('images/default.png')}}";
                            }
                            str = str + "<tr><td>" + ((20 * (page - 1)) + (i + 1)) + "</td>" +
                                "<td><img class='img-thumbnail' src='" + image + "' width='60' height='60'></td>" +
                                "<td>" + data.results[i].name + "</td>" +
                                "<td><button type='button' onclick = 'getCelebrityDetail(" + data.results[i].id + ")' id='38646' class='btn get-movie-data btn-sm btn-primary'>Select</button></td></tr>";
                        }
                    } else {
                        str = str + "<tr>" +
                            "<td colspan='4'>'No Celebrity Found'</td></tr>";
    
                    }
    
                    $('#celebrity-data').html(str);
                    console.log(data)
                    $('#data-modal').modal('show');
                    $("#data-modal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#seacrh_tmdb').prop('disabled', false);
                    $('#seacrh_tmdb').text('Search');
                }
            });
        }
    }
    
    function getCelebrityDetail(celebrityId) {
        $('.close').trigger('click');
        $.ajax({
            type: 'get',
            url: "https://api.themoviedb.org/3/person/" + celebrityId + "?api_key=" + TMDB_KEY + "&language=en-US&append_to_response=combined_credits",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                $('#person_id').val(data.id);
                $('#people').val(data.name);
                $('#celeb_birthday').val(data.birthday);
                $('#celeb_place_of_birth').val(data.place_of_birth);
                $('textarea#biography').val(data.biography.replace(/\n/g, "<br/>"));
                CKEDITOR.instances.biography.setData(data.biography.replace(/\n/g, "<br/>"));
                $('#known_for_department').val(data.known_for_department);
                $('#celeb_gender').val(data.gender);
                $('#celeb_popularity').val(data.popularity);
                $('#celeb_imdb_id').val(data.imdb_id);
                $('#celeb_adult').val(data.adult);
                $('#celeb_profile_pic').val(data.profile_path);
                $('#homepage').val(data.homepage);
                $('#deathday').val(data.deathday);
                if (data.gender == 2) {
                    $("#male_radio").attr('checked', true);
                } else if (data.gender == 1) {
                    $("#female_radio").attr('checked', true);
                }
                if (data.also_known_as.length) {
                    var aka = '';
                    $.each(data.also_known_as, function(i, name) {
                        aka += name + ', ';
                    });
                    aka = aka.replace(/,\s*$/, "");
                    $('#celeb_known_as').val(aka);
                } else {
                    $('#celeb_known_as').val('');
                }
    
                if (data.profile_path) {
                    var image = "https://image.tmdb.org/t/p/w300" + data.profile_path;
                } else {
                    var image = "{{url('images/default.png')}}";
                }
    
                if (data.profile_path) {
                    $('#poster').val(image);
                    $('img#default_img').attr('src', image);
                } else {
                    $('img#default_img').attr('src', '{{url("images/default.png")}}');
                    $('#poster').val('');
                }
                $('.error-help-block').hide();

                // load cast crew
                if (data.combined_credits.cast.length > 0) {
                    var cast = JSON.stringify(data.combined_credits.cast);
                    // $.each(data.combined_credits.cast, function(i, actor) {
                    //     if (i == 0)
                    //         actors += actor.id + ':' + actor.name;
                    //     else
                    //         actors += '|' + actor.id + ':' + actor.name;
                    // });
                    $('#cast_credits').val(cast);
                } else {
                    $('#cast_credits').val('');
                }

                if (data.combined_credits.crew.length > 0) {
                    var crew = '';
                    var cast = JSON.stringify(data.combined_credits.crew);
                    // $.each(data.combined_credits.crew, function(i, actor) {
                    //     if (i == 0)
                    //         actors += actor.id + ':' + actor.name;
                    //     else
                    //         actors += '|' + actor.id + ':' + actor.name;
                    // });
                    $('#crew_credits').val(crew);
                } else {
                    $('#crew_credits').val('');
                }
    
            }
        });
    }
    
    // onchange default image poster will be blank
    $('#default_image').on('change', function() {
        $('#poster').val('');
    });
    
    $("#profile").change(function() {
        readURL(this);
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.profile-user-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    
    function removeFile(index) {
        console.log(index);
        delete uploadedFiles[index];
        $('#file_' + index).remove();
    }
    // published on timestamp
    $(document).ready(function() {
        $(document).on('click', '.edit-timestamp', function() {
            $(this).hide();
            $('#timestampdiv').slideDown('fast');
        });
    
        $(document).on('click', '.cancel-timestamp', function() {
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    
        $(document).on('click', '.save-timestamp', function() {
            var today_date = new Date();
            var aa = $('#aa').val(),
                mm = $('#mm').val(),
                jj = $('#jj').val(),
                hh = $('#hh').val(),
                mn = $('#mn').val(),
                newD = new Date(aa, mm - 1, jj, hh, mn);
    
            event.preventDefault();
    
            if (newD.getFullYear() != aa || (1 + newD.getMonth()) != mm || newD.getDate() != jj || newD.getMinutes() != mn) {
                $('.timestamp-wrap').addClass('form-invalid');
                return;
            } else {
                $('.timestamp-wrap').removeClass('form-invalid');
            }
    
            var month = $('#mm option[value="' + mm + '"]').attr('data-text');
            var date = parseInt(jj, 10);
            var year = aa;
            var hour = ('00' + hh).slice(-2);
            var min = ('00' + mn).slice(-2);
    
            var date_string = month + ' ' + date + ',' + ' ' + year + ' at ' + hour + ':' + min;
            var publish_time = year + '-' + (1 + newD.getMonth()) + '-' + date + ' ' + hour + ':' + min + ':00';
            if (today_date >= newD) {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(1);
                $('#timestamp').html('<label>Published on:' + ' <span class="published_on">' + date_string + '</span></label>');
            } else {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(2);
                $('#timestamp').html('<label>Scheduled for:' + ' <span class="published_on">' + date_string + '</span></label>');
            }
    
            // Move focus back to the Edit link.
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    });
    
    $(".save_people").on('click', function(e) {
        e.preventDefault();
        var err = '';
    
        var person_id = $("#person_id").val();
        var name = $("#people").val();
        var poster = $("#poster").val();
        var known_for_department = $("#known_for_department").val();
        //var celeb_place_of_birth = $("#celeb_place_of_birth").val();
        //var celeb_birthday = $("#celeb_birthday").val();
        var biography = CKEDITOR.instances.biography.getData();
        var default_image = document.getElementById("default_image").files.length;
    
        if (name == '') {
            err = "Name should not be blank";
        }
    
        if (biography == '' && err == '') {
            err = "Description should not be blank";
        }
    
        if (known_for_department == '' && err == '') {
            err = "Department should not be blank";
        }
    
        if (!person_id || person_id <= 0 || person_id == null) {
            if (default_image <= 0 && err == '' && poster == '') {
    
                err = "Please Choose Default Image.";
    
            }
        }
    
        if (err) {
    
            $("#error_messages_text").html(err);
    
            $("#error_popup").click();
    
            return false;
        }
    
        var image = true;
    
        if (!err && biography && image) {
            var formdata = new FormData($('#save_people_form')[0]);
            formdata.append('biography', biography);
            formdata.append('profile', $('#profile').val());
            formdata.append('default_image', default_image);
            $('.save_people').prop('disabled', true);
            $('#add_celebrity_loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                async: false,
                type: 'post',
                processData: false,
                contentType: false,
                url: "{{url('admin/people/save')}}",
                data: formdata,
                dataType: 'json',
                success: function(data) {
                    $('.save_people').prop('disabled', false);
                    if (data.success) {
                        //toastr.success(data.message);
                        //$('#save_people').reset();
                        setTimeout(function() {
                            window.location = "{{url('admin/celebs/create')}}"
                        }, 1000)
                    } else {
                        //toastr.error(data.message);
                    }
                    // $('#add_celebrity_loader').hide();
                },
                error: function() {
                    $('.save_people').prop('disabled', false);
                }
            });
        }
    });
    
    function loadFile(event, id) {
        // alert(event.files[0]);
        var reader = new FileReader();
    
        reader.onload = function() {
            var output = document.getElementById(id);
            // alert(output);
            output.src = reader.result;
            //$("#imagePreview").css("background-image", "url("+this.result+")");
        };
        reader.readAsDataURL(event.files[0]);
    } 
</script>
@endsection