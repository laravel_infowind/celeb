@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet"
    href="{{asset('admin-css/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .container-narrow {
        margin: 150px auto 50px auto;
        max-width: 728px;
    }

    .progress-bar {
        background-color: #5cb85c !important;
    }

    canvas {
        width: 100%;
        height: auto;
    }

    span.select2-container {
        width: 100% !important;
    }

    .example-wizard.panel {
        border: 0px solid white;
        padding: 20px;
        display: inline-block;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        margin-bottom: 10px;
        width: 30%;
    }

    .select2-container .select2-search--inline {
        border: 1px solid #d2d6df !important;
        width: 30%;
    }
</style>
@endsection
<!-- popup -->
<div class="modal fade error-popup" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="media">
                    <div class="media-left">
                        <img src="{{asset('images/warning.jpg')}}" class="media-object" style="width:60px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Information</h4>
                        <p id="error_messages_text"></p>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-primary top" data-dismiss="modal">Okay</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="errors"></div>
<div class="main-content">
    <form id="uploadForm" class="form-horizontal" action="{{route('admin.content.save')}}" method="POST" enctype="multipart/form-data" role="form">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
        <input type="hidden" name="id" value="{{$model->id}}">
        <input type="hidden" name="person_id" id="person_id" value="{{ ($model->id) ? $model->cast_id : 0 }}">
        <input type="hidden" name="profile_pic" id="celeb_profile_pic">
        <input type="hidden" name="gender" id="celeb_gender" value="{{ ($model->id) ? $model->gender : '' }}">
        @if(!$model->id)
        <input type="hidden" name="popularity" id="celeb_popularity">
        <input type="hidden" name="adult" id="celeb_adult">
        <input type="hidden" name="imdb_id" id="celeb_imdb_id">
        <input type="hidden" name="cast_credits" id="cast_credits">
        <input type="hidden" name="crew_credits" id="crew_credits">
        <input type="hidden" name="homepage" id="homepage">
        <input type="hidden" name="deathday" id="deathday">
        @endif
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('name')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span> </label>
                            </div>
                            <div class="card-body">
                                @if(!$model->id)
                                <div class="publish_card">
                                    <div class="publish">
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" class="search-type" checked id="tmdb"
                                                value="tmdb" name="tmdb">
                                            <label for="tmdb" onclick="checkType('tmdb')">
                                                {{tr('tmdb')}} </label>
                                        </div>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" class="search-type form-control"
                                                onclick="checkType('custom')" id="custom" value="custom" name="tmdb">
                                            <label for="custom"> {{tr('custom')}} </label>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                
                                <div class="publish_card">
                                    <input class="form-control" style="background-color: #fff !important;" required type="text" name="name" value="{{$model->name}}" id='people' autocomplete="off">

                                    @if(!$model->id)
                                    <div id="tmdb_btn">
                                        <button style="margin-top:5px; height: 34px; border-radius:0"
                                            title="Click to search on {{tr('tmdb')}}" type="button"
                                            class="btn btn-primary btn-sm" id="seacrh_tmdb"
                                            onclick="getCelebrityList()">Search
                                            {{tr('tmdb')}}</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="description">Position <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                            </div>
                            <div class="card-body">
                                <div class="position">
                                    <select id="position" name="known_for_department" class="form-control">
                                        <option value="Acting" @if($model->known_for_department=='Acting') selected @endif >Acting</option>
                                        <option value="Directing" @if($model->known_for_department=='Directing') selected @endif >Directing</option>
                                        <option value="Writing" @if($model->known_for_department=='Writing') selected @endif >Writing</option>
                                        <option value="Production" @if($model->known_for_department=='Production') selected @endif >Production</option>
                                        <option value="Sound" @if($model->known_for_department=='Sound') selected @endif >Sound</option>
                                        <option value="Crew" @if($model->known_for_department=='Crew') selected @endif >Crew</option>
                                        <option value="Creator" @if($model->known_for_department=='Creator') selected @endif >Creator</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="description">{{tr('description')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                            </div>
                            <div class="card-body">
                                <div class="detail_description_editor">
                                    <textarea class="overview" id="biography" name="biography" rows="4" required="">{!! (!empty($model->biography)) ? $model->biography : '' !!}</textarea>
                                </div>
                            </div>
                        </div>
                        <script src="{{ url('ckeditor-full/ckeditor.js')}}"></script>
                        <script>
                            CKEDITOR.replace( 'biography' , {
                                extraPlugins: 'colorbutton,colordialog',
                                allowedContent: true
                            }); 
                        </script>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="gender">{{tr('gender')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="">
                                    <input id="male_radio" type="radio" value="2" name="gender" {{ ((!empty($model->gender)) && $model->gender == 2) ? 'checked' : '' }} onchange="changeGender(this.value)"/> Male
                                    <input id="female_radio" type="radio" value="1" name="gender" {{ ((!empty($model->gender)) && $model->gender == 1) ? 'checked' : '' }} onchange="changeGender(this.value)"/>
                                    Female
                                </div>
                            </div>
                        </div>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="celeb_birthday">Birthday</label>
                            </div>
                            <div class="card-body">
                                <div class="publish_card mb-20">
                                    <input required="" type="date" class="form-control"
                                        id="celeb_birthday" name="celeb_birthday" value="{{ (!empty($model->birthday)) ?  $model->birthday : '' }}">
                                </div>
                            </div>
                        </div>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="celeb_place_of_birth">Place of Birth</label>
                            </div>
                            <div class="card-body">
                                <div class="publish_card mb-20">
                                    <input class="form-control" type="text" name="celeb_place_of_birth"
                                        value="{{ (!empty($model->place_of_birth)) ?  $model->place_of_birth : '' }}" id='celeb_place_of_birth' required="">
                                </div>
                            </div>
                        </div>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="alternative_titles">Also Known As</label>
                            </div>
                            <div class="card-body">
                                <div class="detail_description_editor">
                                    <textarea id="celeb_known_as" class="form-control celeb_known_as"
                                        name="celeb_known_as" rows="4"><?php if(!empty($model->also_known_as) && $model->id) {
                                        $aka_string = '';
                                        if (isJson($model->also_known_as)) {
                                                $json_aka = json_decode($model->also_known_as, true);
                                                if (!empty($json_aka)) {
                                                    foreach ($json_aka as $key => $value) {
                                                        $aka_string .= $value.', ';
                                                    }
                                                } else {
                                                    $aka_string = '';
                                                }
                                            } else {
                                                $aka_string = $model->also_known_as;
                                            }
                                        echo rtrim($aka_string, ', ');
                                    }?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab4 -->
                @include('admin.people_content._upload')
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <div class="publish_item">
                                                
                                <div style="padding-left:0px; padding-right:0px; width: 100%; margin-left: 3px; margin-top: 9px;" class="progress col-sm-12 col-md-4 col-lg-4"
                                    style="margin-top: 8px;">
                                    <div class="progress-bar">0 %</div>
                                </div>
                                
                            </div>
                            <div class="publish_item mb-20">
                                <label>Moderator <span class="asterisk"><i
                                            class="fa fa-asterisk"></i></span></label>
                                <select class="form-control" required name="moderator_id">
                                    <option value=""></option>
                                    @foreach($moderators as $mod)
                                    <option {{ ($mod->id == $model->moderator_id ) ? 'selected' : '' }}
                                        value="{{ $mod->id }}">{{ $mod->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            {!! App\Helpers\Helper::published_block($model) !!}

                            @if($model->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($model->updated_at)). ' at '. date('H:i', strtotime($model->updated_at)) }}</span></label>
                                </div>
                                <div class="publish_item mb-20">
                                    <label>Created on: <span class="published_on">{{ date('Y-m-d', strtotime($model->created_at)). ' at '. date('H:i', strtotime($model->created_at)) }}</span></label> 
                                </div>
                            
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a id="finish_video" href="#" class="btn btn-primary" disabled>{{ (!$model->id) ? 'Publish' : 'Update' }}</a>
                                @else
                                    <button id="finish_video" type="submit" class="btn btn-primary">{{ (!$model->id) ? 'Publish' : 'Update' }}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label>Vote</label>
                    </div>
                    <div class="card-body">
                        <input type="hidden" id="vote_average" name="vote_average" value="{{($model->vote_average) ? $model->vote_average : 0}}"/>
                        <input type="hidden" id="vote_count" name="vote_count" value="{{($model->vote_count) ? $model->vote_count : 0}}"/>
                        <input type="hidden" id="vote_total" name="vote_total" value="{{($model->vote_rating_total) ? $model->vote_rating_total : 0}}"/>

                        
                        <div class="publish_card mb-20">
                            <label for="vote_average_input">Vote Average: <span class="vavg span-vote-value">{{($model->vote_average) ? $model->vote_average : 0}}</span></label>

                            <span id="edit-vavg-buttons">
                                <button type="button" class="edit-vavg btn edit_v_btn">Edit</button>
                            </span>
                            <div class="wrap_vavg" style="display:none">
                                
                                <input class="form-control" type="text" name="vote_average_input"
                                value="{{($model->vote_average) ? $model->vote_average : 0}}" id='vote_average_input' required="">

                                <p>
                                    <a href="javascript:void(0)" class="save-vavg hide-if-no-js button btn">OK</a>
                                    <a href="javascript:void(0)" class="cancel-vavg hide-if-no-js button-cancel">Cancel</a>
                                </p>

                            </div>
                        </div>

                        <div class="publish_card mb-20">
                            <label for="vote_count_input">Vote Count: <span class="vote_count span-vote-value">{{($model->vote_count) ? $model->vote_count : 0}}</span></label>

                            <span id="edit-vcount-buttons">
                                <button type="button" class="edit-vcount btn edit_v_btn">Edit</button>
                            </span>
                            <div class="wrap_vote_count" style="display:none">
                                
                                <input class="form-control" type="text" name="vote_count_input"
                                value="{{($model->vote_count) ? $model->vote_count : 0}}" id='vote_count_input' required="">

                                <p>
                                    <a href="javascript:void(0)" class="save-vcount hide-if-no-js button btn">OK</a>
                                    <a href="javascript:void(0)" class="cancel-vcount hide-if-no-js button-cancel">Cancel</a>
                                </p>

                            </div>

                        </div>

                    
                        <div class="publish_card mb-20">
                            <label for="vote_rating_total">Vote Total:  <span class="vote_rating_total span-vote-value">{{($model->vote_rating_total) ? $model->vote_rating_total : 0}}</span></label>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label>{{tr('category')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <select class="form-control" onchange="displaySubCategory(this.value);"
                                name="category_id" id="category_id" required="">
                                @foreach($categories as $category)
                                    <option cat_slug="{{ $category->category_slug }}" {{ ($category->id == $model->category_id) ? 'selected' : '' }}
                                    value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label>Sub-Categories <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <select multiple class="form-control select2" name="sub_category_id[]"
                                id="sub_category">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label>Tags</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <div class="tags_wrap">
                                <?php $selected_tags = App\Helpers\Helper::getSelectedTags($model->id); ?>
                            <textarea name="tags_input" rows="3" cols="20" class="the-tags hide form-control" id="tax-input-post_tag" aria-describedby="new-tag-post_tag-desc" spellcheck="false">{{ $selected_tags }}</textarea>

                                <input class="form-control" type="text" name="tags[]" value="" id="tag_id"/> 
                                <a href="javascript:void(0)" class="add-tag-btn btn" role="button"><span aria-hidden="true">Add</span></a>
                            </div>
                            <ul class="tagchecklist" role="list">
                                
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Upload Image</label>
                    </div>
                    <div class="card-body">
                        <div class="image-upload">
                            <div class="image-upload-item">
                                <input type="hidden" name="images_val" id="images_val"/>
                                <div class="image-upload-item">
                                    <input type="hidden" name="poster" id="poster">
                                    <input type="file" id="default_image" accept="image/png, image/jpeg, image/jpg" name="default_image" placeholder="{{tr('profile_image')}}" style="display:none"
                                        onchange="loadFile(this,'default_img')">
                                    <input type="hidden" name="old_profile_image" id="old_profile_image" value="{{ ($model->id) ? $model->profile_path : '' }}">
                                    
                                    <img src="{{($model->id && $model->profile_path) ? $model->profile_path : url('images/default.png')}}"
                                        onclick="$('#default_image').click();return false;" id="default_img"
                                        style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">

                                </div>

                                <p class="help-block">{{tr('image_validate')}} {{tr('image_square')}}</p>
                        
                            </div>
                            
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </form>

</div>


<div class="overlay">
    <div id="loading-img"></div>
</div>

<div class="modal fade" id="data-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Peoples</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumbnail</th>
                            <th>Celebrity Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="celebrity-data">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="">
                    <button class="tmdb_btn_prev pagination_btn btn btn-primary" onclick="getCelebrityList('prev')"
                        prev_page="0">Prev</button>
                    <button class="tmdb_btn_next pagination_btn btn btn-primary" onclick="getCelebrityList('next')"
                        next_page="2">Next</button>
                    <button type="button" class="pagination_btn btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
@section('scripts')
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/moment.min.js')}}"></script>
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    var page = 1;
    var total_page;
    var TMDB_KEY = "{{env('TMDB_API_KEY')}}";
    
    function checkType(type) {
        if (type == 'custom') {
            $('#tmdb_btn').hide();
        } else if (type == 'tmdb') {
            $('#tmdb_btn').show();
        }
    }

    function changeGender(value) {
        if (value == '1') {
            $('#celeb_gender').val(value);
        } else if (value == '2') {
            $('#celeb_gender').val(value);
        }
    }
    
    function getCelebrityList(type) {
    
        if (!$('#people').val()) {
            $('#people').closest('span').addClass('help-block error-help-block alert-danger').html('The field is required.');
        }
    
        if (type == 'prev') {
            page = page - 1;
        } else if (type == 'next') {
            page = page + 1;
        }
    
        var celeb_name = $('#people').val();
        if (celeb_name != '') {
            $('#seacrh_tmdb').prop('disabled', true);
            $('#seacrh_tmdb').text('Searching...');
            $.ajax({
                type: 'get',
                url: "https://api.themoviedb.org/3/search/person?api_key=" + TMDB_KEY + "&query=" + celeb_name + "&page=" + page,
                dataType: 'json',
                success: function(data) {
                    total_page = data.total_pages;
                    if (page == 1) {
                        $('.tmdb_btn_prev').prop('disabled', true);
                    } else {
                        $('.tmdb_btn_prev').prop('disabled', false);
                    }
                    if (page == total_page) {
                        $('.tmdb_btn_next').prop('disabled', true);
                    } else {
                        $('.tmdb_btn_next').prop('disabled', false);
                    }
                    var str = '';
                    if (data.results.length > 0) {
                        for (var i = 0; i < data.results.length; i++) {
                            if (data.results[i].profile_path) {
                                var image = "https://image.tmdb.org/t/p/w300" + data.results[i].profile_path;
                            } else {
                                var image = "{{url('images/default.png')}}";
                            }
                            str = str + "<tr><td>" + ((20 * (page - 1)) + (i + 1)) + "</td>" +
                                "<td><img class='img-thumbnail' src='" + image + "' width='60' height='60'></td>" +
                                "<td>" + data.results[i].name + "</td>" +
                                "<td><button type='button' onclick = 'getCelebrityDetail(" + data.results[i].id + ")' id='38646' class='btn get-movie-data btn-sm btn-primary'>Select</button></td></tr>";
                        }
                    } else {
                        str = str + "<tr>" +
                            "<td colspan='4'>'No Celebrity Found'</td></tr>";
    
                    }
    
                    $('#celebrity-data').html(str);
                    console.log(data)
                    $('#data-modal').modal('show');
                    $("#data-modal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#seacrh_tmdb').prop('disabled', false);
                    $('#seacrh_tmdb').text('Search');
                }
            });
        }
    }
    
    function getCelebrityDetail(celebrityId) {
        $('.close').trigger('click');
        $.ajax({
            type: 'get',
            url: "https://api.themoviedb.org/3/person/" + celebrityId + "?api_key=" + TMDB_KEY + "&language=en-US&append_to_response=combined_credits",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                var department = data.known_for_department;
                $('#person_id').val(data.id);
                $('#people').val(data.name);
                $('#celeb_birthday').val(data.birthday);
                $('#celeb_place_of_birth').val(data.place_of_birth);
                $('textarea#biography').val(data.biography.replace(/\n/g, "<br/>"));
                CKEDITOR.instances.biography.setData(data.biography.replace(/\n/g, "<br/>"));
                $('#known_for_department').val(data.known_for_department);
                $('#celeb_gender').val(data.gender);
                $('#celeb_popularity').val(data.popularity);
                $('#celeb_imdb_id').val(data.imdb_id);
                $('#celeb_adult').val(data.adult);
                $('#celeb_profile_pic').val(data.profile_path);
                $('#homepage').val(data.homepage);
                $('#deathday').val(data.deathday);

                if (department == 'Acting') {
                    $("#position>option:eq(0)").prop('selected', true);
                } else if (department == 'Directing') {
                    $("#position>option:eq(1)").prop('selected', true);
                } else if (department == 'Writing') {
                    $("#position>option:eq(2)").prop('selected', true);
                } else if (department == 'Production') {
                    $("#position>option:eq(3)").prop('selected', true);
                } else if (department == 'Sound') {
                    $("#position>option:eq(4)").prop('selected', true);
                } else if (department == 'Crew') {
                    $("#position>option:eq(5)").prop('selected', true);
                } else if (department == 'Creator') {
                    $("#position>option:eq(6)").prop('selected', true);
                }
                if (data.gender == 2) {
                    $("#male_radio").attr('checked', true);
                } else if (data.gender == 1) {
                    $("#female_radio").attr('checked', true);
                }
                if (data.also_known_as.length) {
                    var aka = '';
                    $.each(data.also_known_as, function(i, name) {
                        aka += name + ', ';
                    });
                    aka = aka.replace(/,\s*$/, "");
                    $('#celeb_known_as').val(aka);
                } else {
                    $('#celeb_known_as').val('');
                }
    
                if (data.profile_path) {
                    var image = "https://image.tmdb.org/t/p/w300" + data.profile_path;
                } else {
                    var image = "{{url('images/default.png')}}";
                }
    
                if (data.profile_path) {
                    $('#poster').val(image);
                    $('img#default_img').attr('src', image);
                } else {
                    $('img#default_img').attr('src', '{{url("images/default.png")}}');
                    $('#poster').val('');
                }
                $('.error-help-block').hide();

                // load cast crew
                if (data.combined_credits.cast.length > 0) {
                    var cast = JSON.stringify(data.combined_credits.cast);
                    // $.each(data.combined_credits.cast, function(i, actor) {
                    //     if (i == 0)
                    //         actors += actor.id + ':' + actor.name;
                    //     else
                    //         actors += '|' + actor.id + ':' + actor.name;
                    // });
                    $('#cast_credits').val(cast);
                } else {
                    $('#cast_credits').val('');
                }

                if (data.combined_credits.crew.length > 0) {
                    var crew = '';
                    var cast = JSON.stringify(data.combined_credits.crew);
                    // $.each(data.combined_credits.crew, function(i, actor) {
                    //     if (i == 0)
                    //         actors += actor.id + ':' + actor.name;
                    //     else
                    //         actors += '|' + actor.id + ':' + actor.name;
                    // });
                    $('#crew_credits').val(crew);
                } else {
                    $('#crew_credits').val('');
                }
    
            }
        });
    }

    // on change position change known_for_department
    $('#position').on('change', function(){
        var department = $(this).val();
        if (department == '1') {
            $("#known_for_department").val('Acting');
        } else if (department == 'Directing') {
            $("#known_for_department").val('Acting');
        } else if (department == 'Writing') {
            $("#known_for_department").val('Writing');
        } else if (department == 'Production') {
            $("#known_for_department").val('Production');
        } else if (department == 'Sound') {
            $("#known_for_department").val('Sound');
        } else if (department == 'Crew') {
            $("#known_for_department").val('Crew');
        } else if (department == 'Creator') {
            $("#known_for_department").val('Creator');
        }
    });
    
    // onchange default image poster will be blank
    $('#default_image').on('change', function() {
        $('#poster').val('');
    });
    
    $("#profile").change(function() {
        readURL(this);
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.profile-user-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    
    function removeFile(index) {
        console.log(index);
        delete uploadedFiles[index];
        $('#file_' + index).remove();
    }
    // published on timestamp
    $(document).ready(function() {
        $(document).on('click', '.edit-timestamp', function() {
            $(this).hide();
            $('#timestampdiv').slideDown('fast');
        });
    
        $(document).on('click', '.cancel-timestamp', function() {
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    
        $(document).on('click', '.save-timestamp', function() {
            var today_date = new Date();
            var aa = $('#aa').val(),
                mm = $('#mm').val(),
                jj = $('#jj').val(),
                hh = $('#hh').val(),
                mn = $('#mn').val(),
                newD = new Date(aa, mm - 1, jj, hh, mn);
    
            event.preventDefault();
    
            if (newD.getFullYear() != aa || (1 + newD.getMonth()) != mm || newD.getDate() != jj || newD.getMinutes() != mn) {
                $('.timestamp-wrap').addClass('form-invalid');
                return;
            } else {
                $('.timestamp-wrap').removeClass('form-invalid');
            }
    
            var month = $('#mm option[value="' + mm + '"]').attr('data-text');
            var date = parseInt(jj, 10);
            var year = aa;
            var hour = ('00' + hh).slice(-2);
            var min = ('00' + mn).slice(-2);
    
            var date_string = month + ' ' + date + ',' + ' ' + year + ' at ' + hour + ':' + min;
            var publish_time = year + '-' + (1 + newD.getMonth()) + '-' + date + ' ' + hour + ':' + min + ':00';
            if (today_date >= newD) {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(1);
                $('#timestamp').html('<label>Published on:' + ' <span class="published_on">' + date_string + '</span></label>');
            } else {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(2);
                $('#timestamp').html('<label>Scheduled for:' + ' <span class="published_on">' + date_string + '</span></label>');
            }
    
            // Move focus back to the Edit link.
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    });
    
    $(".save_people").on('click', function(e) {
        e.preventDefault();
        var err = '';
    
        var person_id = $("#person_id").val();
        var name = $("#people").val();
        var poster = $("#poster").val();
        var known_for_department = $("#known_for_department").val();
        //var celeb_place_of_birth = $("#celeb_place_of_birth").val();
        //var celeb_birthday = $("#celeb_birthday").val();
        var biography = CKEDITOR.instances.biography.getData();
        var default_image = document.getElementById("default_image").files.length;
    
        if (name == '') {
            err = "Name should not be blank";
        }
    
        if (biography == '' && err == '') {
            err = "Description should not be blank";
        }
    
        if (known_for_department == '' && err == '') {
            err = "Department should not be blank";
        }
    
        if (!person_id || person_id <= 0 || person_id == null) {
            if (default_image <= 0 && err == '' && poster == '') {
    
                err = "Please Choose Default Image.";
    
            }
        }
    
        if (err) {
    
            $("#error_messages_text").html(err);
    
            $("#error_popup").click();
    
            return false;
        }
    
        var image = true;
    
        if (!err && biography && image) {
            var formdata = new FormData($('#save_people_form')[0]);
            formdata.append('biography', biography);
            formdata.append('profile', $('#profile').val());
            formdata.append('default_image', default_image);
            $('.save_people').prop('disabled', true);
            $('#add_celebrity_loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                async: false,
                type: 'post',
                processData: false,
                contentType: false,
                url: "{{url('admin/people/save')}}",
                data: formdata,
                dataType: 'json',
                success: function(data) {
                    $('.save_people').prop('disabled', false);
                    if (data.success) {
                        //toastr.success(data.message);
                        //$('#save_people').reset();
                        setTimeout(function() {
                            window.location = "{{url('admin/celebs/create')}}"
                        }, 1000)
                    } else {
                        //toastr.error(data.message);
                    }
                    // $('#add_celebrity_loader').hide();
                },
                error: function() {
                    $('.save_people').prop('disabled', false);
                }
            });
        }
    });
    
    function loadFile(event, id) {
        // alert(event.files[0]);
        var reader = new FileReader();
    
        reader.onload = function() {
            var output = document.getElementById(id);
            // alert(output);
            output.src = reader.result;
            //$("#imagePreview").css("background-image", "url("+this.result+")");
        };
        reader.readAsDataURL(event.files[0]);
    } 

// upload multiple images
var gallery_images = [];
$(document).ready(function(){
    $(document).on('change', '#upload_images', function(){
        var form_data = new FormData();
        var err = 0;
        // Read selected files
        var totalfiles = document.getElementById('upload_images').files.length;
        for (var index = 0; index < totalfiles; index++) {
            var name = document.getElementById("upload_images").files[index].name;
            form_data.append("images[]", document.getElementById('upload_images').files[index]);

            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['png','jpg','jpeg']) == -1)
            {
                alert("Invalid File Extension. Only allowed png, jpg and jpeg!!");
                err++;
            }

            var size = document.getElementById("upload_images").files[index].size;
            if(size > 5000000)
            {
                toastr.error("File Size should be less than 5 MB");
                err++;
            }

        }

        if (err > 0) {
            return false;
        } else {
            // AJAX request
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{{ url("admin/people/images") }}',
                type: 'post',
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (res) {
                    if(res.success) {
                        for(var i=0;i<res.data.length ; i++){
                            $('#image-list').append("<li id="+ res.data[i].name + " class='image-item'><span class='pip'><img class='imageThumb' src='" + res.data[i].url + "' title='" + res.data[i].name + "'/> </span><a title='Delete' class='del_img remove_photo_" + res.data[i].name + "' href='javascript:void(0)' onclick='deleteFile(`image`, "+res.data[i].name+","+"`"+res.data[i].type+"`, `add`, 0"+")'>Remove</a><i id='login_loader_"+res.data[i].name +"' class='fa fa-spinner fa-spin' style='display:none'></i></li>");
                            gallery_images.push(res.data[i]);
                        }
                        $('#images_val').val(JSON.stringify(gallery_images));
                        //console.log(gallery_images);
                    } else {
                        $.each(res.message, function(i, val) {
                            alert(val);
                        });
                    }
                },
                error: function(xhr, status, errorThrown) {
                    alert("Sorry, there was a problem!");
                }
            });
        }
    });
});

// delete file
function deleteFile(filetype, name, type, mode, id) {
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+name).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('admin/people/delete-image') }}/" + filetype + "/" + name + "/"+ type + "/" + mode + "/" + id,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+name).hide();
                if (data.success) {
                    $('li#'+name).remove();
                    $('#upload_images').val('');
                    $.each(gallery_images, function(i, el){
                        if (this.name == name){
                            gallery_images.splice(i, 1);
                        }
                    });
                    $('#images_val').val(JSON.stringify(gallery_images));
                    //console.log(gallery_images);
                    //alert(data.message);
                } else {
                    alert(data.message);
                }
            }

        });
    }
}

// tags
$( function() {
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#tag_id" )
      // don't navigate away from the field on tab when selecting an item
        .on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
            event.preventDefault();
            }
        })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( '{{ url("admin/tag/search")}}', {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
    });
} );

var current_tags = [];
var tag_text = '';
var tagchecklist = $('.tagchecklist');
var newTags = [];
var currentTags = [];
var tags, newtag, newtags, text;
var tagDelimiter = ',';
function array_unique_noempty( array ) {
    var out = [];

    // Trim the values and ensure they are unique.
    $.each( array, function( key, val ) {
        val = $.trim( val );

        if ( val && $.inArray( val, out ) === -1 ) {
            out.push( val );
        }
    } );

    return out;
};

$(document).ready(function(){
    current_tags = $('.the-tags').val().split( tagDelimiter );
    createTags(current_tags);
    
    // add tag
    $(document).on('click', '.add-tag-btn', function(){
        tags = $('.the-tags'),
		newtag = $('#tag_id');
        text = newtag.val();
        tagsval = tags.val();
        newtags = tagsval ? tagsval + tagDelimiter + text : text;

        newtags = array_unique_noempty( newtags.split( tagDelimiter ) ).join( tagDelimiter );
        tags.val( newtags );
        newtag.val('');

        current_tags = tags.val().split( tagDelimiter );
        createTags(current_tags);
    });

    $( '#tag_id').keypress( function( event ) {
        if ( 13 == event.which ) {
            event.preventDefault();
            event.stopPropagation();

            tags = $('.the-tags'),
            newtag = $('#tag_id');
            text = newtag.val();
            tagsval = tags.val();
            newtags = tagsval ? tagsval + tagDelimiter + text : text;

            newtags = array_unique_noempty( newtags.split( tagDelimiter ) ).join( tagDelimiter );
            tags.val( newtags );
            newtag.val('');

            current_tags = tags.val().split( tagDelimiter );

            createTags(current_tags);
        }
    })

    function createTags(currentTags)
    {
        tagchecklist.empty();
        $.each( currentTags, function( key, val ) {
            var listItem, xbutton;
            val = $.trim( val );

            if (val != '') {
                
                listItem = $( '<li />' ).text( val );
                var xbutton = $( '<button type="button" id="post_tag-check-num-' + key + '" class="ntdelbutton">' + '<span>×</span></button>'+ listItem.html() );
        
                listItem.prepend( '&nbsp;' ).prepend( xbutton );
                tagchecklist.append(listItem);

                //$('#tag_id').val('');
            }
        });
    }

    // remove tags
    $(document).on('click', '.ntdelbutton', function(){
        parseTags(this);
    });

    function parseTags(el) {
        var id = el.id,
            num = id.split('post_tag-check-num-')[1],
            current_tags = $('.the-tags').val().split( tagDelimiter ),
            new_tags = [];
        delete current_tags[num];

        // Sanitize the current tags and push them as if they're new tags.
        $.each( current_tags, function( key, val ) {
            val = $.trim( val );
            if ( val ) {
                new_tags.push( val );
            }
        });

        $('.the-tags').val( new_tags.join( tagDelimiter ));

        createTags(current_tags);
        return false;
    }

});

/**
 * Function Name : displaySubCategory()
 * To Display all sub categories based on category id
 *
 * @var category_id    Selected Category id
 * 
 * @return Json Response
 */
 function makeSelected(){ // function is working in js/video-upload.js
    @if($model->sub_category_id )
    var e='{{ $model->sub_category_id}}';
    $('#sub_category').val(e.split(','));
    @endif
}

 var cat_url = "{{ url('select/sub_category')}}";
 function displaySubCategory(category_id = '', step) {
    $("#sub_category").html("");
    if (category_id == '') {
        var category_id = $("#category_id").val();
    }
    // $("#sub_category").html("<p class='text-center'><i class='fa fa-spinner'></i></p>");
    $.ajax({
        type: 'post',
        url: cat_url,
        data: { option: category_id },
        success: function(data) {
            $("#sub_category").html("");
            // console.log(data);return false;
            if (data == undefined) {
                alert("Oops Something went wrong. Kindly contact your administrator.");
                return false;
            }
            if (data.length == 0) {
                alert('No sub categories available. Kindly contact support team.');
                return false;
            }
            var subcategory = '';
            for (var i = 0; i < data.length; i++) {
                var value = data[i];
                subcategory += "<option value='" + value.id + "'>" + value.name + "</option>";
            }
            $("#sub_category").append(subcategory);
            $("#sub_category").select2("destroy");
            setTimeout(function() {
                makeSelected();
                $("#sub_category").select2();
            }, 1000);
        },
        error: function(data) {
            alert("Oops Something went wrong. Kindly contact your administrator.");
        }
    });
}
displaySubCategory();

$(document).ready(function(){
    // File upload via Ajax
    $("#uploadForm").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
            type: 'POST',
            url: '{{route("admin.content.save")}}',
            dataType: 'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function() {
                $('#finish_video').css({'background-color': '#3c8dbc', 'border-color': '#367fa9'});
                $(".progress-bar").width('0%');
                $("#finish_video").text("Processing...");
            },
            error:function(){
                alert('please try again.');
            },
            success: function(resp){
                $('#finish_video').css({'background-color': '#3c8dbc', 'border-color': '#367fa9'});
                $("#finish_video").text("Finish");
                $('.errors').html('');
                if(resp.success == true){
                    $('#uploadForm')[0].reset();
                    location.href = resp.redirect;
                } else if(resp.success == false){
                    $.each(resp.error , function(index, val) { 
                        console.log(index, val);
                        $('.errors').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>'+ val +'</div>');
                    });
                    //alert('please try again.');
                }
            },
            complete: function() {
                $('#finish_video').css({'background-color': '#3c8dbc', 'border-color': '#367fa9'});
                $("#finish_video").text("Redirecting...");
            }
        });
    });
});

// vote count
var ratingc_cal = false;
function calculate_rating(value1, value2, key)
{
    var error = 0;
    var msg = '';
    var vote_count, vote_avg, vote_rating_total;
    
    var validate = validate_votes(value1, value2, key);
    
    if (key == 'vote_count') {
        vote_count = parseInt(value1);
        vote_avg = parseInt(value2);
    }

    if (key == 'vote_average') {
        vote_avg = value1;
        vote_count = value2;
    }

    if (key == 'vote_rating_total') {
        vote_rating_total = value1;
        vote_count = value2;
    }

    vote_rating_total = Math.round(vote_avg * vote_count);
    if (vote_rating_total != 'undefiend') {
        $('#vote_total').val(vote_rating_total); 
        $('.vote_rating_total').text(vote_rating_total); 
    }

    if (validate) {
        return true;
    } else {
        return false;
    }

}

function validate_votes(value, value2, key)
{
    var error = 0;
    var msg = '';
    var float= /^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/;
    var vote_count, vote_avg, vote_rating_total;
    if(!$.isNumeric(value)) {
        error++;
        msg = 'Please enter numeric value';
        //$('#'+key).val(0);
        $('#'+key+'_input').val(0);
    } else if(value < 0 && value != '') {
        error++;
        msg = 'Please enter value greater or equal to 0';
        //$('#'+key).val(0);
        $('#'+key+'_input').val(0);
    } else if(key == 'vote_average' && (parseFloat(value) < 0 || parseFloat(value) > 10)) {
        error++;
        msg = 'The vote average value should be between 0 and 10.';
    }

    if (error > 0) {
        alert(msg);
        return false;
    } else {
        return true;
    }
}

$(document).ready(function(){

    // edit for vote average
    $(document).on('click', '.edit-vavg', function(){
        $(this).hide();
        $('.wrap_vavg').slideDown('fast');
    });

    $(document).on('click', '.cancel-vavg', function(){
        $('.wrap_vavg').slideUp('fast');
        $('.edit-vavg').show();
    });

    $(document).on('click', '.save-vavg', function(){
        var vote_avg = $('#vote_average_input').val();
        var vote_count = $('#vote_count_input').val();
        ratingc_cal = calculate_rating(vote_avg, vote_count, 'vote_average');
        if (ratingc_cal) {
            $('.vavg').text($('#vote_average_input').val());
            $('#vote_average').val($('#vote_average_input').val());
            $('.wrap_vavg').slideUp('fast');
            $('.edit-vavg').show();
        }
    });

    // edit for vote count
    $(document).on('click', '.edit-vcount', function(){
        $(this).hide();
        $('.wrap_vote_count').slideDown('fast');
    });

    $(document).on('click', '.cancel-vcount', function(){
        $('.wrap_vote_count').slideUp('fast');
        $('.edit-vcount').show();
    });

    $(document).on('click', '.save-vcount', function() {
        var vote_avg = $('#vote_average_input').val();
        var vote_count = $('#vote_count_input').val();
        ratingc_cal = calculate_rating(vote_count, vote_avg, 'vote_count');
        if (ratingc_cal) {
            $('.vote_count').text($('#vote_count_input').val());
            $('#vote_count').val($('#vote_count_input').val());
            $('.wrap_vote_count').slideUp('fast');
            $('.edit-vcount').show();
        }
    });

    // edit for vote rating total
    $(document).on('click', '.edit-vtotal', function(){
        $(this).hide();
        $('.wrap_vote_rating_total').slideDown('fast');
    });

    $(document).on('click', '.cancel-vtotal', function(){
        $('.wrap_vote_rating_total').slideUp('fast');
        $('.edit-vtotal').show();
    });

    $(document).on('click', '.save-vtotal', function(){
        $('.vote_rating_total').text($('#vote_rating_total').val());
        $('.wrap_vote_rating_total').slideUp('fast');
        $('.edit-vtotal').show();
    });
});
</script>
@endsection