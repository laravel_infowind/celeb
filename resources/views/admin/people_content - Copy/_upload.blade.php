<div class="card mb-20">
    <div class="card-header">
        <label for="upload_images">Upload Images</label>
    </div>
    <div class="card-body">
        <div class="publish_card mb-20">
            <input type="file" multiple name="images[]" id="upload_images"/>
            <div id="image_preview">
                <ul id=image-list>
                    @if(!empty($people_images))
                        @foreach ($people_images as $item)
                        <li id="{{ $item->name }}" class="image-item">
                            <span class="pip">
                                <img class="imageThumb" src="{{ $item->image }}" title="{{ $item->name }}"> 
                            </span>
                            <a title="Delete" class="del_img remove_photo_{{ $item->name }}" href="javascript:void(0)" onclick="deleteFile('image', `{{ $item->name }}`, `{{ $item->ext }}`, `edit`, {{ $item->id }})">
                                Remove
                            </a>
                            <i id="login_loader_{{ $item->name }}" class="fa fa-spinner fa-spin" style="display:none"></i>
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="card mb-20">
    <div class="card-header">
        <label for="upload_videos">Upload Videos</label>
    </div>
    <div class="card-body">
        <div class="publish_card mb-20">
            <input type="file" multiple name="videos[]" id="upload_video"/>
            <div id="main-video-player"></div>
            <div id="image_preview">
                <ul id=video-list>
                @if(!empty($people_videos))
                    @foreach ($people_videos as $item)
                        <li id="{{ $item->name }}" class="image-item">
                            <span class="pip">
                                <video controls width="250">
                                    <source src="{{ $item->video }}" type="{{ $item->mime_type }}">
                                </video>

                                
                            </span>
                            <a title="Delete" class="del_img remove_video_{{ $item->name }}" href="javascript:void(0)" onclick="deleteFile('video', `{{ $item->name }}`, `{{ $item->ext }}`, `edit`, {{ $item->id }})">
                                Remove
                            </a>
                            <i id="login_loader_{{ $item->name }}" class="fa fa-spinner fa-spin" style="display:none"></i>
                        </li>
                    @endforeach
                @endif
                </ul>
            </div>
        </div>
    </div>
</div>

<style>
input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
</style>