@extends('layouts.admin')

@section('title', tr('add_moderator'))

@section('content-header', tr('add_moderator'))

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.moderators')}}"><i class="fa fa-users"></i> {{tr('moderators')}}</a></li>
    <li class="active">{{tr('add_moderator')}}</li> -->
@endsection

@section('content')

@include('notification.notify')


<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.save.moderator')}}" method="POST" enctype="multipart/form-data" role="form">
    <div class="row">
        
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('add_moderator')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">

                                    <div class="form-group">
                                        <label for="username">{{tr('username')}}*</label>
                                        <input type="text" required pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}"   name="name" class="form-control" id="username" placeholder="{{tr('name')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">{{tr('email')}}*</label>
                                        <input type="email" maxlength="255"  pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,10}$" required class="form-control" id="email" name="email" placeholder="{{tr('email')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="mobile">{{tr('mobile')}}</label>
                                        <input type="text" name="mobile" class="form-control" id="mobile" placeholder="{{tr('mobile')}}" minlength="4" maxlength="16" pattern="[0-9]{4,16}">
                                        <br>
                                        <small style="color:brown">{{tr('mobile_note')}}</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="description">{{tr('description')}}</label>
                                        <textarea class="form-control"  placeholder="{{tr('description')}}" name="description" id="ckeditor"></textarea>
                                    </div>


                                    <div class="form-group">
                                        <label for="password">{{tr('password')}}*</label>
                                        <input type="password" required name="password" class="form-control"  minlength="6" id="password" placeholder="{{tr('password')}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="username">{{tr('password_confirmation')}}*</label>
                                        <input type="password"  minlength="6" required name="password_confirmation" class="form-control" id="username" placeholder="{{tr('password_confirmation')}}">
                                    </div>

                                </div>

                                <input type="hidden" name="timezone" value="" id="userTimezone">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($moderator) !!}
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                 <!-- <button type="reset" class="btn btn-danger">{{tr('cancel')}}</button> -->
                                 @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>Publish</a>
                                @else
                                    <button type="submit" class="btn btn-primary">Publish</button>
                                @endif 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Upload Image</label>
                    </div>
                    <div class="card-body">
                        <div class="image-upload">
                            <div class="image-upload-item">
                                
                                <input type="file"  accept="image/png, image/jpeg" name="picture" id="picture" placeholder="Default image" style="display:none" onchange="loadFile(this,'picture_img')">

                                <img src="{{url('images/default.png')}}" onclick="$('#picture').click();return false;" id="picture_img" style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">
                        
                            </div>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>

@endsection

@section('scripts')
<script src="{{asset('assets/js/jstz.min.js')}}"></script>
<script>
    
    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin/60);
        $("#userTimezone").val(jstz.determine().name());
    });

function loadFile(event, id) {
    // alert(event.files[0]);
    var reader = new FileReader();

    reader.onload = function() {
        var output = document.getElementById(id);
        // alert(output);
        output.src = reader.result;
        //$("#imagePreview").css("background-image", "url("+this.result+")");
    };
    reader.readAsDataURL(event.files[0]);
}

</script>

@endsection