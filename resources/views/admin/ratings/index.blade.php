@extends('layouts.admin')

@section('title', tr('view_rating'))

@section('content-header', tr('view_rating'))

@section('breadcrumb')

<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

<li class="active"><i class="fa fa-star"></i> {{tr('view_rating')}}</li>

@endsection

@section('content')

@include('notification.notify')

<div class="row">
    <div class="col-lg-12">
        <div class="box box-primary">

            <div class="box-header label-primary">

                <b style="font-size:18px;">{{tr('view_rating')}}</b>
            </div>

            <div class="box-body">

                <table id="cast_crewsdatatable" class="table table-bordered table-striped">
                    <div class="search-table-top">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="dataTables_length">
                                    <label>Show
                                        <form method="get" action="{{ url('admin/ratings/index') }}"
                                            class="videos-change-pagination">
                                            <input type="hidden" name="column" value="{{ $column }}">
                                            <input type="hidden" name="order_by"
                                                value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                            <input type="hidden" name="search" value="{{ $rated_by_user_search }}">


                                            <select name="per_page" aria-controls="datatable"
                                                class="form-control input-sm" onchange="this.form.submit()">
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?>
                                                    value="10">10</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?>
                                                    value="25">25</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?>
                                                    value="50">50</option>
                                                <option
                                                    <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?>
                                                    value="100">100</option>
                                            </select>
                                        </form>
                                        entries</label>
                                </div>
                            </div>



                            <div class="col-sm-6">
                                <form action="{{route('admin.ratings.index')}}" class="form">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search by Movie"
                                            class="form-control input-video-search" name="search"
                                            value="{{session('rated_by_user_search')}}">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-success">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <thead>
                        <tr>
                            <th>
                                <a href="{{ url('admin/ratings/index?column=user_reviews.admin_video_id&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">{{tr('movies')}}
                                    <i class="fa fa-sort<?php echo $column == 'admin_video_id' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            <th><a href="{{ url('admin/ratings/index?column=review_count&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$rated_by_user_search) }}">{{tr('review_count')}}
                                <i class="fa fa-sort<?php echo $column == 'admin_video_id' ? '-' . $up_or_down : ''; ?>"></i></a>
                            </th>
                            
                        </tr>
                    </thead>

                    <tbody class="comment-table response-links">
                        @foreach ($model as $value)
                        <tr>    
                            <td><a href="{{ url('admin/ratings/list') }}/{{ $value->admin_video_id }}">{{ $value->title }}
                                <div class="response-links">
                                    <a class="comments-view-item-link" href="{{ url('movies/') }}/{{ $value->adminVideo->unique_id }}">View Video</a>
                                    
                                </div>
                            </td>
                            <td>
                                <a href="{{ url('admin/ratings/list') }}/{{ $value->admin_video_id }}" class="post-com-count post-com-count-approved">
                                    <span class="comment-count-approved comment-count-408" aria-hidden="true">{{ $value->review_count }}</span></a>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>

                </table>
                <div class="pagination-video-list">

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">
                                Showing {{ ($model->currentpage()-1)*$model->perpage()+1 }} to
                                {{ ($model->currentpage()*$model->perpage() < $model->total() ) ? $model->currentpage()*$model->perpage() : $model->total() }}
                                of {{ $model->total() }} entries
                            </div>
                        </div>
                        <div class="col-sm-7 text-right">
                            <div>{{ $model->links() }}</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endsection