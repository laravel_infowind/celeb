@extends('layouts.admin')



@section('title', tr('password_reset'))

@section('content-header', 'Password Reset')

@section('breadcrumb')

    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

    <li><a href="{{route('admin.users')}}"><i class="fa fa-user"></i> {{tr('users')}}</a></li>

    <li class="active"><i class="fa fa-user"></i> {{tr('view_user')}}</li>

@endsection

@section('content')

@include('notification.notify')

<div class="row">



    <div class="col-md-10">



        <div class="box box-primary">



            <div class="box-header label-primary">
                <a href="{{route('admin.users')}}" class="btn btn-default pull-right">{{tr('view_users')}}</a>

            </div>



            <form class="form-horizontal" action="{{route('admin.users.password_update')}}" method="POST" enctype="multipart/form-data" role="form">



                <div class="box-body">

                    <input type="hidden" value="{{ $user_data->id }}" name="user_id" />

                          <div class="form-group row">

                        <label for="password" class="col-sm-2 control-label">* {{tr('password')}}</label>



                        <div class="col-sm-10">

                            <input type="password" required  name="password" pattern=".{6,}" title="{{tr('password_notes')}}" class="form-control" id="password" placeholder="{{tr('password')}}" value="{{old('password')}}">

                        </div>

                    </div>



                    <div class="form-group row">

                        <label for="username" class="col-sm-2  control-label">* {{tr('password_confirmation')}}</label>



                        <div class="col-sm-10">

                            <input type="password" required pattern=".{6,}"  title="{{tr('password_notes')}}"  name="password_confirmation" class="form-control" id="username" placeholder="{{tr('password_confirmation')}}"  value="{{old('password_confirmation')}}">

                        </div>

                    </div>



                </div>



                <div class="box-footer">

                    <a type="reset" href="{{url('admin/users')}}" class="btn btn-danger">{{tr('cancel')}}</a>

                    @if(Setting::get('admin_delete_control'))

                        <a href="#" class="btn btn-success pull-right" disabled>{{tr('submit')}}</a>

                    @else

                        <button type="submit" class="btn btn-success pull-right">{{tr('submit')}}</button>

                    @endif

                </div>

                <input type="hidden" name="timezone" value="" id="userTimezone">

            </form>



        </div>



    </div>



</div>





@endsection

