@if($edit_content)
    @if ($model->file_type == 1)
    <div class="card mb-20">
        <div class="card-header">
            <label for="upload_images">Upload Image</label>
        </div>
        <div class="card-body">
            <div class="publish_card mb-20">
                <input type="file" name="images[]" id="upload_images_remove"/>
                <div id="image_preview">
                    <ul id=image-list>
                        @if(!empty($people_images))
                            <?php $ext = pathinfo($people_images->filename, PATHINFO_EXTENSION);
                            $filename = pathinfo($people_images->filename, PATHINFO_FILENAME);?>
                            <li id="{{ $people_images->name }}" class="image-item">
                                <span class="pip">
                                    <img class="imageThumb" src="{{ $people_images->file }}" title="{{ $filename }}"> 
                                </span>
                                {{-- <a title="Delete" class="del_img remove_photo_{{ $filename }}" href="javascript:void(0)" onclick="deleteFile('image', `{{ $filename }}`, `{{ $ext }}`, `edit`, {{ $people_images->id }})">
                                    Remove
                                </a>
                                <i id="login_loader_{{ $filename }}" class="fa fa-spinner fa-spin" style="display:none"></i> --}}
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($model->file_type == 2)
        <div class="card mb-20">
            <div class="card-header">
                <label for="upload_videos">Upload Video</label>
            </div>
            <div class="card-body">
                <div class="publish_card mb-20">
                    <input type="file" name="videos[]" id="upload_video_remove"/>
                    <div id="main-video-player"></div>
                    <div id="image_preview">
                        <ul id=video-list>
                        @if(!empty($people_videos))
                            <?php $ext = pathinfo($people_videos->filename, PATHINFO_EXTENSION);
                            $filename = pathinfo($people_videos->filename, PATHINFO_FILENAME);?>
                            <li id="{{ $filename }}" class="image-item">
                                <span class="pip">
                                    <video controls width="250">
                                        <source src="{{ $people_videos->file }}" type="{{ $people_videos->mime_type }}">
                                    </video>

                                    
                                </span>
                                {{-- <a title="Delete" class="del_img remove_video_{{ $filename }}" href="javascript:void(0)" onclick="deleteFile('video', `{{ $filename }}`, `{{ $ext }}`, `edit`, {{ $people_videos->id }})">
                                    Remove
                                </a>
                                <i id="login_loader_{{ $filename }}" class="fa fa-spinner fa-spin" style="display:none"></i> --}}
                            </li>
                        @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
@else
<div class="card mb-20">
    <div class="card-header">
        <label for="upload_images">Upload Image</label>
    </div>
    <div class="card-body">
        <div class="publish_card mb-20">
            <input type="file" name="images[]" id="upload_images_remove"/>
            <div id="image_preview">
                <ul id=image-list>
                    @if(!empty($people_images))
                        <?php $ext = pathinfo($people_images->filename, PATHINFO_EXTENSION);
                        $filename = pathinfo($people_images->filename, PATHINFO_FILENAME);?>
                        <li id="{{ $people_images->name }}" class="image-item">
                            <span class="pip">
                                <img class="imageThumb" src="{{ $people_images->file }}" title="{{ $filename }}"> 
                            </span>
                            {{-- <a title="Delete" class="del_img remove_photo_{{ $filename }}" href="javascript:void(0)" onclick="deleteFile('image', `{{ $filename }}`, `{{ $ext }}`, `edit`, {{ $people_images->id }})">
                                Remove
                            </a>
                            <i id="login_loader_{{ $filename }}" class="fa fa-spinner fa-spin" style="display:none"></i> --}}
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="card mb-20">
    <div class="card-header">
        <label for="upload_videos">Upload Video</label>
    </div>
    <div class="card-body">
        <div class="publish_card mb-20">
            <input type="file" name="videos[]" id="upload_video_remove"/>
            <div id="main-video-player"></div>
            <div id="image_preview">
                <ul id=video-list>
                @if(!empty($people_videos))
                    <?php $ext = pathinfo($people_videos->filename, PATHINFO_EXTENSION);
                    $filename = pathinfo($people_videos->filename, PATHINFO_FILENAME);?>
                    <li id="{{ $filename }}" class="image-item">
                        <span class="pip">
                            <video controls width="250">
                                <source src="{{ $people_videos->file }}" type="{{ $people_videos->mime_type }}">
                            </video>

                            
                        </span>
                        {{-- <a title="Delete" class="del_img remove_video_{{ $filename }}" href="javascript:void(0)" onclick="deleteFile('video', `{{ $filename }}`, `{{ $ext }}`, `edit`, {{ $people_videos->id }})">
                            Remove
                        </a>
                        <i id="login_loader_{{ $filename }}" class="fa fa-spinner fa-spin" style="display:none"></i> --}}
                    </li>
                @endif
                </ul>
            </div>
        </div>
    </div>
</div>
@endif


<style>
input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
</style>