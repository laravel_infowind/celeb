@extends('layouts.admin')

@section('title', tr('view_content'))

@section('content-header', tr('view_content'))

@section('breadcrumb')
    
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    
    <li class="active"><i class="fa fa-users"></i> {{tr('view_content')}}</li>
    
@endsection

@section('content')

    @include('notification.notify')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                
                <div class="box-header label-primary">
                    
                    <b style="font-size:18px;">{{tr('view_content')}}</b>

                    {{-- <a href="{{route('admin.content.add')}}" class="btn btn-default pull-right">{{tr('add_content')}}</a> --}}

                </div>

                <div class="box-body">

                    <table id="cast_crewsdatatable" class="table table-bordered table-striped">
                        <div class="search-table-top"> 
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_length">
                                        <label>Show
                                            <form method="get" action="{{ url('admin/contents') }}" class="videos-change-pagination">
                                                <input type="hidden" name="column" value="{{ $column }}">
                                                <input type="hidden" name="order_by" value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                                <input type="hidden" name="search" value="{{ $cast_crew_name }}">


                                                <select name="per_page" aria-controls="datatable" class="form-control input-sm"  onchange="this.form.submit()">
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?> value="10">10</option>
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?> value="25">25</option>
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?> value="50">50</option>
                                                 <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?> value="100">100</option>
                                             </select>
                                         </form>
                                     entries</label>
                                 </div>
                             </div>



                             <div class="col-sm-6">
                                <form action="{{route('admin.contents')}}" class="form">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search by Name" class="form-control input-video-search" name="search" value="{{session('cast_crew_search')}}">
                                        <div class="input-group-prepend">
                                            <button type="submit" class="btn btn-success">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                        <thead>
                            <tr>
                                
                                <th>
                                    <a href="{{ url('admin/contents?column=name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$cast_crew_name) }}">File Name <i class="fa fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
                                </th>
                                <th>
                                    <a href="{{ url('admin/contents?column=people_name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$cast_crew_name) }}">Celebrity <i class="fa fa-sort<?php echo $column == 'people_name' ? '-' . $up_or_down : ''; ?>"></i></a>
                                </th>
                                <th>
                                    <a href="{{ url('admin/contents?column=category_name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$cast_crew_name) }}">Category <i class="fa fa-sort<?php echo $column == 'category_name' ? '-' . $up_or_down : ''; ?>"></i></a>
                                </th>
                                <th>File</th>
                                <th>
                                    <a href="{{ url('admin/contents?column=status&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$cast_crew_name) }}">{{tr('status')}} <i class="fa fa-sort<?php echo $column == 'status' ? '-' . $up_or_down : ''; ?>"></i></a>
                                </th>
                                <th style="vertical-align: 0px;">
						    		<a href="{{ url('admin/contents?column=created_at&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$cast_crew_name) }}">Publish Date <i class="fa fa-sort<?php echo $column == 'created_at' ? '-' . $up_or_down : ''; ?>"></i></a>
						    	</th>
                            </tr>
                        </thead>

                        <tbody>

                            @foreach($model as $i => $data)
                            <tr>
                               
                                <td>{{$data->filename}}
                                    <ul class="table-row-actions">
                                        <li role="presentation">
                                            @if(Setting::get('admin_delete_control'))
                                            <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                            @else
                                            <a role="menuitem" tabindex="-1" href="{{route('admin.content.edit' , array('id' => $data->id))}}">{{tr('edit')}}</a>
                                            @endif
                                        </li> 
                                        <li role="presentation"> 
                                            @if(Setting::get('admin_delete_control'))
                                            <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('view')}}</a>
                                            @else
                                            <a role="menuitem" tabindex="-1" href="{{route('admin.cast_crews.view' , array('id' => $data->people->id))}}">{{tr('view')}}</a>
                                            @endif 
                                        </li>
                                        <li role="presentation">
                                            <?php $decline_msg = tr('decline_content');?>
                                            @if($data->status == 0)
                                            <a class="menuitem" tabindex="-1" href="{{route('admin.content.status',['id'=>$data->id])}}" onclick="return confirm('Are You Sure?')">{{tr('approve')}} </a>
                                            @else
                                            <a class="menuitem" tabindex="-1" href="{{route('admin.content.status',['id'=>$data->id])}}" onclick="return confirm('{{$decline_msg}}')">{{tr('decline')}}</a>
                                            @endif
                                        </li>
                                        <li role="presentation">
                                            @if(Setting::get('admin_delete_control'))
                                            <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>
                                            @else
                                            <a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('content_delete_confirmation' , $data->name)}}&quot;);" href="{{route('admin.content.delete' , array('id' => $data->id))}}">{{tr('delete')}}</a>
                                            @endif
                                        </li>
                                    </ul>
                                </td>

                                
                                <td>{{$data->people_name}}</td>
                                
                                <td>{{$data->category_name}}</td>
							    
                                <td>
                                    @if ($data->id && (!empty($data->file)) && ($data->file_type == '1'))
                                        <a href="{{ $data->file }}" target="_blank"><img title="{{ $data->filename }}" style="width: 50px;" src="{{($data->id && (!empty($data->file))) ? $data->file : url('placeholder.png')}}"></a>
                                    @elseif ($data->id && (!empty($data->file)) && ($data->file_type == '2'))
                                        <a href="{{ $data->file }}" target="_blank"><i class="fa fa-paperclip"></i> {{ $data->filename }}</a>
                                    @endif
                                    
                                </td>
                                <td>
                                    @if($data->status)
                                    <span class="label label-success">{{tr('approve')}}</span>
                                    @else
                                    <span class="label label-warning">{{tr('pending')}}</span>
                                    @endif
                                </td>
                                <td>{{ date('Y-m-d', strtotime($data->created_at)) }}</td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                    <div class="pagination-video-list">
                        
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="dataTables_info" id="datatable-checkbox_info" role="status" aria-live="polite">Showing {{ ($model->currentpage()-1)*$model->perpage()+1 }} to {{ ($model->currentpage()*$model->perpage() < $model->total() ) ? $model->currentpage()*$model->perpage() : $model->total() }} of {{ $model->total() }} entries
                                </div>
                            </div>
                            <div class="col-sm-7 text-right">
                                <div>{{ $model->links() }}</div>
                            </div>
                        </div>

                    </div>
            </div>
        </div>
    </div>
@endsection
