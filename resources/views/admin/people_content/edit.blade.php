@extends('layouts.admin')

@section('title', tr('edit_content'))

@section('content-header', tr('edit_content'))

@section('breadcrumb')
    
@endsection


@section('content')

@include('notification.notify')

@include('admin.people_content._form')

@endsection

@section('scripts')
    <script type="text/javascript">

        function loadFile(event,id){

            $('#'+id).show();

            var reader = new FileReader();

            reader.onload = function(){

                var output = document.getElementById(id);

                output.src = reader.result;
            };

            reader.readAsDataURL(event.files[0]);
        }

    </script>
@endsection
