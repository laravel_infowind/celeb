@extends('layouts.admin')

@section('title', tr('add_content'))

@section('content-header')

    <span style="color:#1d880c !important">{{$model->name}} </span> - {{tr('add_content') }}

@endsection

@section('breadcrumb')
    <!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.cast_crews.index')}}"><i class="fa fa-users"></i> {{tr('cast_crews')}}</a></li>
    <li class="active">{{tr('add_cast_crew')}}</li> -->
@endsection


@section('content')

@include('notification.notify')
@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .container-narrow {
        margin: 150px auto 50px auto;
        max-width: 728px;
    }

    .progress-bar {
        background-color: #5cb85c !important;
    }

    canvas {
        width: 100%;
        height: auto;
    }

    span.select2-container {
        width: 100% !important;
    }

    .example-wizard.panel {
        border: 0px solid white;
        padding: 20px;
        display: inline-block;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        margin-bottom: 10px;
        width: 30%;
    }

    .select2-container .select2-search--inline {
        border: 1px solid #d2d6df !important;
        width: 30%;
    }
</style>
@endsection
<!-- popup -->
<div class="modal fade error-popup" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="media">
                    <div class="media-left">
                        <img src="{{asset('images/warning.jpg')}}" class="media-object" style="width:60px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Information</h4>
                        <p id="error_messages_text"></p>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-primary top" data-dismiss="modal">Okay</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="errors"></div>
<div class="main-content">
    <form id="uploadForm" class="form-horizontal" action="{{route('admin.add_content.save')}}" method="POST" enctype="multipart/form-data" role="form">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
        <input type="hidden" name="id" value="{{$model->id}}">
        <input type="hidden" name="person_id" id="person_id" value="{{ ($model->id) ? $model->cast_id : 0 }}">
        <input type="hidden" name="images_val" id="images_val" value=""/>
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('name')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span> </label>
                            </div>
                            <div class="card-body">
                                <div class="publish_card">
                                    <input class="form-control" style="background-color: #fff !important;" required type="text" name="name" value="{{$model->name}}" id='people' autocomplete="off" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- tab4 -->
                @include('admin.people_content._upload')
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <div class="publish_item">
                                                
                                <div style="padding-left:0px; padding-right:0px; width: 100%; margin-left: 3px; margin-top: 9px;" class="progress col-sm-12 col-md-4 col-lg-4"
                                    style="margin-top: 8px;">
                                    <div class="progress-bar">0 %</div>
                                </div>
                                
                            </div>
                            <div class="publish_item mb-20">
                                <label>Moderator <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                                <select class="form-control" required name="moderator_id">
                                    <option value=""></option>
                                    @foreach($moderators as $mod)
                                    <option {{ ($mod->id == $model->moderator_id ) ? 'selected' : '' }}
                                        value="{{ $mod->id }}">{{ $mod->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            {!! App\Helpers\Helper::published_block($model) !!}

                            @if($model->id)
                                <div class="publish_item mb-20">
                                    <label>Modified on: <span class="published_on">{{ date('Y-m-d', strtotime($model->updated_at)). ' at '. date('H:i', strtotime($model->updated_at)) }}</span></label>
                                </div>
                                <div class="publish_item mb-20">
                                    <label>Created on: <span class="published_on">{{ date('Y-m-d', strtotime($model->created_at)). ' at '. date('H:i', strtotime($model->created_at)) }}</span></label> 
                                </div>
                            
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a id="finish_video" href="#" class="btn btn-primary" disabled>{{ (!$model->id) ? 'Publish' : 'Publish' }}</a>
                                @else
                                    <button id="finish_video" type="submit" class="btn btn-primary">{{ (!$model->id) ? 'Publish' : 'Publish' }}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-20">
                    <div class="card-header">
                        <label>{{tr('category')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <select class="form-control" onchange="displaySubCategory(this.value);"
                                name="category_id" id="category_id" required="">
                                @foreach($categories as $category)
                                    <option cat_slug="{{ $category->category_slug }}" {{ ($category->id == $model->category_id) ? 'selected' : '' }}
                                    value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label>Sub-Categories <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <select multiple class="form-control select2" name="sub_category_id[]"
                                id="sub_category">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label>Tags</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            <div class="tags_wrap">
                            <?php $selected_tags = App\Helpers\Helper::getSelectedTags($model->id); ?>
                            <textarea name="tags_input" rows="3" cols="20" class="the-tags hide form-control" id="tax-input-post_tag" aria-describedby="new-tag-post_tag-desc" spellcheck="false">{{ $selected_tags }}</textarea>

                                <input class="form-control" type="text" name="tags[]" value="" id="tag_id"/> 
                                <a href="javascript:void(0)" class="add-tag-btn btn" role="button"><span aria-hidden="true">Add</span></a>
                            </div>
                            <ul class="tagchecklist" role="list">
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>


<div class="overlay">
    <div id="loading-img"></div>
</div>

<div class="modal fade" id="data-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Peoples</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumbnail</th>
                            <th>Celebrity Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="celebrity-data">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="">
                    <button class="tmdb_btn_prev pagination_btn btn btn-primary" onclick="getCelebrityList('prev')"
                        prev_page="0">Prev</button>
                    <button class="tmdb_btn_next pagination_btn btn btn-primary" onclick="getCelebrityList('next')"
                        next_page="2">Next</button>
                    <button type="button" class="pagination_btn btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
@section('scripts')
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/moment.min.js')}}"></script>
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    var page = 1;
    var total_page;
    var TMDB_KEY = "{{env('TMDB_API_KEY')}}";

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.profile-user-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    
    function removeFile(index) {
        console.log(index);
        delete uploadedFiles[index];
        $('#file_' + index).remove();
    }
    // published on timestamp
    $(document).ready(function() {
        $(document).on('click', '.edit-timestamp', function() {
            $(this).hide();
            $('#timestampdiv').slideDown('fast');
        });
    
        $(document).on('click', '.cancel-timestamp', function() {
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    
        $(document).on('click', '.save-timestamp', function() {
            var today_date = new Date();
            var aa = $('#aa').val(),
                mm = $('#mm').val(),
                jj = $('#jj').val(),
                hh = $('#hh').val(),
                mn = $('#mn').val(),
                newD = new Date(aa, mm - 1, jj, hh, mn);
    
            event.preventDefault();
    
            if (newD.getFullYear() != aa || (1 + newD.getMonth()) != mm || newD.getDate() != jj || newD.getMinutes() != mn) {
                $('.timestamp-wrap').addClass('form-invalid');
                return;
            } else {
                $('.timestamp-wrap').removeClass('form-invalid');
            }
    
            var month = $('#mm option[value="' + mm + '"]').attr('data-text');
            var date = parseInt(jj, 10);
            var year = aa;
            var hour = ('00' + hh).slice(-2);
            var min = ('00' + mn).slice(-2);
    
            var date_string = month + ' ' + date + ',' + ' ' + year + ' at ' + hour + ':' + min;
            var publish_time = year + '-' + (1 + newD.getMonth()) + '-' + date + ' ' + hour + ':' + min + ':00';
            if (today_date >= newD) {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(1);
                $('#timestamp').html('<label>Published on:' + ' <span class="published_on">' + date_string + '</span></label>');
            } else {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(2);
                $('#timestamp').html('<label>Scheduled for:' + ' <span class="published_on">' + date_string + '</span></label>');
            }
    
            // Move focus back to the Edit link.
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    });
    
    $(".save_people").on('click', function(e) {
        e.preventDefault();
        var err = '';
    
        var person_id = $("#person_id").val();
        var name = $("#people").val();
        var poster = $("#poster").val();
        var known_for_department = $("#known_for_department").val();
        //var celeb_place_of_birth = $("#celeb_place_of_birth").val();
        //var celeb_birthday = $("#celeb_birthday").val();
        var biography = CKEDITOR.instances.biography.getData();
        var default_image = document.getElementById("default_image").files.length;
    
        if (name == '') {
            err = "Name should not be blank";
        }
    
        if (biography == '' && err == '') {
            err = "Description should not be blank";
        }
    
        if (known_for_department == '' && err == '') {
            err = "Department should not be blank";
        }
    
        if (!person_id || person_id <= 0 || person_id == null) {
            if (default_image <= 0 && err == '' && poster == '') {
    
                err = "Please Choose Default Image.";
    
            }
        }
    
        if (err) {
    
            $("#error_messages_text").html(err);
    
            $("#error_popup").click();
    
            return false;
        }
    
        var image = true;
    
        if (!err && biography && image) {
            var formdata = new FormData($('#save_people_form')[0]);
            formdata.append('biography', biography);
            formdata.append('profile', $('#profile').val());
            formdata.append('default_image', default_image);
            $('.save_people').prop('disabled', true);
            $('#add_celebrity_loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                async: false,
                type: 'post',
                processData: false,
                contentType: false,
                url: "{{url('admin/people/save')}}",
                data: formdata,
                dataType: 'json',
                success: function(data) {
                    $('.save_people').prop('disabled', false);
                    if (data.success) {
                        //toastr.success(data.message);
                        //$('#save_people').reset();
                        setTimeout(function() {
                            window.location = "{{url('admin/celebs/create')}}"
                        }, 1000)
                    } else {
                        //toastr.error(data.message);
                    }
                    // $('#add_celebrity_loader').hide();
                },
                error: function() {
                    $('.save_people').prop('disabled', false);
                }
            });
        }
    });
    
    function loadFile(event, id) {
        // alert(event.files[0]);
        var reader = new FileReader();
    
        reader.onload = function() {
            var output = document.getElementById(id);
            // alert(output);
            output.src = reader.result;
            //$("#imagePreview").css("background-image", "url("+this.result+")");
        };
        reader.readAsDataURL(event.files[0]);
    } 

// upload multiple images
var gallery_images = [];
$(document).ready(function(){
    $(document).on('change', '#upload_images', function(){
        var form_data = new FormData();
        var err = 0;
        // Read selected files
        var totalfiles = document.getElementById('upload_images').files.length;
        for (var index = 0; index < totalfiles; index++) {
            var name = document.getElementById("upload_images").files[index].name;
            form_data.append("images[]", document.getElementById('upload_images').files[index]);

            var ext = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(ext, ['png','jpg','jpeg']) == -1)
            {
                alert("Invalid File Extension. Only allowed png, jpg and jpeg!!");
                err++;
            }

            var size = document.getElementById("upload_images").files[index].size;
            if(size > 5000000)
            {
                toastr.error("File Size should be less than 5 MB");
                err++;
            }

        }

        if (err > 0) {
            return false;
        } else {
            // AJAX request
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{{ url("admin/people/images") }}',
                type: 'post',
                data: form_data,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (res) {
                    if(res.success) {
                        for(var i=0;i<res.data.length ; i++){
                            $('#image-list').append("<li id="+ res.data[i].name + " class='image-item'><span class='pip'><img class='imageThumb' src='" + res.data[i].url + "' title='" + res.data[i].name + "'/> </span><a title='Delete' class='del_img remove_photo_" + res.data[i].name + "' href='javascript:void(0)' onclick='deleteFile(`image`, "+res.data[i].name+","+"`"+res.data[i].type+"`, `add`, 0"+")'>Remove</a><i id='login_loader_"+res.data[i].name +"' class='fa fa-spinner fa-spin' style='display:none'></i></li>");
                            gallery_images.push(res.data[i]);
                        }
                        $('#images_val').val(JSON.stringify(gallery_images));
                        //console.log(gallery_images);
                    } else {
                        $.each(res.message, function(i, val) {
                            alert(val);
                        });
                    }
                },
                error: function(xhr, status, errorThrown) {
                    alert("Sorry, there was a problem!");
                }
            });
        }
    });
});

// delete file
function deleteFile(filetype, name, type, mode, id) {
    var result = confirm("Are you sure to delete?");
    if(result){
        $('#login_loader_'+name).show();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            type: "GET",
            url: "{{ url('admin/people/delete-image') }}/" + filetype + "/" + name + "/"+ type + "/" + mode + "/" + id,
            dataType:'json',
            success: function(data) {
                $('#login_loader_'+name).hide();
                if (data.success) {
                    $('li#'+name).remove();
                    $('#upload_images').val('');
                    $.each(gallery_images, function(i, el){
                        if (this.name == name){
                            gallery_images.splice(i, 1);
                        }
                    });
                    $('#images_val').val(JSON.stringify(gallery_images));
                    //console.log(gallery_images);
                    //alert(data.message);
                } else {
                    alert(data.message);
                }
            }

        });
    }
}

$(document).ready(function(){
    // File upload via Ajax
    $("#uploadForm").on('submit', function(e){
        e.preventDefault();
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
            type: 'POST',
            url: '{{route("admin.add_content.save")}}',
            dataType: 'json',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function() {
                $('#finish_video').css({'background-color': '#3c8dbc', 'border-color': '#367fa9'});
                $(".progress-bar").width('0%');
                $("#finish_video").text("Processing...");
            },
            error:function(){
                alert('please try again.');
                $("#finish_video").text("Publish");
            },
            success: function(resp){
                $('#finish_video').css({'background-color': '#3c8dbc', 'border-color': '#367fa9'});
                $("#finish_video").text("Finish");
                $('.errors').html('');
                if(resp.success == true){
                    $("#finish_video").text("Redirecting...");
                    $('#uploadForm')[0].reset();
                    location.href = resp.redirect;
                } else if(resp.success == false){
                    $("#finish_video").text("Publish");
                    $.each(resp.error , function(index, val) { 
                        console.log(index, val);
                        $('.errors').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>'+ val +'</div>');
                    });
                    //alert('please try again.');
                }
            },
            complete: function() {
                $('#finish_video').css({'background-color': '#3c8dbc', 'border-color': '#367fa9'});
            }
        });
    });
});

// tags
$( function() {
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#tag_id" )
      // don't navigate away from the field on tab when selecting an item
        .on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
            event.preventDefault();
            }
        })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( '{{ url("admin/tag/search")}}', {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
    });
} );

var current_tags = [];
var tag_text = '';
var tagchecklist = $('.tagchecklist');
var newTags = [];
var currentTags = [];
var tags, newtag, newtags, text;
var tagDelimiter = ',';
function array_unique_noempty( array ) {
    var out = [];

    // Trim the values and ensure they are unique.
    $.each( array, function( key, val ) {
        val = $.trim( val );

        if ( val && $.inArray( val, out ) === -1 ) {
            out.push( val );
        }
    } );

    return out;
};

$(document).ready(function(){
    current_tags = $('.the-tags').val().split( tagDelimiter );
    createTags(current_tags);
    
    // add tag
    $(document).on('click', '.add-tag-btn', function(){
        tags = $('.the-tags'),
		newtag = $('#tag_id');
        text = newtag.val();
        tagsval = tags.val();
        newtags = tagsval ? tagsval + tagDelimiter + text : text;

        newtags = array_unique_noempty( newtags.split( tagDelimiter ) ).join( tagDelimiter );
        tags.val( newtags );
        newtag.val('');

        current_tags = tags.val().split( tagDelimiter );
        createTags(current_tags);
    });

    $( '#tag_id').keypress( function( event ) {
        if ( 13 == event.which ) {
            event.preventDefault();
            event.stopPropagation();

            tags = $('.the-tags'),
            newtag = $('#tag_id');
            text = newtag.val();
            tagsval = tags.val();
            newtags = tagsval ? tagsval + tagDelimiter + text : text;

            newtags = array_unique_noempty( newtags.split( tagDelimiter ) ).join( tagDelimiter );
            tags.val( newtags );
            newtag.val('');

            current_tags = tags.val().split( tagDelimiter );

            createTags(current_tags);
        }
    })

    function createTags(currentTags)
    {
        tagchecklist.empty();
        $.each( currentTags, function( key, val ) {
            var listItem, xbutton;
            val = $.trim( val );

            if (val != '') {
                
                listItem = $( '<li />' ).text( val );
                var xbutton = $( '<button type="button" id="post_tag-check-num-' + key + '" class="ntdelbutton">' + '<span>×</span></button>'+ listItem.html() );
        
                listItem.prepend( '&nbsp;' ).prepend( xbutton );
                tagchecklist.append(listItem);

                //$('#tag_id').val('');
            }
        });
    }

    // remove tags
    $(document).on('click', '.ntdelbutton', function(){
        parseTags(this);
    });

    function parseTags(el) {
        var id = el.id,
            num = id.split('post_tag-check-num-')[1],
            current_tags = $('.the-tags').val().split( tagDelimiter ),
            new_tags = [];
        delete current_tags[num];

        // Sanitize the current tags and push them as if they're new tags.
        $.each( current_tags, function( key, val ) {
            val = $.trim( val );
            if ( val ) {
                new_tags.push( val );
            }
        });

        $('.the-tags').val( new_tags.join( tagDelimiter ));

        createTags(current_tags);
        return false;
    }

});
</script>
@endsection

@endsection

@section('scripts')
    <script type="text/javascript">

        function loadFile(event,id){

            $('#'+id).show();

            var reader = new FileReader();

            reader.onload = function(){

                var output = document.getElementById(id);

                output.src = reader.result;
            };

            reader.readAsDataURL(event.files[0]);
        }

    </script>
@endsection
