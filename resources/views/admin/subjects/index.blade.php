@extends('layouts.admin')

@section('title',tr('subjects'))

@section('content-header',tr('subjects'))

@section('breadcrumb')

	<li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
	<li class="active">{{tr('subjects')}}</li>

@endsection

@section('content')

	@include('notification.notify')

	<div class="row">
		<div class="col-lg-12">
			<div class="box box-primary">
				<div class="box-header label-primary">
					<b style="font-size: 18px;">{{tr('subjects')}}</b>
					<a href="{{route('admin.add.subject')}}" class="btn btn-default pull-right">{{tr('add_subject')}}</a>
				</div>
				<div class="box-body">

					@if(count($subjects) > 0)
						
						<table id="datatable-withoutpagination" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>{{tr('id')}}</th>
									<th>{{tr('subject')}}</th>
									<th>{{tr('status')}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($subjects as $i=>$value)
								<tr>
									<td>{{showEntries($_GET, $i+1)}}</td>
									<td><a href="{{route('admin.coupon.view',$value->id)}}">{{$value->subject}}</a>
										<ul class="table-row-actions">

												<li role="presentation">
													<a class = "menuitem"  tabindex= "-1" href="{{route('admin.edit.subject',$value->id)}}">{{tr('edit')}}</a>
												</li>

												<li role="presentation">
													<a class="menuitem" tabindex="-1" href="{{route('admin.delete.subject',$value->id)}}" onclick="return confirm('Are You Sure?')">{{tr('delete')}}</a>
												</li>

												<li role="presentation">
													@if($value->status == 0)
													<a class="menuitem" tabindex="-1" href="{{route('admin.subject.update',['id'=>$value->id,'status'=>1])}}" onclick="return confirm('Are You Sure?')">{{tr('approve')}} </a>
													@else
													<a class="menuitem" tabindex="-1" href="{{route('admin.subject.update',['id'=>$value->id,'status'=>0])}}" onclick="return confirm('Are You Sure')">{{tr('decline')}}</a>
													@endif
												</li>
										</ul>
											
									</td>
									
									<td>
										@if($value->status ==0)
										<span class="label label-warning">{{tr('declined')}}</span>
										@else
										<span class="label label-success">{{tr('approved')}}</span>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						
						</table>
						
						<div align="right" id="paglink"><?php echo $subjects->links(); ?></div>

					@else
						<h3 class="no-result">{{tr('subject_result_not_found_error')}}</h3>
					@endif
				</div>
			</div>
		</div>
	</div>


@endsection

