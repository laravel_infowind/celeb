@extends('layouts.admin')

@section('title',tr('edit_coupon'))

@section('content-header',tr('edit_coupon'))

@section('breadcrumb')

	<!-- <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>

	<li><a href="{{route('admin.coupon.list')}}"><i class="fa fa-gift"></i>{{tr('coupons')}}</a></li>

	<li class="active">{{tr('edit_coupon')}}</li> -->

@endsection

@section('content')

	@include('notification.notify')



<div class="main-content">
    <form action="{{route('admin.save.subject')}}" method="POST" class="form-horizontal" role="form">
	<input type="hidden" name="id" value="{{$edit_subject->id}}">
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('edit_coupon')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="form-body">
                                    
									<div class="form-group">
										<label for="title">{{tr('subject')}}*</label>
										<input type="text" name="subject" role="title" min="5" max="20" class="form-control" placeholder="Enter subject" value="{{$edit_subject->subject ?$edit_subject->subject : old('subject') }}">
									</div>
                                    <div class="form-group" style="width: 30%">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </form>

</div>

@endsection