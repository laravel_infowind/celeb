@extends('layouts.admin')

@section('title', tr('tags'))

@section('content-header', tr('tags'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-tag"></i> {{tr('tags')}}</li>
@endsection

@section('content')

	@include('notification.notify')

	<div class="row">
        <div class="col-lg-4">
            <form class="form-horizontal" action="{{route('admin.save.tag')}}" method="POST" enctype="multipart/form-data" role="form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="boxx">
                            <div class="box-bodyy">
                                <div class="card mb-20">
                                    <div class="card-header">
                                        <label for="title">{{tr('add_tag')}}</label>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-body">
        
                                            <div class="form-group">
                                                <label for="name">{{tr('name')}}*</label>
                                                <input type="text" required class="form-control" pattern = "[a-zA-Z0-9\s\-\.]{2,100}" title="{{tr('only_alphanumeric')}}" id="name" name="name" placeholder="{{tr('tag_name')}}">
                                            </div>

                                            <button type="submit" class="btn btn-primary">Add New Tag</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-4">
        
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="">Publish</label>
                            </div>
                            <div class="card-body">
                                <div class="publish_card">
                                {!! App\Helpers\Helper::published_block($tag) !!}
        
                                
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="publish_item">
                                    <div class="box-tools pull-left" style="width: 30%">
                                        <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                        @if(Setting::get('admin_delete_control'))
                                            <a href="#" class="btn btn-primary" disabled>Publish</a>
                                        @else
                                            <button type="submit" class="btn btn-primary">Publish</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div> --}}
                </div>
            </form>
        </div>
        <div class="col-lg-8">
          	<div class="box box-primary">
	          	<div class="box-header label-primary">
	                <b style="font-size:18px;">{{tr('tags')}}</b>
	                <a href="{{route('admin.add.tag')}}" class="btn btn-default pull-right">{{tr('add_tag')}}</a>
	            </div>
	            
	            <div class="box-body">
                    <div class="search-table-top"> 
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="dataTables_length">
                                            <label>Show
                                                <form method="get" action="{{ url('admin/tags') }}" class="videos-change-pagination">
                                                    <input type="hidden" name="column" value="{{ $column }}">
                                                    <input type="hidden" name="order_by" value="<?= (isset($_GET['order_by']) && ($_GET['order_by'] == 'asc')) ? 'asc' : 'desc'; ?>">
                                                    <input type="hidden" name="search" value="{{ $tag_name }}">


                                                    <select name="per_page" aria-controls="datatable" class="form-control input-sm"  onchange="this.form.submit()">
                                                    <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 10)) ? 'selected' : ''; ?> value="10">10</option>
                                                    <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 25)) ? 'selected' : ''; ?> value="25">25</option>
                                                    <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 50)) ? 'selected' : ''; ?> value="50">50</option>
                                                    <option <?= (isset($_GET['per_page']) && ($_GET['per_page'] == 100)) ? 'selected' : ''; ?> value="100">100</option>
                                                </select>
                                            </form>
                                        entries</label>
                                    </div>
                                </div>



                                <div class="col-sm-6">
                                    <form action="{{route('admin.tags')}}" class="form">
                                        <div class="input-group">
                                            <input type="text" placeholder="Search by Name" class="form-control input-video-search" name="search" value="{{session('tag_search')}}">
                                            <div class="input-group-prepend">
                                                <button type="submit" class="btn btn-success">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

	            	@if(count($tags) > 0)

		              	<table id="datatable-withoutpagination" class="table table-bordered table-striped">

							<thead>
							    <tr>
                                    <th>
                                        <a href="{{ url('admin/tags?column=name&order_by='.$asc_or_desc.'&per_page='.$per_page.'&search='.$tag_name) }}">{{tr('name')}} <i class="fa fa-sort<?php echo $column == 'name' ? '-' . $up_or_down : ''; ?>"></i></a>
                                    </th>
							      <th>Slug</th>
							      <th>Count</th>
                                </tr>
							</thead>

							<tbody>
								@foreach($tags as $i => $tag)

								    <tr>
								      	<td>{{$tag->name}}

								      		<ul class="table-row-actions"> 
	            								
						                  	<li role="presentation">
                                                @if(Setting::get('admin_delete_control'))
                                                    <a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('edit')}}</a>
                                                @else
                                                    <a role="menuitem" tabindex="-1" href="{{route('admin.edit.tag' , array('id' => $tag->id))}}">{{tr('edit')}}</a>
                                                @endif
                                            </li>
                                            
						                  	<li role="presentation">

							                  	@if(Setting::get('admin_delete_control'))

								                  	<a role="button" href="javascript:;" class="btn disabled" style="text-align: left">{{tr('delete')}}</a>

								                @else

						                  			<a role="menuitem" tabindex="-1" onclick="return confirm(&quot;{{tr('tag_delete_confirmation' , $tag->name)}}&quot;);" href="{{route('admin.delete.tag' , array('tag_id' => $tag->id))}}">{{tr('delete')}}</a>
						                  		@endif
						                  	</li>
                                        </ul>
								      	</td>
								      	
		                            	<td>
                                            {{$tag->slug}}
		                            	</td>
		                            	<td>
                                            <?php $count_videos = App\Helpers\Helper::countTagVideos($tag->id);
                                            echo '<a href="'.url('admin/content/view?tag=').$tag->id.'">'. $count_videos .'</a>';
                                            ?>

		                            	</td>
								       
								    </tr>
								@endforeach
							</tbody>
						
						</table>

						<div align="right" id="paglink"><?php echo $tags->links(); ?></div>


					@else
						<h3 class="no-result">{{tr('no_result_found')}}</h3>
					@endif
	            
	            </div>

          	</div>
        </div>
    </div>

@endsection
