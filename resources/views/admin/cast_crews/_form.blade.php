@section('styles')
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<link rel="stylesheet"
    href="{{asset('admin-css/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('admin-css/plugins/iCheck/all.css')}}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
    .container-narrow {
        margin: 150px auto 50px auto;
        max-width: 728px;
    }

    canvas {
        width: 100%;
        height: auto;
    }

    span.select2-container {
        width: 100% !important;
    }

    .example-wizard.panel {
        border: 0px solid white;
        padding: 20px;
        display: inline-block;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        margin-bottom: 10px;
        width: 30%;
    }

    .select2-container .select2-search--inline {
        border: 1px solid #d2d6df !important;
        width: 30%;
    }
</style>
@endsection
<!-- popup -->
<div class="modal fade error-popup" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="media">
                    <div class="media-left">
                        <img src="{{asset('images/warning.jpg')}}" class="media-object" style="width:60px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Information</h4>
                        <p id="error_messages_text"></p>
                    </div>
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-primary top" data-dismiss="modal">Okay</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content">
    <form class="form-horizontal" action="{{route('admin.cast_crews.save')}}" method="POST" enctype="multipart/form-data" role="form">
        <input type="hidden" name="id" value="{{$model->id}}">
        <input type="hidden" name="person_id" id="person_id" value="{{ ($model->id) ? $model->cast_id : 0 }}">
        <input type="hidden" name="profile_pic" id="celeb_profile_pic">
        <input type="hidden" name="gender" id="celeb_gender" value="{{ ($model->id) ? $model->gender : '' }}">
        @if(!$model->id)
        <input type="hidden" name="popularity" id="celeb_popularity">
        <input type="hidden" name="adult" id="celeb_adult">
        <input type="hidden" name="imdb_id" id="celeb_imdb_id">
        <input type="hidden" name="cast_credits" id="cast_credits">
        <input type="hidden" name="crew_credits" id="crew_credits">
        <input type="hidden" name="homepage" id="homepage">
        <input type="hidden" name="deathday" id="deathday">
        @endif
        
        <div class="row">
            <div class="col-md-8">
                <div class="boxx">
                    <div class="box-bodyy">
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="title">{{tr('name')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span> </label>
                            </div>
                            <div class="card-body">
                                @if(!$model->id)
                                <div class="publish_card">
                                    <div class="publish">
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" class="search-type" checked id="tmdb"
                                                value="tmdb" name="tmdb">
                                            <label for="tmdb" onclick="checkType('tmdb')">
                                                {{tr('tmdb')}} </label>
                                        </div>
                                        <div class="radio radio-primary radio-inline">
                                            <input type="radio" class="search-type form-control"
                                                onclick="checkType('custom')" id="custom" value="custom" name="tmdb">
                                            <label for="custom"> {{tr('custom')}} </label>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                
                                <div class="publish_card">
                                    <input class="form-control" style="background-color: #fff !important;" required type="text" onkeyup="canonical(this.value)" name="name" value="{{$model->name}}" id='people' autocomplete="off">

                                    @if(!$model->id)
                                    <div id="tmdb_btn">
                                        <button style="margin-top:5px; height: 34px; border-radius:0"
                                            title="Click to search on {{tr('tmdb')}}" type="button"
                                            class="btn btn-primary btn-sm" id="seacrh_tmdb"
                                            onclick="getCelebrityList()">Search
                                            {{tr('tmdb')}}</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="description">Position <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                            </div>
                            <div class="card-body">
                                <div class="position">
                                    <?PHP 
                                      $positions = explode(',',$model->position);
                                    ?>
                                    <div class="checkbox checkbox-inline checkbox-primary">
                                        <input type="checkbox" value="1" @if(in_array(1,$positions)) checked @endif name="position[]" id="actor">
                                        <label for="actor">Actor</label>
                                    </div>
                                    <div class="checkbox checkbox-inline checkbox-primary">
                                        <input type="checkbox" value="2" @if(in_array(2,$positions)) checked @endif name="position[]" id="director">
                                        <label for="director">Director</label>
                                    </div>
                                    <div class="checkbox checkbox-inline checkbox-primary">
                                        <input type="checkbox" value="3" @if(in_array(3,$positions)) checked @endif name="position[]" id="writer">
                                        <label for="writer">Writer</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="description">Known for Department <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                            </div>
                            <div class="card-body">
                                <div class="position">
                                    <select id="position" name="known_for_department" class="form-control">
                                        <option value="Acting" @if($model->known_for_department=='Acting') selected @endif >Acting</option>
                                        <option value="Directing" @if($model->known_for_department=='Directing') selected @endif >Directing</option>
                                        <option value="Writing" @if($model->known_for_department=='Writing') selected @endif >Writing</option>
                                        <option value="Production" @if($model->known_for_department=='Production') selected @endif >Production</option>
                                        <option value="Sound" @if($model->known_for_department=='Sound') selected @endif >Sound</option>
                                        <option value="Crew" @if($model->known_for_department=='Crew') selected @endif >Crew</option>
                                        <option value="Creator" @if($model->known_for_department=='Creator') selected @endif >Creator</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="description">{{tr('description')}} <span class="asterisk"><i class="fa fa-asterisk"></i></span></label>
                            </div>
                            <div class="card-body">
                                <div class="detail_description_editor">
                                    <textarea class="overview" id="biography" name="biography" rows="4" required="">{!! (!empty($model->biography)) ? $model->biography : '' !!}</textarea>
                                </div>
                            </div>
                        </div>
                        <script src="{{ url('ckeditor-full/ckeditor.js')}}"></script>
                        <script>
                            CKEDITOR.replace( 'biography' , {
                                extraPlugins: 'colorbutton,colordialog',
                                allowedContent: true
                            }); 
                        </script>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="gender">{{tr('gender')}}</label>
                            </div>
                            <div class="card-body">
                                <div class="">
                                    <input id="male_radio" type="radio" value="2" name="gender" {{ ((!empty($model->gender)) && $model->gender == 2) ? 'checked' : '' }} onchange="changeGender(this.value)"/> Male
                                    <input id="female_radio" type="radio" value="1" name="gender" {{ ((!empty($model->gender)) && $model->gender == 1) ? 'checked' : '' }} onchange="changeGender(this.value)"/>
                                    Female
                                </div>
                            </div>
                        </div>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="celeb_birthday">Birthday</label>
                            </div>
                            <div class="card-body">
                                <div class="publish_card mb-20">
                                    <input required="" type="date" class="form-control"
                                        id="celeb_birthday" name="celeb_birthday" value="{{ (!empty($model->birthday)) ?  $model->birthday : '' }}">
                                </div>
                            </div>
                        </div>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="celeb_place_of_birth">Place of Birth</label>
                            </div>
                            <div class="card-body">
                                <div class="publish_card mb-20">
                                    <input class="form-control" type="text" name="celeb_place_of_birth"
                                        value="{{ (!empty($model->place_of_birth)) ?  $model->place_of_birth : '' }}" id='celeb_place_of_birth' required="">
                                </div>
                            </div>
                        </div>

                        <div class="card mb-20">
                            <div class="card-header">
                                <label for="alternative_titles">Also Known As</label>
                            </div>
                            <div class="card-body">
                                <div class="detail_description_editor">
                                    <textarea id="celeb_known_as" class="form-control celeb_known_as"
                                        name="celeb_known_as" rows="4"><?php if(!empty($model->also_known_as) && $model->id) {
                                        $aka_string = '';
                                        if (isJson($model->also_known_as)) {
                                                $json_aka = json_decode($model->also_known_as, true);
                                                if (!empty($json_aka)) {
                                                    foreach ($json_aka as $key => $value) {
                                                        $aka_string .= $value.', ';
                                                    }
                                                } else {
                                                    $aka_string = '';
                                                }
                                            } else {
                                                $aka_string = $model->also_known_as;
                                            }
                                        echo rtrim($aka_string, ', ');
                                    }?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Publish</label>
                    </div>
                    <div class="card-body">
                        <div class="publish_card">
                            {!! App\Helpers\Helper::published_block($model) !!}
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="publish_item">
                            <div class="box-tools pull-left" style="width: 30%">
                                <!-- <a href="" class="btn btn-danger">{{tr('cancel')}}</a> -->
                                @if(Setting::get('admin_delete_control'))
                                    <a href="#" class="btn btn-primary" disabled>{{ (!$model->id) ? 'Publish' : 'Update' }}</a>
                                @else
                                    <button type="submit" class="btn btn-primary">{{ (!$model->id) ? 'Publish' : 'Update' }}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-20">
                    <div class="card-header">
                        <label for="">Upload Image</label>
                    </div>
                    <div class="card-body">
                        <div class="image-upload">
                            <div class="image-upload-item">
                                
                                <div class="image-upload-item">
                                    <input type="hidden" name="poster" id="poster">
                                    <input type="file" id="default_image" accept="image/png, image/jpeg, image/jpg" name="default_image" placeholder="{{tr('profile_image')}}" style="display:none"
                                        onchange="loadFile(this,'default_img')">
                                    <input type="hidden" name="old_profile_image" id="old_profile_image" value="{{ $model->profile_image }}">
                                    
                                    <img src="{{($model->id && $model->profile_path) ? $model->profile_path : url('images/default.png')}}"
                                        onclick="$('#default_image').click();return false;" id="default_img"
                                        style="width: 100%; height: auto; margin-bottom: 0px; display: block; margin: 0 auto">

                                </div>
                                <p class="help-block">{{tr('image_validate')}} {{tr('image_square')}}</p>
                        
                            </div>
                            @if(!empty(Request::get('id') ))
                            @if($model->is_slider)
                            <div class="banner_status_{{$model->id}}">  
                                <span onclick="change_banner_status('{{$model->id}}', 1)" class="label label-success btn">Yes</span>
                            </div>
                            @else
                            <div class="banner_status_{{$model->id}}">  
                                <span onclick="change_banner_status('{{$model->id}}', 0)" class="label label-danger btn">No</span>
                            </div>
                            @endif
                            @endif
                            
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
        <div class="row">
        <div class="col-lg-8">
        @seoForm($model)
        </div>
        </div>
    </form>

</div>


<div class="overlay">
    <div id="loading-img"></div>
</div>

<div class="modal fade" id="data-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Peoples</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumbnail</th>
                            <th>Celebrity Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="celebrity-data">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="">
                    <button class="tmdb_btn_prev pagination_btn btn btn-primary" onclick="getCelebrityList('prev')"
                        prev_page="0">Prev</button>
                    <button class="tmdb_btn_next pagination_btn btn btn-primary" onclick="getCelebrityList('next')"
                        next_page="2">Next</button>
                    <button type="button" class="pagination_btn btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
@section('scripts')
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/moment.min.js')}}"></script>
<script src="{{asset('admin-css/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>


    function canonical(title){
        var canonical_url = title;
        $.ajax({
        type:'POST',
        url:'{{ url("admin/person/seo-canonical")}}',
        data: {
            canonical_url:canonical_url,
            _token: '<?php echo csrf_token(); ?>',
            video_id: '<?php echo $model->id; ?>'
        },
        success: function(data){
                $('#page_canonical_url').val(data);
                $('input[name="meta[7]"]').val(data);
            },
            error: function(){
                alert('Something wrong with request');
            }
            
        });
     } 

    var page = 1;
    var total_page;
    var TMDB_KEY = "{{env('TMDB_API_KEY')}}";
    
    function checkType(type) {
        if (type == 'custom') {
            $('#tmdb_btn').hide();
        } else if (type == 'tmdb') {
            $('#tmdb_btn').show();
        }
    }

    function changeGender(value) {
        if (value == '1') {
            $('#celeb_gender').val(value);
        } else if (value == '2') {
            $('#celeb_gender').val(value);
        }
    }
    
    function getCelebrityList(type) {
    
        if (!$('#people').val()) {
            $('#people').closest('span').addClass('help-block error-help-block alert-danger').html('The field is required.');
        }
    
        if (type == 'prev') {
            page = page - 1;
        } else if (type == 'next') {
            page = page + 1;
        }
    
        var celeb_name = $('#people').val();
        if (celeb_name != '') {
            $('#seacrh_tmdb').prop('disabled', true);
            $('#seacrh_tmdb').text('Searching...');
            $.ajax({
                type: 'get',
                url: "https://api.themoviedb.org/3/search/person?api_key=" + TMDB_KEY + "&query=" + celeb_name + "&page=" + page,
                dataType: 'json',
                success: function(data) {
                    total_page = data.total_pages;
                    if (page == 1) {
                        $('.tmdb_btn_prev').prop('disabled', true);
                    } else {
                        $('.tmdb_btn_prev').prop('disabled', false);
                    }
                    if (page == total_page) {
                        $('.tmdb_btn_next').prop('disabled', true);
                    } else {
                        $('.tmdb_btn_next').prop('disabled', false);
                    }
                    var str = '';
                    if (data.results.length > 0) {
                        for (var i = 0; i < data.results.length; i++) {
                            if (data.results[i].profile_path) {
                                var image = "https://image.tmdb.org/t/p/w300" + data.results[i].profile_path;
                            } else {
                                var image = "{{url('images/default.png')}}";
                            }
                            str = str + "<tr><td>" + ((20 * (page - 1)) + (i + 1)) + "</td>" +
                                "<td><img class='img-thumbnail' src='" + image + "' width='60' height='60'></td>" +
                                "<td>" + data.results[i].name + "</td>" +
                                "<td><button type='button' onclick = 'getCelebrityDetail(" + data.results[i].id + ",`"+ data.results[i].name + "`)' id='38646' class='btn get-movie-data btn-sm btn-primary'>Select</button></td></tr>";
                        }
                    } else {
                        str = str + "<tr>" +
                            "<td colspan='4'>'No Celebrity Found'</td></tr>";
    
                    }
    
                    $('#celebrity-data').html(str);
                    console.log(data)
                    $('#data-modal').modal('show');
                    $("#data-modal").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#seacrh_tmdb').prop('disabled', false);
                    $('#seacrh_tmdb').text('Search');
                }
            });
        }
    }
    
    function getCelebrityDetail(celebrityId,name) {
        if(name){
            canonical(name)
        }
        $('.close').trigger('click');
        $.ajax({
            type: 'get',
            url: "https://api.themoviedb.org/3/person/" + celebrityId + "?api_key=" + TMDB_KEY + "&language=en-US&append_to_response=combined_credits",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                var department = data.known_for_department;
                $('#person_id').val(data.id);
                $('#people').val(data.name);
                $('#celeb_birthday').val(data.birthday);
                $('#celeb_place_of_birth').val(data.place_of_birth);
                $('textarea#biography').val(data.biography.replace(/\n/g, "<br/>"));
                CKEDITOR.instances.biography.setData(data.biography.replace(/\n/g, "<br/>"));
                $('#known_for_department').val(data.known_for_department);
                $('#celeb_gender').val(data.gender);
                $('#celeb_popularity').val(data.popularity);
                $('#celeb_imdb_id').val(data.imdb_id);
                $('#celeb_adult').val(data.adult);
                $('#celeb_profile_pic').val(data.profile_path);
                $('#homepage').val(data.homepage);
                $('#deathday').val(data.deathday);
                $('#page_title').val(data.name);
                $('#page_description').val(data.biography);
                if (department == 'Acting') {
                    $("#position>option:eq(0)").prop('selected', true);
                } else if (department == 'Directing') {
                    $("#position>option:eq(1)").prop('selected', true);
                } else if (department == 'Writing') {
                    $("#position>option:eq(2)").prop('selected', true);
                } else if (department == 'Production') {
                    $("#position>option:eq(3)").prop('selected', true);
                } else if (department == 'Sound') {
                    $("#position>option:eq(4)").prop('selected', true);
                } else if (department == 'Crew') {
                    $("#position>option:eq(5)").prop('selected', true);
                } else if (department == 'Creator') {
                    $("#position>option:eq(6)").prop('selected', true);
                }
                if (data.gender == 2) {
                    $("#male_radio").attr('checked', true);
                } else if (data.gender == 1) {
                    $("#female_radio").attr('checked', true);
                }
                if (data.also_known_as.length) {
                    var aka = '';
                    $.each(data.also_known_as, function(i, name) {
                        aka += name + ', ';
                    });
                    aka = aka.replace(/,\s*$/, "");
                    $('#celeb_known_as').val(aka);
                } else {
                    $('#celeb_known_as').val('');
                }
    
                if (data.profile_path) {
                    var image = "https://image.tmdb.org/t/p/w300" + data.profile_path;
                } else {
                    var image = "{{url('images/default.png')}}";
                }
    
                if (data.profile_path) {
                    $('#poster').val(image);
                    $('img#default_img').attr('src', image);
                } else {
                    $('img#default_img').attr('src', '{{url("images/default.png")}}');
                    $('#poster').val('');
                }
                $('.error-help-block').hide();

                // load cast crew
                if (data.combined_credits.cast.length > 0) {
                    var cast = JSON.stringify(data.combined_credits.cast);
                    // $.each(data.combined_credits.cast, function(i, actor) {
                    //     if (i == 0)
                    //         actors += actor.id + ':' + actor.name;
                    //     else
                    //         actors += '|' + actor.id + ':' + actor.name;
                    // });
                    $('#cast_credits').val(cast);
                } else {
                    $('#cast_credits').val('');
                }

                if (data.combined_credits.crew.length > 0) {
                    var crew = '';
                    var cast = JSON.stringify(data.combined_credits.crew);
                    // $.each(data.combined_credits.crew, function(i, actor) {
                    //     if (i == 0)
                    //         actors += actor.id + ':' + actor.name;
                    //     else
                    //         actors += '|' + actor.id + ':' + actor.name;
                    // });
                    $('#crew_credits').val(crew);
                } else {
                    $('#crew_credits').val('');
                }
    
            }
        });
    }

    // on change position change known_for_department
    $('#position').on('change', function(){
        var department = $(this).val();
        if (department == '1') {
            $("#known_for_department").val('Acting');
        } else if (department == 'Directing') {
            $("#known_for_department").val('Acting');
        } else if (department == 'Writing') {
            $("#known_for_department").val('Writing');
        } else if (department == 'Production') {
            $("#known_for_department").val('Production');
        } else if (department == 'Sound') {
            $("#known_for_department").val('Sound');
        } else if (department == 'Crew') {
            $("#known_for_department").val('Crew');
        } else if (department == 'Creator') {
            $("#known_for_department").val('Creator');
        }
    });
    
    // onchange default image poster will be blank
    $('#default_image').on('change', function() {
        $('#poster').val('');
    });
    
    $("#profile").change(function() {
        readURL(this);
    });
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.profile-user-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    
    function removeFile(index) {
        console.log(index);
        delete uploadedFiles[index];
        $('#file_' + index).remove();
    }
    // published on timestamp
    $(document).ready(function() {
        $(document).on('click', '.edit-timestamp', function() {
            $(this).hide();
            $('#timestampdiv').slideDown('fast');
        });
    
        $(document).on('click', '.cancel-timestamp', function() {
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    
        $(document).on('click', '.save-timestamp', function() {
            var today_date = new Date();
            var aa = $('#aa').val(),
                mm = $('#mm').val(),
                jj = $('#jj').val(),
                hh = $('#hh').val(),
                mn = $('#mn').val(),
                newD = new Date(aa, mm - 1, jj, hh, mn);
    
            event.preventDefault();
    
            if (newD.getFullYear() != aa || (1 + newD.getMonth()) != mm || newD.getDate() != jj || newD.getMinutes() != mn) {
                $('.timestamp-wrap').addClass('form-invalid');
                return;
            } else {
                $('.timestamp-wrap').removeClass('form-invalid');
            }
    
            var month = $('#mm option[value="' + mm + '"]').attr('data-text');
            var date = parseInt(jj, 10);
            var year = aa;
            var hour = ('00' + hh).slice(-2);
            var min = ('00' + mn).slice(-2);
    
            var date_string = month + ' ' + date + ',' + ' ' + year + ' at ' + hour + ':' + min;
            var publish_time = year + '-' + (1 + newD.getMonth()) + '-' + date + ' ' + hour + ':' + min + ':00';
            if (today_date >= newD) {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(1);
                $('#timestamp').html('<label>Published on:' + ' <span class="published_on">' + date_string + '</span></label>');
            } else {
                $('#publish_time').val(publish_time);
                $('#publish_type').val(2);
                $('#timestamp').html('<label>Scheduled for:' + ' <span class="published_on">' + date_string + '</span></label>');
            }
    
            // Move focus back to the Edit link.
            $('#timestampdiv').slideUp('fast');
            $('.edit-timestamp').show();
        });
    });
    
    $(".save_people").on('click', function(e) {
        e.preventDefault();
        var err = '';
    
        var person_id = $("#person_id").val();
        var name = $("#people").val();
        var poster = $("#poster").val();
        var known_for_department = $("#known_for_department").val();
        //var celeb_place_of_birth = $("#celeb_place_of_birth").val();
        //var celeb_birthday = $("#celeb_birthday").val();
        var biography = CKEDITOR.instances.biography.getData();
        var default_image = document.getElementById("default_image").files.length;
    
        if (name == '') {
            err = "Name should not be blank";
        }
    
        if (biography == '' && err == '') {
            err = "Description should not be blank";
        }
    
        if (known_for_department == '' && err == '') {
            err = "Department should not be blank";
        }
    
        if (!person_id || person_id <= 0 || person_id == null) {
            if (default_image <= 0 && err == '' && poster == '') {
    
                err = "Please Choose Default Image.";
    
            }
        }
    
        if (err) {
    
            $("#error_messages_text").html(err);
    
            $("#error_popup").click();
    
            return false;
        }
    
        var image = true;
    
        if (!err && biography && image) {
            var formdata = new FormData($('#save_people_form')[0]);
            formdata.append('biography', biography);
            formdata.append('profile', $('#profile').val());
            formdata.append('default_image', default_image);
            $('.save_people').prop('disabled', true);
            $('#add_celebrity_loader').show();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                async: false,
                type: 'post',
                processData: false,
                contentType: false,
                url: "{{url('admin/people/save')}}",
                data: formdata,
                dataType: 'json',
                success: function(data) {
                    $('.save_people').prop('disabled', false);
                    if (data.success) {
                        //toastr.success(data.message);
                        //$('#save_people').reset();
                        setTimeout(function() {
                            window.location = "{{url('admin/celebs/create')}}"
                        }, 1000)
                    } else {
                        //toastr.error(data.message);
                    }
                    // $('#add_celebrity_loader').hide();
                },
                error: function() {
                    $('.save_people').prop('disabled', false);
                }
            });
        }
    });
    
    function loadFile(event, id) {
        // alert(event.files[0]);
        var reader = new FileReader();
    
        reader.onload = function() {
            var output = document.getElementById(id);
            // alert(output);
            output.src = reader.result;
            //$("#imagePreview").css("background-image", "url("+this.result+")");
        };
        reader.readAsDataURL(event.files[0]);
    }

    function change_banner_status(video_id, status)
    {
	$.ajax({
		type:'POST',
		url:'{{ url("admin/videos/change-banner-status")}}',
		data: {
			video_id: video_id,
			status: status,
			_token: 'ztv35zmBDoEsw5PvGxkTRAftTZ7kHq5VX8mZKoQh'
		},
		success: function(res){
			if(res.data != '') {
				$('.banner_status_'+res.video_id).html(res.data);
			} else {
				alert('Something wrong with request');
			}
		},
		error: function(){
			alert('Something wrong with request');
		}
	});
}
   </script>

</script>
@endsection