@extends('layouts.admin.focused')

@section('title', tr('login'))

<style type="text/css">
body {
    background: #000 !important; 
}
</style>

@section('content')

    <div class="login-box-body" style="height:275px;">


        <form class="form-layout" role="form" method="POST" action="{{ url('/admin/login') }}">
            {{ csrf_field() }}

            <div class="login-logo">
               <input type="hidden" name="timezone" value="" id="userTimezone">
            </div>

            <p class="text-center mb30"></p>

            <div class="form-inputs">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                    <input type="email" class="form-control input-lg" value="{{Setting::get('demo_admin_email')}}" name="email"  required placeholder="{{tr('email')}}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <input type="password" class="form-control input-lg" value="{{Setting::get('demo_admin_password')}}" required name="password" placeholder="{{tr('password')}}" >

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                </div>

                <div class="form-group">
                    <div class="form-checkbox">
                        <input type="checkbox" class="form-control-input" id="formCheck" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-control-label" for="formCheck">Remember me</label>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-md-4">
                    <button class="btn btn-success btn-block mb15" type="submit">
                        <span><i class="fa fa-btn fa-sign-in"></i> {{tr('login')}}</span>
                    </button>
                </div>
                <div class="col-md-8">
                    @if (Route::has('admin.password.request'))
                        <div class="text-right">
                            <a class="btn btn-block btn-warning text-white" href="{{ route('admin.password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                    @endif
                </div>
            </div>

            <div class="clearfix"></div>

        </form>

    </div>

@endsection

@section('scripts')

<script src="{{asset('assets/js/jstz.min.js')}}"></script>
<script>
    
    $(document).ready(function() {

        var dMin = new Date().getTimezoneOffset();
        var dtz = -(dMin/60);
        $("#userTimezone").val(jstz.determine().name());
    });

</script>

@endsection

