@extends('layouts.admin')

@section('title', tr('view_videos'))

@section('content-header')

{{tr('view_videos')}}

@endsection

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-video-camera"></i> {{tr('view_videos')}}</li>
@endsection



@section('content')

@endsection