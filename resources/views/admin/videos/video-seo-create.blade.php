@extends('layouts.admin')
@section('title', tr('view_videos'))
@section('content-header')
SEO Video
@endsection
@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li><a href="{{route('admin.videos')}}"><i class="fa fa-video-camera"></i> {{tr('videos')}}</a></li>
    <li class="active"><i class="fa fa-video-camera"></i> {{tr('video')}}</li>
@endsection

@section('content')
	<style type="text/css">
		.seo-panel {
		    padding: 20px;
		    background: #fff;
		}
		.seo-panel .image{
			width: 75px;
			display: inline-block;
		}
		.seo-panel .image img{
			width: 100%;
		}
		.seo-panel .name{
			display: inline-block;
			vertical-align: bottom;
		}
	</style>
	<div class="seo-panel">
	<div class="row">
		<div class="col-md-12">
			<form action="{{ url('admin/video/seo') }}" method="POST">
				@csrf
				<div class="row">
					<div class="col-md-12">
						<div class="image">
							<img src="{{ $video->default_image }}">
						</div>
						<div class="name">
							<h3>{{ $video->title }}</h3>
						</div>
					</div>
				</div>

				<input type="hidden" name="video_id" value="{{ $video->id }}">
				<div class="form-group" style="margin-top: 50px;">
					<label>Title</label>
					<input type="text" name="title" class="form-control" value="{{ $seo == null ? $video->title : $seo->title }}" placeholder="SEO Title">
				</div>
				<div class="form-group">
					<label>Keywords</label>
                                        <input type="text" name="keywords" class="form-control" value="{{ $seo == null ? $video->title : $seo->keywords }}" placeholder="keyword 1, keyword 2, keyword 3">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea name="description" class="form-control" rows="5" placeholder="SEO Description">{{ $seo == null ? substr($video->description, 0, 275) : $seo->description }}</textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update SEO</button>
				</div>
			</form>
		</div>
	</div>
	</div>
	
@endsection
