<?php 
$post_date = ( $model->id ) ? $model->publish_time : date('Y-m-d H:i:s');
$jj        = ( $model->id ) ? date( 'd', strtotime($post_date)) : date( 'd' );
$mm        = ( $model->id ) ? date( 'm', strtotime($post_date)) : date( 'm' );
$aa        = ( $model->id ) ? date( 'Y', strtotime($post_date)) : date( 'Y' );
$hh        = ( $model->id ) ? date( 'H', strtotime($post_date)) : date( 'H' );
$mn        = ( $model->id ) ? date( 'i', strtotime($post_date)) : date( 'i' );
$ss        = ( $model->id ) ? date( 's', strtotime($post_date)) : date( 's' );

$multi = 0;
$cur_jj = date( 'd' );
$cur_mm = date( 'm' );
$cur_aa = date( 'Y' );
$cur_hh = date( 'H' );
$cur_mn = date( 'i' );

$month_names = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

$month = '<label><select id="mm" name="mm">';
for ( $i = 1; $i < 13; $i = $i + 1 ) {
    $monthnum  = str_pad($i, 2, '0', STR_PAD_LEFT);
    $monthtext = $month_names[$i-1];
    if ($monthnum == $mm) {
        $selected = 'selected';
    } else {
        $selected = '';
    }
    $month    .= '<option value="' . $monthnum . '" '. $selected .' data-text="' . $monthtext . '">';
    /* translators: 1: Month number (01, 02, etc.), 2: Month abbreviation. */
    $month .= sprintf( __( '%1$s-%2$s' ), $monthnum, $monthtext ) . "</option>\n";
}
$month .= '</select></label>';

$day    = '<label><input type="text" ' . ( $multi ? '' : 'id="jj" ' ) . 'name="jj" value="' . $jj . '" size="2" maxlength="2" autocomplete="off" /></label>';
$year   = '<label><input type="text" ' . ( $multi ? '' : 'id="aa" ' ) . 'name="aa" value="' . $aa . '" size="4" maxlength="4" autocomplete="off" /></label>';
$hour   = '<label><input type="text" ' . ( $multi ? '' : 'id="hh" ' ) . 'name="hh" value="' . $hh . '" size="2" maxlength="2" autocomplete="off" /></label>';
$minute = '<label><input type="text" ' . ( $multi ? '' : 'id="mn" ' ) . 'name="mn" value="' . $mn . '" size="2" maxlength="2" autocomplete="off" /></label>';

if ($model->id) {
    if(strtotime($model->publish_time) > time()) {
        $stamp = '<label>Scheduled for: <span class="published_on">'. date('Y-m-d', strtotime($model->publish_time)). ' at '. date('H:i', strtotime($model->publish_time)) .'</span></label>';
        $publish_time = $model->publish_time;
        $publish_type = 2;
    } else {
        $stamp = '<label>Published on: <span class="published_on">'. date('Y-m-d', strtotime($model->publish_time)). ' at '. date('H:i', strtotime($model->publish_time)) .'</span></label>';
        $publish_time = $model->publish_time;
        $publish_type = 1;
    }
} else {
    $stamp = '<label>Publish: <span class="published_on">immediately</span></label>';
    $publish_time = date('Y-m-d H:i:s');
    $publish_type = 1;
}
?>
<div class="publish_item mb-20">
    <span id="timestamp">
        {!! $stamp !!}
    </span>
    <a href="javascript:void(0)" class="edit-timestamp hide-if-no-js btn" role="button">
    <span aria-hidden="true">Edit</span>
    </a>
    <fieldset id="timestampdiv" class="hide-if-js" style="display: none;">
        <div class="timestamp-wrap">
            <?php /* translators: 1: Month, 2: Day, 3: Year, 4: Hour, 5: Minute. */
            printf( __( '%1$s %2$s, %3$s at %4$s:%5$s' ), $month, $day, $year, $hour, $minute ); ?>
        </div>
        <p>
            <a href="javascript:void(0)" class="save-timestamp hide-if-no-js button btn">OK</a>
            <a href="javascript:void(0)" class="cancel-timestamp hide-if-no-js button-cancel">Cancel</a>
        </p>
    </fieldset>
</div>
<input type="hidden" name="publish_time" id="publish_time" value="{{ $publish_time }}"/>
<input type="hidden" name="publish_type" id="publish_type" value="{{ $publish_type }}"/>