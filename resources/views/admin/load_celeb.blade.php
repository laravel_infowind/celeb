@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-primary" id="import_celeb">Import Celeb</button>
        </div>
        <div id ="result"></div>

        <ul class="batch_count"></ul>

        <table border="1px solid #000">
            <tbody id="reported">
                
            </tbody>
        </table>
    </div>


@endsection

@section('scripts')

<script type="text/javascript">

    $(document).ready(function() {
        $(document).on('click', '#import_celeb', function(){
            load_celeb(1);
        });
    });

function load_celeb(page = 1)
{
	$.ajax({
		type: 'GET',
		url: '?page=' + page,
        datatype: "json",
        beforeSend: function() {
            $('#import_celeb').attr('disabled', true);
        },
        success: function(res){
            $('#import_celeb').attr('disabled', false);
            console.log(res);
            if(res.success) {
                page++;
                if (res.current_page <= res.last_page) {
                    $('ul.batch_count').append('<li> Batch Count => ' + res.current_page + '</li>')
                    $('#reported').append('<tr><td>'+ res.reported +'</td></tr>');
                    load_celeb(page);
                } else {
                    $('#result').append(res.msg);
                    $('#reported').append('<tr><td>'+ res.reported +'</td></tr>');
                }
            } else {
                alert('Something wrong with request');
                $('#reported').append('<tr><td>'+ res.reported +'</td></tr>');
			}
		},
		error: function(){
            $('#import_celeb').attr('disabled', false);
			alert('Something wrong with request');
		}
	});
}
</script>

@endsection