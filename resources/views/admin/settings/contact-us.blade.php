@extends('layouts.admin')

@section('title', tr('contact_us_setting'))

@section('content-header', tr('contact_us_setting'))

@section('breadcrumb')
    <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard"></i>{{tr('home')}}</a></li>
    <li class="active"><i class="fa fa-money"></i> {{tr('contact_us_setting')}}</li>
@endsection

@section('content')

@include('notification.notify')

    <div class="row">

        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">

                    <h3 class="box-title">{{tr('contact_us_setting')}}</h3>

                </div>

                <section class="content">
                
	            <div class="main-content">
                <form action="{{(Setting::get('admin_delete_control') == 1) ? '' : route('admin.contact-us.save')}}" method="POST" enctype="multipart/form-data" role="form">
                            
                    <div class="box-body"> 
                        <div class="row"> 

                            <div class="col-md-12">
                                <h3 class="settings-sub-header text-uppercase"><b>Contact Us Setting</b></h3>
                                <hr>

                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="contact_email">E-mail Address</label>
                                    <input type="text" class="form-control" name="contact_email" id="vue_site_path" value="{{Setting::get('contact_email')}}">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="contact_mobile">Mobile</label>
                                    <input type="text" class="form-control" name="contact_mobile" id="vue_site_url" value="{{Setting::get('contact_mobile')}}">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="contact_address">Contact Address</label>
                                    <input type="text" class="form-control" name="contact_address" id="vue_site_name" value="{{Setting::get('contact_address')}}">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="contact_description">Contact Info</label>
                                    <textarea id="ckeditor" name="contact_description" class="form-control" required placeholder="{{tr('enter_text')}}">{{Setting::get('contact_description')}}</textarea>
                                </div>
                            </div> 
                        </div>
                    </div>
                          <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="reset" class="btn btn-warning">{{tr('reset')}}</button>

                        @if(Setting::get('admin_delete_control') == 1)
                            <button type="submit" class="btn btn-primary pull-right" disabled>{{tr('submit')}}</button>
                        @else
                            <button type="submit" class="btn bg-blue pull-right">{{tr('submit')}}</button>
                        @endif
                    </div>
                </form>
            </div>
          </section>
        </div>
    </div>

    </div>


@endsection