<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{Setting::get('site_name' , tr('site_name'))}}</title>
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/theme.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <link rel="shortcut icon" type="image/png" href="{{Setting::get('site_icon' , asset('img/favicon.png'))}}"/>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/responsive.css') }}">
</head>
<body>
<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas-content" data-off-canvas-content>
            <header class="error-header">
                <div class="error-logo">
                    <a href="{{url('/')}}"><img class="" src="{{Setting::get('site_logo')}}" alt="spark"></a>
                </div>
            </header>
            <section class="error-page-main" style="background: url('{{ Setting::get('home_page_bg_image') }}') no-repeat center center / cover">
                <div class="large-error-page">
                    <div class="large-centered">
                        <div class="error-page-content text-center" style="padding:0 !important">
                            <div class="error-img text-center">
                            </div>
                            <?php $get_content = get_error_page(); ?>

                            @if(!empty($get_content))
                                {!! $get_content->description !!}
                            @endif
                            {{-- <h2>4<span>0</span>4</h2>
                            <h1>{{tr('page_not_found')}}</h1>
                            <p>Sorry, what you were looking for is no longer available, but, we have plenty of other Titles for your viewing pleasure. Be sure to check them out, click the 'Go to Home page' button, enjoy!</p>
                            <a href="{{ url('/') }}" class="button">{{tr('go_back_home')}}</a> --}}
                            <!-- <div class="error-page-errorCode"><span>Error Code <strong>NSES-404</strong></span></div> -->
                        </div>
                    </div>
                </div>
            </section>
            @include('guest.include.footer')
        </div>
        <!--end off canvas content-->
    </div><!--end off canvas wrapper inner-->
</div><!--end off canvas wrapper-->
<!-- script files -->
</body>
</html>
