@extends('guest.layout.app')
<?php 
$current_url = Request::url();
$check_seo_tags = App\Helpers\Helper::checkSeoTag($current_url);
if(!$check_seo_tags) { ?>
@section('title', $data['seo'] == null ? Setting::get('site_name').' | Movie | '. implode(', ', $data['sub_category']) :
Setting::get('site_name').' | '. $data['seo']->title )
@section('description', $data['seo'] == null ? substr($video->description, 0, 275) : $data['seo']->description)
@section('keywords', $data['seo'] == null ? Setting::get('site_name') : $data['seo']->keywords)
@section('author', Setting::get('meta_author'))
<?php } ?>

@section('content')
<section class="movie-detail-intro overlay-gradient ptb100"
    style="background: url({{ Setting::get('home_page_bg_image') }});"></section>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<section class="movie-detail-intro2">
    <div class="container container-main movie_over">
        <div class="row movie_overplay">

            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <div class="movie-poster-left">
                    <div id="movie-posterset" class="movie-poster">
                        <img class="image" src="{{ $video->default_image }}"
                            alt="{{ Setting::get('site_name'). ' | ' .$video->title}}">
                        <a href="{{ $video->id }}" id="auto_play"><i class="fa fa-play"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <div class="movie-details-right">
                    <div class="movie-details">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                                <h1 class="title">{{ $video->title }}</h1>
                                <ul class="movie-subtext">
                                    <li>{{ $video->age }}</li>
                                    <li>{{ $video->release_date }}</li>
                                    <li>{{ $video->duration }}</li>
                                </ul>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="rating text-right">

                                    <div class="rating-div">
                                        <div class="rating-right-main">
                                            <div class="rating-val">
                                                <i class="fa fa-star" style="color: orange;"></i>
                                                <span class="rating_count">{{ $video->ratings }}/</span>
                                                <span class="rating_out_of">10</span>
                                            </div>
                                            <div class="rating-ttl-val">
                                                <span class="vote_total">{{ $video->vote_count }} votes</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rating-div2">
                                        <div class="rating-star2">
                                            @if(Auth::check())
                                            
                                            <?php $reviews = App\Helpers\Helper::getReview(Auth::user()->id, $video->id); 
                                                $rating = (!empty($reviews) && ($reviews->rating > 0)) ? $reviews->rating : 'Rate This';
                                                ?>
                                                <span class="rate_this rate_this_btn"><i id="fa_star_change" class="fa {{ ($rating>0) ? 'fa-star fa-fw' : 'fa-star-o' }}"></i> <span class="rate_count">{{$rating}}</span></span>
                                                <span class="rate_this_content" style="display: none">
                                                    <a class="star-rating-delete" title="Delete" rel="nofollow">x</a>
                                                    <!-- Rating Stars Box -->
                                                    <div class='rating-stars text-left'>
                                                        <ul id='stars_rate_this'>
                                                        @for($i=1; $i<=10; $i++)
                                                            <li class='star {{ ($rating >= $i) ? "selected" : "" }}' data-value='{{ $i }}'>
                                                            <i class='fa fa-star fa-fw'></i>
                                                            </li>
                                                        @endfor
                                                        </ul>
                                                    </div>
                                                </span>
                                            @else
                                                <span class="signin_click rate_this_btn">
                                                    <i class="fa fa-star-o"></i> <span>Rate This</span></span>
                                            @endif

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-12 col-sm-12 col-md-9 col-lg-9">
                                <div class="details-movie-div details-movie-div-top m-0">
                                    <h6>Categories:</h6>
                                    <p>{!! implode(', ', $data['sub_category']) !!}</p>
                                </div>
                                <div class="details-movie-div details-movie-div-top m-0">
                                    @if(count($data['tags'])>0)
                                    <h6>Tags:</h6>
                                    <p><?php 
                                        $tag_names = [];
                                        
                                            foreach($data['tags'] as $tags) {
                                                $tag_names[] =  '<a href="'. url('search/?tag='.$tags->tag->slug) .'">'. $tags->tag->name .'</a>';
                                            }
                                        
                                        echo implode(', ', $tag_names);
                                        ?>
                                    </p>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3">
                                @if(Auth::user())
                                @else
                                <div class="play_trailer_wrap">
                                    <a class="btn btn-icon btn-main btn-effect signin_click  play_trailer" id=""><i class="fa fa-play"></i> Play Trailer</a>
                                </div>
                                @endif
                            </div>

                        </div>
                    </div>
                    <div class="movie-widget-details">
                        <aside class="widget widget-movie-details">
                            <!-- <h2 class="title">Details</h2> -->
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="details-movie-div">
                                        <h6>Details:</h6>
                                        {!! html_entity_decode($video->details) !!}
                                    </div>
                                    <!-- <div class="details-movie-div">
                                        <h6>Release date:</h6>
                                        <p>{{ $video->release_date }}</p>
                                    </div> -->
                                    <div class="details-movie-div">
                                        <h6>Director:</h6>
                                        <p>
                                            @if($data['directors'] != null)
                                            <?php $directors = '';
                                            $k = 0;
                                            foreach($data['directors'] as $director) {
                                                if ($k == 2) {
                                                    $directors .= '<a href="'. url('search/?director='.$director->name) .'">'. $director->name .'</a> | ';
                                                    break;
                                                }  else {
                                                    $directors .= '<a href="'. url('search/?director='.$director->name) .'">'. $director->name .'</a>, ';
                                                }
                                            $k++;}
                                            if (count($data['directors']) > 3) {
                                                echo $directors.' <a class="click-full-casts">See full directors <i class="fa fa-angle-double-right"></i></a>';
                                            } else {
                                                echo rtrim($directors, ', ');
                                            }
                                            
                                            ?>
                                            @else
                                            N/A
                                            @endif
                                        </p>
                                    </div>
                                    <div class="details-movie-div">
                                        <h6>Writer:</h6>
                                        <p class="writers">
                                            @if($data['writers'] != null)
                                            <?php $writers = '';
                                            $j = 0;
                                            foreach($data['writers'] as $writer) {
                                                if ($j == 2) {
                                                $writers .= '<a href="'. url('search/?writer='.$writer->name) .'">'. $writer->name .'</a> | ';

                                                //$writers .= '<a id="click-full-writers">See full writers >></a>';
                                                break;
                                                } else {
                                                    $writers .= '<a href="'. url('search/?writer='.$writer->name) .'">'. $writer->name .'</a>, ';
                                                }
                                            $j++;}
                                            if (count($data['writers']) > 3) {
                                                echo $writers.' <a class="click-full-casts">See full writers <i class="fa fa-angle-double-right"></i></a>';
                                            } else {
                                                echo rtrim($writers, ', ');
                                            }
                                            ?>
                                            @else
                                            N/A
                                            @endif
                                        </p>
                                    </div>
                                    <div class="details-movie-div">
                                        <h6>Cast:</h6>
                                        <p class="cast-wrapper">
                                                @if($data['actors'] != null)
                                                <?php $casts = '';
                                                $i = 0;
                                                foreach($data['actors'] as $actor) {
                                                    if($i == 2) {
                                                        $casts .='<a href="'. url('search/?actor='.$actor->name) .'">'.$actor->name.'</a> | ';
                                                        
                                                        break;
                                                    } else {
                                                        $casts .='<a href="'. url('search/?actor='.$actor->name) .'">'.$actor->name.'</a>, ';
                                                    }
                                                $i++;
                                                }
                                                
                                                if (count($data['actors']) > 3) {
                                                    echo $casts.' <a class="click-full-casts">See full cast & crew <i class="fa fa-angle-double-right"></i></a>';
                                                } else {
                                                    echo rtrim($casts, ', ');
                                                }
                                                
                                                ?>
                                                @else
                                                N/A
                                                @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    
                    <?php if (!empty($video->alternative_titles)) { ?>
                    <div class="movie-widget-details akas">
                        <aside class="widget widget-movie-details">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    
                                        <div class="details-movie-div">
                                            <h6>Also Known As:</h6>
                                                <?php 
                                                if (json_validate($video->alternative_titles)) {
                                                    $aka = '';
                                                    foreach(json_decode($video->alternative_titles) as $title) {
                                                        $aka .= $title->title. ', ';
                                                    }
                                                    echo '<p class="">'. trim($aka, ', '). '</p>';
                                                } else {
                                                    echo '<p class="">'. $video->alternative_titles. '</p>';
                                                }
                                                
                                            ?>
                                        </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <?php } ?>
                    
                    <div class="movie-widget-details">
                        <aside class="widget">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                    <ul class="social-btns">
                                        <li>
                                            <a href="#" class="social-btn-roll facebook">
                                                <div class="social-btn-roll-icons">
                                                    <i class="social-btn-roll-icon fa fa-facebook"></i>
                                                    <i class="social-btn-roll-icon fa fa-facebook"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="social-btn-roll twitter">
                                                <div class="social-btn-roll-icons">
                                                    <i class="social-btn-roll-icon fa fa-twitter"></i>
                                                    <i class="social-btn-roll-icon fa fa-twitter"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="social-btn-roll google-plus">
                                                <div class="social-btn-roll-icons">
                                                    <i class="social-btn-roll-icon fa fa-google-plus"></i>
                                                    <i class="social-btn-roll-icon fa fa-google-plus"></i>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="social-btn-roll instagram">
                                                <div class="social-btn-roll-icons">
                                                    <i class="social-btn-roll-icon fa fa-instagram"></i>
                                                    <i class="social-btn-roll-icon fa fa-instagram"></i>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<section class="movie-detail-main ptb30">
    <div class="container container-main">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="inner pr30">
                    
                    <div class="description-reviews-tabs">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                              <a class="nav-link {{ (($tab_name) && $tab_name == 'reviews') ? '' : 'active' }}" data-toggle="tab" href="#description">Description</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link {{ (($tab_name) && $tab_name == 'reviews') ? 'active' : '' }}" data-toggle="tab" href="#reviews">Reviews ({{ $user_reviews->total() }})</a>
                            </li>

                            @if(count((array)$data['actors']) > 3)
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#casts">Cast & Crew</a>
                            </li>
                            @endif
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane {{ (($tab_name) && $tab_name == 'reviews') ? '' : 'active' }}" id="description">
                                <div class="storyline">
                                    <div class="card-body">
                                        {{-- <h2 id="review-form" class="title">Description</h2> --}}
                                        {!! html_entity_decode($video->description) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane {{ (($tab_name) && $tab_name == 'reviews') ? 'active' : '' }} fade" id="reviews">
                                @if ($user_reviews->total() > 0)
                                <div class="user_reviews_section">
                                    {{-- <h2 class="title">Reviews ({{ $user_reviews->total() }})</h2> --}}
                                    <div class="user_review_list">
                                        @foreach($user_reviews as $review)
                                        <div class="user_reviews">
                                            {{-- <div class="author_img">
                                                <img class="channel_detailimg" src="{{ url('placeholder.png') }}" alt="{{ Setting::get('site_name'). ' | ' .$review->user->name}}">
                                            </div> --}}
                                            <div class="user-comments">
                                                <div class="tinystarbar" title="{{ $review->rating }}/10">
                                                    <div class="rating text-left">
                                                        @if($review->rating >= 1)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 2)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 3)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 4)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 5)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 6)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 7)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 8)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 9)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                        @if($review->rating >= 10)
                                                        <i class="fa fa-star" style="color: orange; font-weight: 900;"></i>
                                                        @else
                                                        <i class="fa fa-star" style="color: #dddddd; font-weight: 100;"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="comment-meta-div">
                                                    <div class="comment-meta">
                                                        <b><span>{{ $review->user->name }}</span></b> - {{ date('d F Y', strtotime($review->created_at)) }}
                                                    </div>
                                                    <div class="comment-subject">{{ $review->subject }}</div>
                                                    <div class="author-comment">
                                                        {!! $review->comment !!}
                                                    </div>
                                                    @if($review->spoilers == 'YES')
                                                    <span class="spoiler-warning"><strong>Warning: Spoilers</strong></span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @if ($user_reviews->total() > 5)
                                        <div class="user-au-link btn btn-main">
                                        <a href="{{ url($review->adminVideo->unique_id.'/reviews/'.$review->admin_video_id) }}">See all {{ $user_reviews->total() }} user reviews</a> »
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                @endif

                                <div class="mt30">
                                    @if (Auth::user())
                                    <h2 class="title">Leave a review</h2>
                                    <div class="leave-comment-section">
                                        @if(count($errors))
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <?php
                                            // get review of this user
                                            $reviews = App\Helpers\Helper::getReview(Auth::user()->id, $video->id);
                                            $review_id = (!empty($reviews)) ? $reviews->id : '';
                                            $subject = (!empty($reviews)) ? $reviews->subject : '';
                                            $comment = (!empty($reviews)) ? $reviews->comment : '';
                                            $spoilers = (!empty($reviews)) ? $reviews->spoilers : '';
                                            $rating = (!empty($reviews)) ? $reviews->rating : 0;
                                        ?>
                                        <form action="{{ url('save-review') }}#review-form" method="post">
                                            <input type="hidden" id="review_id" name="review_id" value="{{ $review_id }}"/>
                                            <input type="hidden" value="reviews" name="tab_name"/>
                                            <input type="hidden" name="csrf_token" value="{{ csrf_token() }}"/>
                                            <input type="hidden" name="rating" class="form-control" id="rating" value="{{ $rating }}"/>
                                            <input type="hidden" id="admin_video_id" name="admin_video_id" value="{{ $video->id }}" class="form-control"/>
                                            <div class="form-group">
                                                <label for="rating">Your Rating</label>
                                                <!-- Rating Stars Box -->
                                                <div class='rating-stars text-left'>
                                                    <ul id='stars'>
                                                    @for($i=1; $i<=10; $i++)
                                                        <li class='star {{ ($rating >= $i) ? 'selected' : '' }}' data-value='{{ $i }}'>
                                                        <i class='fa fa-star fa-fw'></i>
                                                        </li>
                                                    @endfor
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="subject">Subject</label>
                                                <input type="text" placeholder="Write a subject for your comment here" class="form-control" required="" maxlength="255" value="{{$subject}}" name="subject">
                                            </div>
                                            <div class="form-group">
                                                <label for="review">Review</label>
                                                <textarea rows="6" placeholder="Write your review here"  class="form-control" required="" name="comment">{!!$comment!!}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="spoilers">Does this comment contain spoilers?</label>
                                                <input type="radio" required="" name="spoilers" value="1" {{ $spoilers == 'YES' ? 'checked' : '' }}/> Yes
                                                <input type="radio" required="" name="spoilers" value="0" {{ $spoilers == 'NO' ? 'checked' : '' }}/> No
                                                
                                            </div>
                                            {{-- <div class="form-group">
                                                <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{Setting::get('GOOGLE_RECAPTCHA_SITE_KEY')}}"></div>
                                            </div> --}}
                                            <div class="form-group">
                                                <input type="submit" name="submit" value="Submit"
                                                    class="btn btn-main btn-effect nomargin" />
                                            </div>
                                        </form>
                                    </div>
                                    @else
                                    <div class="review-form-inner has-border">
                                        <p class="comment-login-required">Only logged in users may leave a review.</p>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            <div class="tab-pane" id="casts">
                                <div class="details-movie-div">
                                    <div class="card-body">
                                        <div class="director_tab_cont mb-3">
                                            <h6>Directed by:</h6>
                                            <p>
                                                @if($data['directors'] != null)
                                                <?php $directors = '';
                                                foreach($data['directors'] as $director) {
                                                    $directors .= '<a href="'. url('search/?director='.$director->name) .'">'. $director->name .'</a>, ';
                                                }
                                                echo rtrim($directors, ', ');
                                                ?>
                                                @else
                                                N/A
                                                @endif
                                            </p>
                                        </div>

                                        <div class="writing_tab_cont mb-3">
                                        <h6>Writing Credits:</h6>
                                        <p class="writers">
                                            @if($data['writers'] != null)
                                            <?php $writers = '';
                                            foreach($data['writers'] as $writer) {
                                                $writers .= '<a href="'. url('search/?writer='.$writer->name) .'">'. $writer->name .'</a>, ';
                                            }
                                            echo rtrim($writers, ', ');
                                            ?>
                                            @else
                                            N/A
                                            @endif
                                        </p>
                                        </div>

                                        <div class="cast_tab_cont mb-2">
                                            <h6>Cast:</h6>
                                            <ul class="cast-wrapper">
                                                <li>
                                                    @if($data['actors'] != null)
                                                    <?php $actor_name = [];
                                                    foreach($data['actors'] as $actor) {
                                                        $actor_name[] ='<a href="'. url('search/?actor='.$actor->name) .'">'.$actor->name.'</a>';
                                                    }
                                                    echo implode(', ', $actor_name);?>
                                                    @else
                                                    N/A
                                                    @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="related-movies mt30">
                        <div class="row">
                            <div class="col-md-8">
                                <h2 class="title">Related Movies</h2>
                            </div>
                        </div>
                        <div class="row mt20">
                            <?php $display_video_count = 0 ?>
                            @foreach($data['related_video'] as $videos)
                            @foreach($videos as $related_video)
                            @if($related_video['admin_video_id'] != $video->id && $display_video_count < 6) <?php $display_video_count += 1; 
                                //$movie_description = (strlen($related_video['details']) > 300) ? substr($related_video['details'], 0, 300).'...' : $related_video['details'];
                                $movie_description = App\Helpers\Helper::truncate($related_video['details'], 200, '...<a href="'.url('movies/'.$related_video['unique_id']).'">More</a>', true, true);
                                //$movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
                            ?> <div class="item mb10 col-lg-2 col-md-3 col-sm-4 col-6">
                                    <a href="{{ url('movies/'.$related_video['unique_id'])  }}">
                                        <div class="movie-box-1">
                                            <div class="poster">
                                                <div class="movie_item">
                                                    <img src="{{ $related_video['default_image'] }}"
                                                        alt="{{ Setting::get('site_name'). ' | ' .$related_video['title']}}"
                                                        role="button" data-toggle="popover" data-trigger="hover"
                                                        tabindex="0" data-placement="right" data-title="" data-content="" data-container="body" data-html="true" data-original-title=""
                                                        title="" />

                                                        <div class="movie_item_hover">
                                                            <div class='popover-movie-title'>{{ $related_video['title'] }}</div>
                                                            <div class='movie-info-row'>
                                                                <div class="movie-info-item">
                                                                    <span class='movie-year'>{{ $related_video['release_date'] }}</span>
                                                                    <span class='movie-runtime'>{{ $related_video['duration'] }}</span>
                                                                </div>
                                                                <div class="movie-info-item">
                                                                    <span class='movie-stars'>
                                                                        <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                                                        {{ $related_video['ratings'] }}/<span class='rating_out_of'>10</span>
                                                                    </span>
                                                                    <span class='movie-vote'>
                                                                        <span class='vote_total'>{{ (!empty($related_video->vote_rating_total)) ? $related_video->vote_rating_total : 0 }} votes</span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class='movie-description'>
                                                                {!! $movie_description !!}
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>

                                        </div>
                                    </a>
                        </div>
                        @endif
                        @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<!-- Modal -->

{{-- <div id="review" class="small-dialog zoom-anim-dialog mfp-hide">
    <div class="review-wrapper">
        <div class="small-dialog-headline">
            <h4 class="text-center">
                <div class="ice-flex">
                    <div class="ice-header-image-wrapper"><img alt="" src="{{ $video->default_image }}" class="ice-header-image"></div>
                    <div class="ice-flex-expanding-column ice-header-content"><span class="a-size-medium ice-header-primary-text">{{ $video->title }}</span></div>
                </div>
            </h4>
        </div>
        <div class="small-dialog-content">
            <form id="user_review_form" action="#" method="post">
                <p class="status"></p>
                <div class="form-group">
                    <label for="rating">Your Rating</label>
                    <input type="hidden" name="rating" class="form-control" id="rating"/>
                </div>
                <div class="form-group">
                    <label for="headline">Headline</label>
                    <input type="text" placeholder="Write a headline for your review here" maxlength="175" class="form-control" required="" value="" name="headline">
                </div>
                <div class="form-group">
                    <label for="review">Review</label>
                    <textarea rows="6" placeholder="Write your review here" maxlength="10000" class="form-control" required="" value="" name="headline"></textarea>
                </div>
                <div class="form-group">
                    <label for="spoilers">Does this review contain spoilers?</label>
                    <input type="radio" name="spoilers" value="1"/> Yes
                    <input type="radio" name="spoilers" value="0"/> No
                     
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Submit"
                        class="btn btn-main btn-effect nomargin" />
                </div>
            </form>
        </div>
    </div>
</div> --}}

@endsection