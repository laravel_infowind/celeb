@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
   <div class="checkout-page">
        <div class="row justify-content-center">
            <div class="col-md-12">
               <div class="checkout-plan-heading">
               		Standard
               </div><!--checkout-plan-heading-->

               <div class="plan-desc1">
					Monthly recurring subscription gives you access to our entire library of video with no limitations or restrictions. Your Subscription will renew each month until cancelled. Number of sub-accounts: 14.
				</div><!--plan-desc-->

				<div class="t-am">
					<table>
						<tr>
							<td>Amount</td>
							<td>$14.99</td>
						</tr>

						<tr>
							<td>Total</td>
							<td>$14.99</td>
						</tr>

					</table>
				</div><!--t-am-->

				<div class="sub-plan-heading">
		        	<span>Payment Options</span>
		        </div>

		        <div class="payment-option">
		        	<p><label class="container1">PayPal
					  <input type="checkbox" checked="checked">
					  <span class="checkmark"></span>
					</label></p>

		        	<p>
		        		<label class="container1">Card Payment
						  <input type="checkbox" checked="checked">
						  <span class="checkmark"></span>
						</label>
		        	</p>
		        </div><!--payment-option-->

            </div> 
        </div>
   </div><!--checkout-page-page-->
</div>
@endsection
