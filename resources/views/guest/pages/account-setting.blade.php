@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
    <div class="account-settings">
        <div class="checkout-plan-heading">
            Account settings
        </div><!--checkout-plan-heading-->

        <div class="setting-inner">
            <table>
                <tr>
                    <td>PROFILE</td>
                    <td>
                        <p><strong>yunush.infowind@gmail.com</strong></p>
                        <p>Password ********</p>
                        <p>Phone</p>
                    </td>
                    <td>
                        <p><a href="#">Edit Profile</a></p>
                        <p><a href="#">Change Password</a></p>
                        <p><a href="#">Delete Account</a></p>
                    </td>
                </tr>

                <tr>
                    <td>PLAN DETAILS</td>
                    <td>
                        <p><strong>Standard</strong></p>
                    </td>
                    <td>
                        <p><a href="#">Change Plan</a></p>
                        <p><a href="#">Billing Details</a></p>
                    </td>
                </tr>

                 <tr>
                    <td>MANAGE PROFILE</td> 
                    <td>
                        <p class="usr-name-sec"><span><i class="fa fa-user"></i></span> Yunush Infowind</p>
                    </td>
                    <td>
                        <p><a href="#">Manage Profile</a></p>
                    </td>
                </tr>

                <tr>
                    <td>CARD DETAILS</td> 
                    <td>
                        <p><strong>xxxx-xxxx-xxxx-4242</strong></p>
                    </td>
                    <td>
                        <p><a href="#">Add Card</a></p>
                        <p><a href="#">Card Details</a></p>
                    </td>
                </tr>
            </table>
        </div><!--setting-inner-->
    </div><!--account-settings-->
</div><!--container-->
@endsection
