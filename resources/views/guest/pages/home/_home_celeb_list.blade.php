<div class="main-items">
            <ul class="items">
         <div class="main-items">
            <ul class="items">
            @if(count($celebs)>0)
            @foreach($celebs as $celeb)
              <li class="item">
                <div class="item-inn">
                  @if(Auth::user())
                  <div class="item-img"><a href="{{url('celeb-detail',['id'=>$celeb->id])}}"><img src="{{App\Helpers\Helper::getUserImage($celeb->id)}}" alt="itm-img"></a></div>
                  @else
                  <div class="item-img"><a href="{{url('login')}}"><img src="{{App\Helpers\Helper::getUserImage($celeb->id)}}" alt="itm-img"></a></div>
                  @endif
                  <div class="item-cont">
                    
                    <div class="item-fav">
                    @if(Auth::user())
                    <button  id="celeb_{{$celeb->id}}" class="btn fav-btn @if(checkCelebFav($celeb->id)) active_favorite @endif"><i onclick="favUnfavCeleb('{{Auth::user()->id}}','{{$celeb->id}}')" class="fas fa-heart"></i></button>
                    @else
                    <a href="{{url('login')}}"  class="btn fav-btn"><i class="fas fa-heart"></i></a>
                    @endif
                    </div>

                    @if(Auth::user())
                    <div class="item-title"><a href="{{url('celeb-detail',['id'=>$celeb->id])}}">{{$celeb->name}}</a></div>
                    @else
                    <div class="item-title"><a href="{{url('login')}}">{{$celeb->name}}</a></div>
                    @endif
                    <div class="item-rating">
                    {!!get_rating(round($celeb->ratings))!!}
                    </div>
                  </div>
                </div>
              </li>
              @endforeach
              @else
              
              <div id="no_record"  class="alert-danger text-center main-items">
                  <h1> No Recor found!</h1> 
              </div>
              @endif
            </ul>

            </div>

            </ul>
          </div>
          <div class="navigation-pagination">
            {{$celebs->links()}}
          </div>
          <script>
                $(document).ready(function(){
                    $('.disabled').hide();
                    $('.pagination > li').eq(0).hide();
                    $('.pagination li').eq(1).removeClass('page-item');
                    $('.pagination li').eq(1).addClass('loadmore-button');
                    $('.pagination li').eq(1).find('a').text('Load more');
                    // $('.active').next().find('a').text('Load more')
                    $('.pagination li > a').on('click',function(e){
                    e.preventDefault();
                    getCelebList(this.href);
                   
                });
                });

            </script>
           
           
              