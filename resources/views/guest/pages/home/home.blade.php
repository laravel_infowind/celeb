@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')
<section class="middle-section">
<div class="main-banner-slider">
          <div class="banner-slider owl-carousel">
            @foreach($sliders as $value)
            <div class="item">
              <div class="banner-slider-item">
                <div class="banner-slider-img">
                  <img  src="{{$value->profile_path}}" alt="">
                </div>
              </div>
            </div>
            @endforeach
            <!-- <div class="item">
              <div class="banner-slider-item">
                <div class="banner-slider-img">
                  <img  src="{{url('front/assets/images/banner-img2.png')}}" alt="">
                </div>
              </div>
            </div>
            <div class="item">
              <div class="banner-slider-item">
                <div class="banner-slider-img">
                  <img src="{{url('front/assets/images/banner-img3.png')}}" alt="">
                </div>
              </div>
            </div>
            <div class="item">
              <div class="banner-slider-item">
                <div class="banner-slider-img">
                  <img src="{{url('front/assets/images/banner-img2.png')}}" alt="">
                </div>
              </div>
            </div> -->
          </div>
      </div>
      </div>

      <div class="main-items-section">
        <div class="container">
          <div class="top-filter">
            <div class="row">
              <div class="col-lg-4">
                <div class="lft-filter-left">
                  <div class="lft-filter">
                    <i class="fas fa-user"></i> Celebs <span class="btn sm-btn rounded-pill ms-1 ripple-surface" id="celeb_count"> {{$totalCount}}</span>
                  </div>
                  <div class="lft-filter-btn">
                    <div class="filter-btn1"><span class="btn btn-sm" id="toggle-btn-filter">Filters <i class="fas fa-caret-down"></i></span></div>
                    <div class="filter-btn2"><span class="btn btn-sm" id="toggle-btn-alft"><i class="fas fa-ellipsis-v"></i></span>
                      <div class="filter-celebrities">
                        <ul>
                        <?php
                          $alphas = range('A', 'Z');
                          foreach($alphas as $value)
                             {
                            ?>
                            <li id="mobile_alpha_{{$value}}"><a  href="{{url('/?char='.$value)}}">{{$value}}</a></li>
                        <?php }
                        ?>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="right-filter-dropdowns">
                   <div class="form-group">
                    <div class="results-fltr">
                      <a class="btn ripple-surface" href="{{url('/')}}" ><i class="fas fa-refresh"></i>Reset All Filter</a>
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <div class="results-fltr">
                      <button class="btn ripple-surface"><i class="fas fa-bars"></i> Filter results</button>
                    </div>
                  </div> -->
                  <div class="form-group">
                    <div class="input-group search-filter">
                      <input type="search" id="search"  class="form-control" placeholder="Serach celeb names" aria-label="Search" aria-describedby="search-addon">
                      <span class="input-group-text border-0" onclick="getCelebList()" id="search-addon">
                        <i class="fas fa-search"></i>
                      </span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="dropdown">
                      <button class="btn ripple-surface" type="button" id="dropdownMenuButton" data-mdb-toggle="dropdown" aria-expanded="false">
                        <i class="fas fa-angle-down"></i><span id="sort_by"> Sort by </span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="position: absolute; left: 0px; top: 0px; margin: 0px; right: auto; bottom: auto; transform: translate(0px, 50px);" data-popper-placement="bottom-start">
                        <li><a class="dropdown-item" id="asc"  onclick="getCelebList('','','asc')" href="javascript:void(0)">Assending</a></li>
                        <li><a class="dropdown-item" id="desc" onclick="getCelebList('','','desc')" href="javascript:void(0)">Desending</a></li>
                      </ul>
                      <input type="hidden" name="order" value="" id="order">
                      <input type="hidden"  id="current_celeb_count" value="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="list">
          </div>
          <div class="addtm">
            <img src="{{('front/assets/images/addtm.png')}}" alt="">
          </div>
        </div>
      </div>
    </section>
    <script src="{{ asset('front/assets/js/jquery.min.js') }}"></script>
    <script>

        $(document).ready(function(){
            getCelebList();
            $('.right-filter-dropdowns').keypress(function (e) {
            if (e.which == 13) {
              getCelebList();
            }
          });
        });

        function getCelebList(url,char,order){
          if(order){
            $('#order').val(order);
            $(".dropdown-item").css({backgroundColor: "",color:""});
            if(order == 'asc'){
              text = "Ascending"
              $("#asc").css({backgroundColor: "#1f1a17",color:"red"});
            }else{
              text = "Descending"
              $("#desc").css({backgroundColor: "#1f1a17",color:"red"});
            }
            $('#sort_by').text(text);
          }
          
           var url = (url)?url:"{{url('get-celeb-list')}}"
           if(url){
             urlarr = url.split('?');
           }
           if(urlarr[1]){
           console.log(urlarr[1].split('='))
           }
           var page = (urlarr[1])? urlarr[1].split('='):'';
            $.ajax({
                type: 'GET',
                url: url,
                data:{search:$('#search').val(),char:"{{$char}}",order:$('#order').val()},
                success: function (data) {
                    if(($('#search').val() || char)||(!$('#search').val()&& !urlarr[1])){
                        if(urlarr[1] &&page[1]>1 ){
                          $('.navigation-pagination').remove();
                          $('#list').append(data.html);
                        }else{
                        $('#list').html('');
                        $('#list').html(data.html);
                        }
                    }else{
                       $('.navigation-pagination').remove();
                       $('#list').append(data.html);
                    }

                    if(!page){
                       var pre_count =  $('#current_celeb_count').val(data.currentCount);
                    }else{
                      var count = data.currentCount;
                      var currnt_count = parseInt($('#current_celeb_count').val()) + parseInt(count);
                      $('#current_celeb_count').val(currnt_count);
                    }
                    if((page && page[1] > 1) || $('#search').val() || "{{(isset($_GET['char']))?$_GET['char']:''}}"){
                      // $('#celeb_count').text($('#current_celeb_count').val());
                    }
                    $('#celeb_count').text(data.totalCount);
                    if("{{$char}}"){
                        $('.footer-cont > ul > li').removeClass('active');
                        $('#alpha_'+"{{$char}}").addClass('active');
                        $('#mobile_alpha_'+"{{$char}}").addClass('active');
                    }
                   
                },
                error: function() {
                    console.log(data);
                }
            });
        }

      

    </script>
    @endsection
