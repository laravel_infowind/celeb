@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
  <div class="cart-list-sec">
       <div class="card-header">Card List</div>

       <ul>
           <li class="add-default">
               <div class="cart-img"><i class="fab fa-cc-visa"></i></div>
               <div class="cart-details">
                   <p>4111111111111111</p>
                   <p>Set As Default </p>
               </div>
               <div class="cart-delete"><a href="#"><i class="fas fa-trash"></i></a></div>
           </li>

            <li>
               <div class="cart-img"><i class="fab fa-cc-mastercard"></i></div>
               <div class="cart-details">
                   <p>3566002020360505</p>
                   <p>Default </p>
               </div>
               <div class="cart-delete"><a href="#"><i class="fas fa-trash"></i></a></div>
           </li>

            <li>
               <div class="cart-img"><i class="fab fa-cc-visa"></i></div>
               <div class="cart-details">
                   <p>5555555555554444</p>
                   <p>Default </p>
               </div>
               <div class="cart-delete"><a href="#"><i class="fas fa-trash"></i></a></div>
           </li>

            <li>
               <div class="cart-img"><i class="fab fa-cc-mastercard"></i></div>
               <div class="cart-details">
                   <p>5105105105105100</p>
                   <p>Default </p>
               </div>
               <div class="cart-delete"><a href="#"><i class="fas fa-trash"></i></a></div>
           </li>

           <div class="add-new-cart">
               <a href="#"><i class="fas fa-plus"></i> Add new cart</a>
           </div>
       </ul>
  </div><!--cart-list-sec-->
</div><!--container-->
@endsection
