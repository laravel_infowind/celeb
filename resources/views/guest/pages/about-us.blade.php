@extends('guest.layout.app')

@section('title', Setting::get('site_name').' | '. 'About Us')
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')
<section class="movie-detail-intro overlay-gradient" style="background: url({{ Setting::get('home_page_bg_image') }}); min-height: 250px; position: relative; padding-top: 165px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="title" style="text-align: center;color: #ffffff;">About Us</h1>
			</div>
		</div>
	</div>

</section>

@endsection
