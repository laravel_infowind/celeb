@extends('guest.layout.app')
@if( request()->get('sub-category') )
@section('title', Setting::get('site_name').' | Movie | ' . request()->get('sub-category'))
@endif
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')

<section class="search-result home-pg-movie ptb80">
    <div class="container container-main">
        <div class="row cat_top_head">
            @if( request()->get('sub-category') )
                @if(!empty($subcategory))
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="cat_top_left_img">
                            @if (!empty($subcategory->subCategoryImage[0]))
                                <img class="channel_detailimg" src="{{ $subcategory->subCategoryImage[0]->picture }}" alt="{{ Setting::get('site_name'). ' | ' .$subcategory->name}}">
                            @else
                                <img class="channel_detailimg" src="{{ url('placeholder.png') }}" alt="{{ Setting::get('site_name'). ' | ' .$subcategory->name}}">
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-6">
                        <div class="cat_top_right_cont">
                            <h1 class="channel_name_heading">{{ $subcategory->name }}</h1>
                            <p class="cat_top_details">{!! $subcategory->description !!}</p>
                        </div>
                    </div>
                
                @endif
            @endif
        </div>
        <div class="row movie_heading">
            <div class="col col-md-6 col-sm-12" id="set_viewgenres">
                @if( request()->get('sub-category') )
                <h3 class="mmt_title title">Movies</h3>
                <span id="Genres">
                    <span>{{ request()->get('sub-category') }}: <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </span>
                </span>
                @endif
                <div class="row"
                    style="display:none; margin-left: 74px; position: absolute; z-index: 940; background-color: rgba(0, 0, 0, .9);border: solid 1px rgba(255, 255, 255, .15); top: 38px; padding: 5px;"
                    id="Genresdata">
                    @if($genres->isNotEmpty())
                    @foreach($genres as $genre)
                    <div class="col-md-3 col-xs-6 col-6 genredata_div">
                        <a href="{{ url("sub-category/".$genre->name) }}">{{ $genre->name}}</a>
                    </div>
                    @endforeach
                    @endif
                </div>
                <!-- For query name  -->
                @if( request()->get('query'))
                <h3 class="mmt_title title">Search Result For :</h3>
                <span class="search_span"><span>{{ request()->get('query') }}</span></span>
                @endif
                <!--   For Actor Name -->
                @if( request()->get('actor'))
                <h3 class="mmt_title title">Search Result For :</h3>
                <span class="search_span"><span>{{ request()->get('actor') }}</span></span>
                @endif
                <!-- For Director Name -->
                @if( request()->get('director'))
                <h3 class="mmt_title title">Search Result For :</h3>
                <span class="search_span"><span>{{ request()->get('director') }}</span></span>
                @endif
                <!-- For Writer Name -->
                @if( request()->get('writer'))
                <h3 class="mmt_title title">Search Result For :</h3>
                <span class="search_span"><span>{{ request()->get('writer') }} </span></span>
                @endif
                <!-- For Tag Name -->
                @if( request()->get('tag'))
                <h3 class="mmt_title title">Search Result For :</h3>
                <span class="search_span"><span>{{ request()->get('tag') }}</span></span>
                @endif

                @if($videos->isEmpty())
                <p class="video_notshow">Nothing found, please try again.</p>
            </div>
          @else
        </div>
    </div>
    

    <div class="row search_row">
        @foreach($videos as $video)
        <?php
        //$movie_description = (strlen($video['details']) > 300) ? substr($video['details'], 0, 300).'...' : $video['details'];
        $movie_description = App\Helpers\Helper::truncate($video['details'], 200, '...<a href="'.url('movies/'.$video['unique_id']).'">More</a>', true, true);
        //$movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
        ?>
        <div class="col-lg-2 col-md-3 col-sm-4 col-6 search_column">
<!--            <h3 class="search_subtitle title">{{ $video->name }}</h3>    -->

            @if( request()->get('tag') )
            @section('title', Setting::get('site_name').' | Movie | ' . $video->name)
            @endif
            @if( request()->get('director') )
            @section('title', Setting::get('site_name').' | Movie | ' . $video->name)
            @endif
            @if( request()->get('writer') )
            @section('title', Setting::get('site_name').' | Movie | ' . $video->name)
            @endif
            @if( request()->get('actor') )
            @section('title', Setting::get('site_name').' | Movie | ' . $video->name)
            @endif
            @if( request()->get('query') )
            @section('title', Setting::get('site_name').' | Movie | ' . $video->name)
            @endif
            <a href="{{ url('movies/'.$video['unique_id'])  }}">
                <div class="movie-box-1">
                    <div class="poster">

                        <div class="movie_item">
                            <img class="cover_img" src="{{ $video->default_image }}"
                                alt="{{ Setting::get('site_name').' | '.$video->title }}" role="button"
                                data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right"
                                data-title="" data-content="" data-container="body" data-html="true" data-original-title="" title="" />


                    <div class="movie_item_hover">
                        <div class='popover-movie-title'>{{ $video['title'] }}</div>
                        <div class='movie-info-row'>
                            <div class="movie-info-item">
                                <span class='movie-year'>{{ $video['release_date'] }}</span>
                                <span class='movie-runtime'>{{ $video['duration'] }}</span>
                            </div>
                            <div class="movie-info-item">
                                <span class='movie-stars'>
                                    <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                    {{ $video['ratings'] }}/<span class='rating_out_of'>10</span>
                                </span>
                                <span class='movie-vote'>
                                    <span class='vote_total'>{{ (!empty($video->vote_rating_total)) ? $video->vote_rating_total : 0 }} votes</span>
                                </span>
                            </div>
                        </div>
                        <div class='movie-description'>
                            {!! $movie_description !!}
                        </div>
                    </div>


                        </div>
                    </div>

                </div>
            </a>
        </div>
        @endforeach
        @endif
    </div>
    <div>
        <div class="all-pagination">{{ $videos->links() }}</div>
    </div>
    </div>
</section>
<!-- <button id="loadmore_data" class="btn" style="display:none !important;left:45%;bottom:30px;background-color: #5cb85c;border-color: #4cae4c;color: #fff;font-weight: 400; margin: 100px auto 0 auto;display: block;">Load More</button> -->
<div class="ajax-loading" style="display: none;">
    <span><i class="fa fa-circle clr_red"></i></span>
    <span><i class="fa fa-circle clr_white"></i></span>
    <span><i class="fa fa-circle clr_red"></i></span>
</div>
@endsection
