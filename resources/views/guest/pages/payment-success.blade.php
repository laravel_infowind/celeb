@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
  <div class="cart-list-sec">
      
       <div class="success-payment-inner">
           <div class="right-check-icon">
               <i class="fas fa-check"></i>
           </div><!--right-check-icon-->

           <div class="thank-heading">Thank you!</div>
           <div class="thank-text">
               <p>Your payment is received successfully !!</p>
               <p>Now you can watch video</p>
           </div><!--thank-text-->

           <div class="goto-home"><a href="">Go to home</a></div> 
       </div>
   </div><!--cart-list-sec-->
</div><!--container-->
@endsection
