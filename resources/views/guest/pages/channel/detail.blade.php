@extends('guest.layout.app')
@section('title', Setting::get('site_name').' | '. 'Single Channel')
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<section class="home-pg-movie ptb80">
    <div class="container">
        <div class="row channel_detailrow">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <img class="channel_detailimg" src="{{ $data['picture'] }}" alt="{{ Setting::get('site_name'). ' | ' .$data['name']}}">
            </div>
            <div class="col-lg-9 col-md-8 col-sm-6">
                
                <div class="cat_top_right_cont">
                    <h1 class="channel_name_heading">{{ $data['name'] }}</h1>
                    <p class="cat_top_details">{!! $data['description'] !!}</p>
                </div>
            </div>
        </div>
        <div class="row movie_heading" style="padding:0px; margin-left: -60px; margin-right: -60px;">
            <div class="col col-lg-6 col-md-9 col-sm-12">
                <h3 class="mmt_title title">Channels</h3>
                <span id="Genres">
                    <span>{{ $channel_name }}: <i class="fa fa-caret-down" aria-hidden="true">
                        </i>
                    </span>
                </span>
                <div class="row"
                    style="display:none; margin-left: 73px; position: absolute; z-index: 940; background-color: rgba(0, 0, 0, .9);border: solid 1px rgba(255, 255, 255, .15); top: 38px; padding: 5px;"
                    id="Genresdata">
                    @foreach($channels as $data)
                    <div class="col-md-3 col-6 col-xs-6 genredata_div">
                        <a href="{{ url('/channels/'.$data->slug) }}">{{ $data['name'] }}</a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="related-movies row mt20 channel_detail" style="padding:0px; margin-left: -60px; margin-right: -60px;">
            @foreach($videos as $data)
            <?php
                //$movie_description = (strlen($data['details']) > 300) ? substr($data['details'], 0, 300).'...' : $data['details'];
                $movie_description = App\Helpers\Helper::truncate($data['details'], 200, '...<a href="'.url('movies/'.$data['unique_id']).'">More</a>', true, true);
               // $movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
                ?>
            <div class="item col-lg-2 col-md-4 col-sm-6 col-xs-6 col-6">
                <a href="{{ url('movies/'.$data['unique_id'])  }}">
                    <div class="movie-box-1">
                        <div class="poster">
                            <div class="movie_item">
                                <img src="{{ $data['default_image'] }}" alt="{{ Setting::get('site_name'). ' | ' .$data['title']}}" role="button" data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right" data-title="" data-content="" data-container="body" data-html="true" data-original-title="" title=""/>

                                <div class="movie_item_hover">
                                    <div class='popover-movie-title'>{{ $data['title'] }}</div>
                                    <div class='movie-info-row'>
                                        <div class="movie-info-item">
                                            <span class='movie-year'>{{ $data['release_date'] }}</span>
                                            <span class='movie-runtime'>{{ $data['duration'] }}</span>
                                        </div>
                                        <div class="movie-info-item">
                                            <span class='movie-stars'>
                                                <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                                {{ $data['ratings'] }}/<span class='rating_out_of'>10</span>
                                            </span>
                                            <span class='movie-vote'>
                                                <span class='vote_total'>{{ (!empty($data->vote_rating_total)) ? $data->vote_rating_total : 0 }} votes</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class='movie-description'>
                                        {!! $movie_description !!}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <div>
            <div class="channel-detail-all-pagination">{{ $videos->links() }}</div>
        </div>
    </div>
</section>
<div class="ajax-loading" style="display: none;">  
    <span><i  class="fa fa-circle clr_red"></i></span>
    <span><i  class="fa fa-circle clr_white"></i></span>
    <span><i  class="fa fa-circle clr_red"></i></span>
</div>
@endsection