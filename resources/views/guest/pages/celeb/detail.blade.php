@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))

@section('content')
<section class="middle-section">
      <div class="inner-page-middle-sec">
          <div class="detail-page-inn">
              <div class="container">
                  <div class="row">
                      <div class="col-lg-3">
                          <div class="detail-page-left">
                              <div class="user-personal-lft">
                                <div class="detail-user-img">
                                    <img src="{{$celeb->profile_path}}" alt="">
                                </div>
                                <div class="detail-user-cont">
                                    <div class="add-fav-btn">
                                        <button id="celeb_{{$celeb->id}}" class="btn fav-btn @if(checkCelebFav($celeb->id)) active_favorite @endif">
                                          <span>Add to favorites</span><i onclick="favUnfavCeleb('{{(Auth::user())?Auth::user()->id:''}}','{{$celeb->id}}')" class="fas fa-heart"></i>
                                        </button>
                                    </div>
                                    <div class="user-rat-btn">
                                      <button class="btn rounded-pill">
                                          <span>User Rating ({{round($celeb->ratings)}})</span>
                                          <span class="item-rating">
                                          {!!get_rating(round($celeb->ratings))!!}
                                          </span>
                                        </button>
                                    </div>
                                </div>
                              </div>
                              <div class="user-personal-info">
                                    <div class="detail-head-top d-lg-none">
                                        <h2>Becoming Jane</h2>
                                        <p><span>Rating</span> 
                                            <span class="item-rating">
                                                <i class="fas fa-star active"></i>
                                                <i class="fas fa-star active"></i>
                                                <i class="fas fa-star active"></i>
                                                <i class="fas fa-star active"></i>
                                                <i class="fas fa-star"></i>
                                            </span>
                                        </p>
                                    </div>
                                    <div class="personal-info-cnt">
                                        <div class="personal-info-heading">Personal Info</div>
                                        <div class="personal-info-cont">
                                            <div class="personal-info-div">
                                                <h5>Known For</h5>
                                                <p>{{($celeb->known_for_department)?$celeb->known_for_department:'-'}}</p>
                                            </div>
                                            <div class="personal-info-div">
                                                <h5>Known Credits</h5>
                                                <p>34</p>
                                            </div>
                                            <div class="personal-info-div">
                                                <h5>Gender</h5>
                                                <p>{{($celeb->gender == 1)?'Female':'Male'}}</p>
                                            </div>
                                            <div class="personal-info-div">
                                                <h5>Birthday</h5>
                                              
                                                <p>{{($celeb->birthday)?$celeb->birthday:'-'}} ({{($celeb->birthday)?getAgeYear($celeb->birthday):'-'}} years old)</p>
                                            </div>
                                            <div class="personal-info-div">
                                                <h5>Place of Birth</h5>
                                                <p>{{$celeb->place_of_birth}}</p>
                                            </div>
                                            <div class="personal-info-div">
                                               <h5>Also Known As</h5>
                                               <?php 
                                               $known = json_decode($celeb->also_known_as);
                                               $str = '';
                                               if(!empty($known) && is_array($known) && count($known)>0){
                                                   foreach($known as $val){
                                                    $str = $val .' , ' . $str;
                                                   }
                                               }else{
                                                   $str =  !empty($celeb->also_known_as)?((empty(json_decode($celeb->also_known_as, true)) && is_array(json_decode($celeb->also_known_as, true)))?'-':$celeb->also_known_as):'-';  
                                                
                                               }
                                               ?>
                                                <p>{{$str}}</p>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="detail-page-right">
                            <div class="detail-head-top d-none d-lg-block">
                                <h2>{{$celeb->name}}</h2>
                                <p><span>Rating</span> 
                                    <span class="item-rating">
                                        {!!get_rating(round($celeb->ratings))!!}
                                    </span>
                                </p>
                            </div>
                            <div class="detail-sec">
                                <div class="detail-sec-head">Biography</div>
                                <div class="detail-sec-cont">
                                    <div class="detail-secDic">
                                        <div class="card">
                                            <div class="card-body">
                                                {!!$celeb->biography!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-sec">
                                <div class="detail-sec-head">Photo gallery</div>
                                <div class="detail-sec-cont">
                                    <div class="detail-pro-list">
                                         <ul>
                                       
                                             @if(count($celeb->images)>0)
                                             @foreach($celeb->images as $key => $image)
                                             @if($key <= 2)
                                             <li>
                                                 <div class="detail-pro-img">
                                                    <a href="#">
                                                        <img src="{{$image->file}}" alt="">
                                                    </a>
                                                 </div>
                                             </li>
                                             @endif
                                             @endforeach
                                             <li>
                                                 <div class="detail-pro-more">
                                                     <a href="#">
                                                        <div class="more-pro-cont">
                                                            <h2>+156</h2>
                                                            <p>More photo</p>
                                                            <span class="more-pro-btn"><i class="fas fa-arrow-circle-right"></i></span>
                                                        </div>
                                                     </a>
                                                 </div>
                                             </li>
                                             @endif
                                         </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="detail-sec">
                                <div class="detail-sec-head">Video gallery</div>
                                <div class="detail-sec-cont">
                                    <div class="detail-pro-list">
                                         <ul>
                                             @if(count($celeb->videos)>0)
                                             @foreach($celeb->videos as $video)
                                             <li>
                                                 <div class="detail-pro-img">
                                                    <a href="#">
                                                    <img src="{{url('front/assets/images/itm-img.png')}}" alt="">
                                                    </a>
                                                    <div class="play-btn">
                                                        <button class="btn"><i class="fas fa-play-circle"></i></button>
                                                    </div>
                                                 </div>
                                             </li>
                                             @endforeach
                                             <li>
                                                 <div class="detail-pro-more">
                                                     <a href="#">
                                                        <div class="more-pro-cont">
                                                            <h2>+37</h2>
                                                            <p>More video</p>
                                                            <span class="more-pro-btn"><i class="fas fa-arrow-circle-right"></i></span>
                                                        </div>
                                                     </a>
                                                 </div>
                                             </li>
                                             @endif
                                         </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="detail-sec">
                                <div class="detail-sec-tab">


                                    <!-- Tabs navs -->
                                    <ul class="nav nav-tabs mb-3" id="ex1" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="ex1-tab-1" data-mdb-toggle="tab" href="#ex1-tabs-1" role="tab" aria-controls="ex1-tabs-1" aria-selected="true">Reviews (12)</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="ex1-tab-2" data-mdb-toggle="tab" href="#ex1-tabs-2" role="tab" aria-controls="ex1-tabs-2" aria-selected="false">Discussion (0)</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active"  href="{{url('subscription')}}" role="tab" aria-controls="ex1-tabs-1" aria-selected="true">Get access</a>
                                        </li>
                                    </ul>
                                    <!-- Tabs navs -->
                                    
                                    <!-- Tabs content -->
                                    <div class="tab-content" id="ex1-content">
                                        <div class="tab-pane fade show active" id="ex1-tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="reviews-tab">
                                                        <div class="reviews-item">
                                                            <div class="item-rating">
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <div class="reviews-dt">
                                                                DemoGuy - <span>24 October 2020</span>
                                                            </div>
                                                            <div class="reviews-cont">
                                                                <h4>Very Good</h4>
                                                                <p>The shotgun theory is an approach of mass marketing. It involves reaching as many people as possible through television, cable and radio. On the Web, it refers to a lot of advertising done through banners to text ads in as many websites as possible, in order to get enough eyeballs that will hopefully turn into sales. An example of shotgun marketing would be to simply place an ad on primetime</p>
                                                            </div>
                                                        </div>
                                                        <div class="reviews-item">
                                                            <div class="item-rating">
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <div class="reviews-dt">
                                                                DemoGuy - <span>24 October 2020</span>
                                                            </div>
                                                            <div class="reviews-cont">
                                                                <h4>Very Good</h4>
                                                                <p>The shotgun theory is an approach of mass marketing. It involves reaching as many people as possible through television, cable and radio. On the Web, it refers to a lot of advertising done through banners to text ads in as many websites as possible, in order to get enough eyeballs that will hopefully turn into sales. An example of shotgun marketing would be to simply place an ad on primetime</p>
                                                            </div>
                                                        </div>
                                                        <div class="reviews-item">
                                                            <div class="item-rating">
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <div class="reviews-dt">
                                                                DemoGuy - <span>24 October 2020</span>
                                                            </div>
                                                            <div class="reviews-cont">
                                                                <h4>Very Good</h4>
                                                                <p>The shotgun theory is an approach of mass marketing. It involves reaching as many people as possible through television, cable and radio. On the Web, it refers to a lot of advertising done through banners to text ads in as many websites as possible, in order to get enough eyeballs that will hopefully turn into sales. An example of shotgun marketing would be to simply place an ad on primetime</p>
                                                            </div>
                                                        </div>
                                                        <div class="reviews-item">
                                                            <div class="item-rating">
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star active"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <div class="reviews-dt">
                                                                DemoGuy - <span>24 October 2020</span>
                                                            </div>
                                                            <div class="reviews-cont">
                                                                <h4>Very Good</h4>
                                                                <p>The shotgun theory is an approach of mass marketing. It involves reaching as many people as possible through television, cable and radio. On the Web, it refers to a lot of advertising done through banners to text ads in as many websites as possible, in order to get enough eyeballs that will hopefully turn into sales. An example of shotgun marketing would be to simply place an ad on primetime</p>
                                                            </div>
                                                        </div>

                                                        <div class="reviews-item-more text-center">
                                                            <button class="btn">View more</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="ex1-tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">
                                            <div class="card">
                                                <div class="card-body">
                                                    ssc
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Tabs content -->


                                </div>
                            </div>

                            <div class="detail-sec">
                                <div class="detail-sec-head">Similar Celebs</div>
                                <div class="detail-sec-cont">
                                    <div class="detail-pro-list">
                                         <ul>
                                             @if(!empty($similarCelebs))
                                             @foreach($similarCelebs as $celeb)
                                             <li>
                                                 <div class="detail-pro-img">
                                                    <a href="{{url('celeb-detail/'.$celeb->id)}}">
                                                        <img src="{{App\Helpers\Helper::getUserImage($celeb->id)}}" alt="">
                                                    </a>
                                                    <div class="detail-pro-nm">
                                                       {{$celeb->name}}
                                                    </div>
                                                 </div>
                                             </li>
                                             @endforeach
                                             @endif
                                         </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>

    <script>

    function favoriteUnfavoriteUser(userId,type)
    {
        $.ajax({
                type: 'GET',
                url: "{{url('fav-unfav-user')}}",
                data:{user_id:userId,type:type},
                success: function (data) {
           
                },
                error: function() {
                    console.log(data);
                }
            });
    }

    </script>
    @endsection
