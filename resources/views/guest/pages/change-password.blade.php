@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
 <div class="edit-profile cart-list-sec">
   <div class="card-header">Change password</div>

   <div class="login-inner">

    <div class="card-body">
      <form>
        <input type="hidden" name="_token" value="">
        <div class="form-group row">
          <label for="email" class="">Old Password</label>
          <div class="col-md-12"> 
            <input id="email" type="text" class="form-control " placeholder="Enter old Password" autocomplete="off"></div>
          </div> 

          <div class="form-group row"> 
            <label for="password" class="">New password</label>
            <div class="col-md-12">
              <input id="password" type="text" class="form-control " placeholder="Enter new password" autocomplete="off">
            </div>
          </div>

           <div class="form-group row"> 
            <label for="password" class="">Confirm password</label>
            <div class="col-md-12">
              <input id="password" type="text" class="form-control " placeholder=" Enter confirm password" autocomplete="off">
            </div>
          </div>

             <div class="form-group row mb-0"> 
            <div class="">
              <button type="submit" class="btn btn-primary logBtn"> 
                Change Password
              </button>                                            
            </div>
          </div>          

          </div>

          
        </form>
      </div>
    </div>
  </div><!--edit-profile-->
</div><!--container-->
@endsection
