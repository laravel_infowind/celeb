<div class="container container-main">
    <div class="row search_row">
        @foreach($videos as $video)
        <?php
          //$movie_description = (strlen($video['details']) > 300) ? substr($video['details'], 0, 300).'...' : $video['details'];
          $movie_description = App\Helpers\Helper::truncate($video['details'], 200, '...<a href="'.url('movies/'.$video['unique_id']).'">More</a>', true, true);
          $movie_description = str_replace(array("'", "\"", "&quot;"), "", htmlspecialchars($movie_description ) );
          ?>
        <div class="col-lg-2 col-md-3 col-sm-4 col-6 search_column">
            <!--      <h3 class="search_subtitle title">{{ $video->name }}</h3>      -->
            <a href="{{ url('movies/'.$video['unique_id'])  }}">
                <div class="movie-box-1">
                    <div class="poster">
                        <div class="movie_item">
                            <img class="cover_img" src="{{ $video->default_image }}"
                                alt="{{ Setting::get('site_name').' | '.$video->title }}" role="button"
                                data-toggle="popover" data-trigger="hover" tabindex="0" data-placement="right"
                                data-title="" data-content="" data-container="body" data-html="true" data-original-title="" title="" />

                            <div class="movie_item_hover">
                                <div class='popover-movie-title'>{{ $video['title'] }}</div>
                                <div class='movie-info-row'>
                                    <div class="movie-info-item">
                                        <span class='movie-year'>{{ $video['release_date'] }}</span>
                                        <span class='movie-runtime'>{{ $video['duration'] }}</span>
                                    </div>
                                    <div class="movie-info-item">
                                        <span class='movie-stars'>
                                            <i class='fa fa-star' style='color: orange; font-weight: 900;'></i>
                                            {{ $video['ratings'] }}/<span class='rating_out_of'>10</span>
                                        </span>
                                        <span class='movie-vote'>
                                            <span class='vote_total'>{{ (!empty($video->vote_rating_total)) ? $video->vote_rating_total : 0 }} votes</span>
                                        </span>
                                    </div>
                                </div>
                                <div class='movie-description'>
                                    {!! $movie_description !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>