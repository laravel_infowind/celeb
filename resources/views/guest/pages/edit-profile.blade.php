@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
 <div class="edit-profile cart-list-sec">
   <div class="card-header">Edit profile</div>

   <div class="login-inner">

    <div class="card-body">
      <form>
        <input type="hidden" name="_token" value="">
        <div class="form-group row">
          <label for="email" class="">Full Name</label>
          <div class="col-md-12"> 
            <input id="email" type="text" class="form-control " placeholder="Enter full Name" autocomplete="off"></div>
          </div> 

          <div class="form-group row"> 
            <label for="password" class="">Email address</label>
            <div class="col-md-12">
              <input id="password" type="text" class="form-control " placeholder="Enter email address" autocomplete="off">
            </div>
          </div>

           <div class="form-group row"> 
            <label for="password" class="">Mobile number</label>
            <div class="col-md-12">
              <input id="password" type="text" class="form-control " placeholder="Enter mobile number" autocomplete="off">
            </div>
          </div>

             <div class="form-group row mb-0"> 
            <div class="">
              <button type="submit" class="btn btn-primary logBtn"> 
                Save
              </button>                                            
            </div>
          </div>          

          </div>

          
        </form>
      </div>
    </div>
  </div><!--edit-profile-->
</div><!--container-->
@endsection
