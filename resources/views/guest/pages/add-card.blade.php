@extends('guest.layout.app')
@section('title', Setting::get('meta_title'))
@section('description', Setting::get('meta_description'))
@section('keywords', Setting::get('meta_keywords'))
@section('author', Setting::get('meta_author'))
@section('content')
<div class="container">
    <div class="account-settings add-card-sec">
        <div class="checkout-plan-heading">
           Add card
        </div><!--checkout-plan-heading-->

        <div class="add-card-inner">
            <div class="row">
            <div class="col-md-6">
                <div class="card-left-img">
                    <img src="{{url('front/assets/images/card-img.png')}}">
                    <div class="goto-home"><a href="">VIEW CARDS</a></div><!-- goto-home -->
                </div><!--card-left-img-->


            </div><!--col-md-6-->

             <div class="col-md-6">
                 <div class="add-car-field">
                    <div class="card-body">
                         <form>
                            <div class="form-group row">
                              <label for="" class="">Card Number</label>
                                <div class="col-md-12"> 
                                    <input type="text" class="form-control " placeholder="Enter card number" autocomplete="off">
                                </div>
                            </div><!--form-group-->

                             <div class="form-group row">
                              <label for="" class="">Valid Upto</label> 
                                <div class="col-md-6">
                                    <input type="text" class="form-control " placeholder="MM" autocomplete="off">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control " placeholder="YY" autocomplete="off">
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                              <label for="" class="">CVV Number</label>
                                <div class="col-md-12"> 
                                    <input type="text" class="form-control " placeholder="Enter CVV" autocomplete="off">
                                </div>
                            </div><!--form-group-->

                              <div class="form-group row">
                              <label for="" class="">Billing Zip</label>
                                <div class="col-md-12"> 
                                    <input type="text" class="form-control " placeholder="Enter address zip" autocomplete="off">
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                                <div class="card-name">
                                    Credit or debit card
                                </div><!--card-name-->
                            </div><!--form-group-->

                            <div class="goto-home"><a href="">Save Card</a></div><!-- goto-home -->

                         </form>
                    </div><!--card-body-->
                 </div><!-- login-inner -->
            </div><!--col-md-6-->
            </div><!--row-->
        </div><!--add-card-inner-->
    </div><!--account-settings-->
</div><!-- container -->
@endsection
