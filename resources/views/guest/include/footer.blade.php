<footer class="footer">
     <div class="main-footer">
      <div class="container">
        <div class="footer-item item-celebrities">
          <div class="footer-head">Browse Celebrities</div>
          <div class="footer-cont">
            <ul>
              <?php
                $alphas = range('A', 'Z');
                foreach($alphas as $value)
                {
                  ?>
                  <li id="alpha_{{$value}}"><a  href="{{url('/?char='.$value)}}">{{$value}}</a></li>
              <?php }
              ?>
            </ul>
          </div>
        </div>
        <div class="footer-item item-links">
          <div class="footer-head">Usefull links</div>
          <div class="footer-cont footer-menu">
            <ul>
            <?php $footer_menu = App\Helpers\Helper::getStaticPages(); ?>
                        @if(!empty($footer_menu))
                            @foreach($footer_menu as $row)
                              @if($row->is_error_page == 0)
                              <li class="list-inline-item">
                                 <a class="footer_anchor" href="{{ url($row->page_slug) }}">{{ $row->heading }}</a>
                              </li>
                              @endif
                            @endforeach
                        @endif
              <!-- <li><a href="#">Contact us</a></li>
              <li><a href="#">About us</a></li>
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Privacy Policy</a></li>
              <li><a href="#">Terms of Use</a></li>
              <li><a href="#">Cookie Policy</a></li>
              <li><a href="#">Cookie Settings</a></li> -->
            </ul>
          </div>
        </div>
        <div class="footer-item item-connect">
          <div class="footer-head">Connect with us</div>
          <div class="footer-cont footer-social">
            <ul>
              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li><a href="#"><i class="fab fa-twitter"></i></a></li>
              <li><a href="#"><i class="fab fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
     </div>
     <div class="copy-right">
      <div class="container">
      <div class="copy-txt">{{ Setting::get('site_name') }} &copy; {{ date('Y') }} {{ Setting::get('copyright_content') }}
                  </div>
      </div>
     </div>
    </footer>
    <script>
       function favUnfavCeleb(userId,celebId)
                {
                  $.ajax({
                      type: 'GET',
                      url: "{{url('fav-unfav-user')}}",
                      data:{user_id:userId,celeb_id:celebId},
                      success: function (data) {
                        $('#celeb_'+celebId).toggleClass('active_favorite');
                      },
                      error: function() {
                          console.log(data);
                      }
                  });
                }
    </script>