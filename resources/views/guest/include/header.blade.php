<header class="header">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="top-left">
              <div class="main-logo">
                <a href="{{url('/')}}">
                  <img src="{{ Setting::get('site_logo') }}" alt="Celebnakedness">
                </a>
              </div>
              <div class="menuToggle-button">
                <button class="nav-toggle">
                  <div class="icon-menu">
                    <span class="line line-1"></span>
                    <span class="line line-2"></span>
                    <span class="line line-3"></span>
                  </div>
                </button>
              </div>
            </div>
          </div> 
          <div class="col-md-6">
            <div class="menus">
              <div class="nav nav-container">
                <ul class="links">
                  <li class="active"><a href="{{url('/')}}">Home</a></li>
                  <li class=""><a href="#">Browse</a></li>
                  <li class=""><a href="{{url('about')}}">About us</a></li>
                  <li class=""><a href="#">Blog</a></li>
                  <li class=""><a href="{{url('contact-us')}}">Contact us</a></li>
                  <li class=""><a href="#">Login</a></li>
                  <li class=""><a href="#">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="top-right">
              <div class="right-menu">
                <ul>
                  @if(Auth::check())
                  <li><a href="{{url('user-logout')}}"><i class="fas fa-sign-in-alt"></i> Logout</a></li>
                  @else
                  <li><a href="{{url('login')}}"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                  <li><a href="{{url('signup')}}"><i class="fas fa-user-plus"></i> Signup</a></li>
                  @endif
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    