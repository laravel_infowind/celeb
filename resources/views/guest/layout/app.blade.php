<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Celebnakedness</title>
  <link rel="shortcut icon" href="{{'public/front/assets/images/favicon.png'}}" />
  <!---------Css------------>
  
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/bootstrap/css/mdb.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/menu.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/aos.css') }}">
  <!---------Css------------>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/responsive.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('front/assets/css/responsive.css') }}">
  <script src="{{ asset('front/assets/js/jquery.min.js') }}"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" 
   href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>


<body class="home">
  <div class="main-wrapper">

 @include('guest.include.header')

@yield('content')

@include('guest.include.footer')


  </div>

  <!---------Js------------>
  <script src="{{ asset('front/assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('front/assets/bootstrap/js/mdb.min.js') }}"></script>
  <script src="{{ asset('front/assets/js/menu.js') }}"></script>
  <script src="{{ asset('front/assets/js/script.js') }}"></script>
  <script src="{{ asset('front/assets/js/owl.carousel.js') }}"></script>
  <script src="{{ asset('front/assets/js/aos.js') }}"></script>
 
  <!-- <script>
    AOS.init({
      easing: 'ease-in-out-sine'
    });
  </script> -->
  <script>

    var owl = jQuery('.banner-slider');
    owl.owlCarousel({
      margin: 0,
      autoplay: true,
      nav: true,
      items: 3,
      loop: true,
      dots: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1000: {
          items: 3
        }
      }
    })
  </script>




<script>
  @if(Session::has('success'))
  toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true,
    "showDuration": "1000",
    "hideDuration": "1000",
    "positionClass": "toast-top-center",
  }
  		toastr.success("{{ session('success') }}");
  @endif

  @if(Session::has('error'))
  toastr.options =
  {
  	"closeButton" : true,
  	"progressBar" : true,
    "showDuration": "1000",
    "hideDuration": "1000",
    "positionClass": "toast-top-center",
  }
  		toastr.error("{{ session('error') }}");
  @endif

</script>
  
</body>

</html>