<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVotesToUserPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_payments', function (Blueprint $table) {
            $table->index('subscription_id');
            $table->index('user_id');
            $table->index('payment_id');
            $table->index('coupon_code');
            $table->index('expiry_date');
            $table->index('status');
            $table->index('is_cancelled');
            $table->index('is_coupon_applied');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_payments', function (Blueprint $table) {
            //
        });
    }
}
