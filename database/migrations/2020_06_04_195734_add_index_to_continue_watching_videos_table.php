<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToContinueWatchingVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('continue_watching_videos', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('sub_profile_id');
            $table->index('admin_video_id');
            $table->index('position');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('continue_watching_videos', function (Blueprint $table) {
            //
        });
    }
}
