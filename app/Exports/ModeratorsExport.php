<?php

namespace App\Exports;

use App\Moderator;
use Maatwebsite\Excel\Concerns\FromCollection;

class ModeratorsExport implements FromCollection
{
    public function collection()
    {
        return Moderator::all();
    }
}