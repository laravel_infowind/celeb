<?php

namespace App\Exports;

use App\UserPayment;
use Maatwebsite\Excel\Concerns\FromCollection;

class UserPaymentsExport implements FromCollection
{
    public function collection()
    {
        return UserPayment::all();
    }
}