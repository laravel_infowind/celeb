<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleVideo extends Model
{
    public $table = "people_videos";
}
