<?php

namespace App\Helpers;

use App\AdminVideo;
use App\AdminVideoImage;
use App\ContinueWatchingVideo;
use App\EmailTemplate;
use App\LikeDislikeVideo;
use App\Mail\CommonMail;
use App\User;
use App\UserHistory;
use App\UserReview;
use App\Page;
use App\Tag;
use App\AdminVideoTag;
use App\PeopleTag;
use App\Wishlist;
use Auth;
use Aws\S3\S3Client;
use DB;
use Exception;
use File;
use Hash;
use Image;
use Log;
use Mail;
use Mailgun\Mailgun;
use Setting;
use Storage;
use App\People;

use Illuminate\Http\Request;

class Helper
{
    public static function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function web_url()
    {
        return url('/');
    }

    public static function generate_email_code($value = "")
    {
        return uniqid($value);
    }

    public static function generate_email_expiry()
    {
        return time() + 24 * 3600 * 30; // 30 days
    }

    // Check whether email verification code and expiry

    public static function check_email_verification($verification_code, $user_id, &$error)
    {

        if (!$user_id) {

            $error = tr('user_id_empty');

            return false;

        } else {

            $user_details = User::find($user_id);
        }

        // Check the data exists

        if ($user_details) {

            // Check whether verification code is empty or not

            if ($verification_code) {

                // Log::info("Verification Code".$verification_code);

                // Log::info("Verification Code".$user_details->verification_code);

                if ($verification_code === $user_details->verification_code) {

                    // Token is valid

                    $error = null;

                    // Log::info("Verification CODE MATCHED");

                    return true;

                } else {

                    $error = tr('verification_code_mismatched');

                    // Log::info(print_r($error,true));

                    return false;
                }

            }

            // Check whether verification code expiry

            if ($user_details->verification_code_expiry > time()) {

                // Token is valid

                $error = null;

                Log::info(tr('token_expiry'));

                return true;

            } else if ($user_details->verification_code_expiry < time() || (!$user_details->verification_code || !$user_details->verification_code_expiry)) {

                $user_details->verification_code = Helper::generate_email_code();

                $user_details->verification_code_expiry = Helper::generate_email_expiry();

                $user_details->save();

                // If code expired means send mail to that user

                $subject = tr('verification_code_title');
                $email_data = $user_details;
                $page = "emails.welcome";
                $email = $user_details->email;
                $result = Helper::send_email($page, $subject, $email, $email_data);

                $error = tr('verification_code_expired');

                Log::info(print_r($error, true));

                return false;
            }

        }

    }

    // Note: $error is passed by reference
    public static function is_token_valid($entity, $id, $token, &$error)
    {
        if (
            ($entity == 'USER' && ($row = User::where('id', '=', $id)->where('token', '=', $token)->first()))
        ) {
            if ($row->token_expiry > time()) {
                // Token is valid
                $error = null;
                return $row;
            } else {
                $error = array('success' => false, 'error_messages' => Helper::get_error_message(103), 'error_code' => 103);
                return false;
            }
        }
        $error = array('success' => false, 'error_messages' => Helper::get_error_message(104), 'error_code' => 104);
        return false;
    }

    // Convert all NULL values to empty strings
    public static function null_safe($arr)
    {
        $newArr = array();
        foreach ($arr as $key => $value) {
            $newArr[$key] = ($value == null) ? "" : $value;
        }
        return $newArr;
    }

    public static function generate_token()
    {
        return Helper::clean(Hash::make(rand() . time() . rand()));
    }

    public static function generate_token_expiry()
    {
        $token_expiry = Setting::get('token_expiry_hour') ? Setting::get('token_expiry_hour') : 1;

        return time() + $token_expiry * 3600; // 1 Hour
    }

    public static function send_email($page, $subject, $email, $email_data)
    {

        // Check the email configuration

        if (Setting::get('email_notification') == OFF) {

            return Helper::get_error_message(123);
        }

        // check the email configured

        if (config('mail.username') && config('mail.password')) {

            try {

                $site_url = url('/');

                $isValid = 1;

                if (envfile('MAIL_DRIVER') == 'mailgun' && Setting::get('MAILGUN_PUBLIC_KEY') && Setting::get('is_mailgun_check_email') == 1) {

                    Log::info("isValid - STRAT");

                    # Instantiate the client.

                    $email_address = new Mailgun(Setting::get('MAILGUN_PUBLIC_KEY'));

                    $validateAddress = $email;

                    # Issue the call to the client.
                    $result = $email_address->get("address/validate", array('address' => $validateAddress));

                    # is_valid is 0 or 1

                    $isValid = $result->http_response_body->is_valid;

                    Log::info("isValid FINAL STATUS - " . $isValid);

                }

                if ($isValid) {

                    $content = "";

                    $template = [];

                    if (isset($email_data['template_type'])) {

                        $template = EmailTemplate::where('template_type', $email_data['template_type'])->first();

                    }

                    if ($template) {

                        $content = $template->description;

                        $subject = $template->subject ? str_replace('<%site_name%>', Setting::get('site_name'), $template->subject) : '';

                        $subject = $subject ? str_replace('&lt;%site_name%&gt;', Setting::get('site_name'), $subject) : '';

                        $content = isset($email_data['email']) ? str_replace('<%email%>', $email_data['email'], $content) : $content;

                        $content = isset($email_data['email']) ? str_replace('&lt;%email%&gt;', $email_data['email'], $content) : $content;

                        $content = isset($email_data['password']) ? str_replace('<%password%>', $email_data['password'], $content) : $content;

                        $content = isset($email_data['password']) ? str_replace('&lt;%password%&gt;', $email_data['password'], $content) : $content;

                        $content = str_replace('<%site_name%>', Setting::get('site_name'), $content);

                        $content = str_replace('&lt;%site_name%&gt;', Setting::get('site_name'), $content);

                        if ($template->template_type == 'admin_forgot_password' || $template->template_type == 'moderator_forgot_password') {
                            $content = str_replace('<%link%>', $email_data['link'], $content);

                            $content = str_replace('&lt;%link%&gt;', $email_data['link'], $content);
                        }

                        if ($template->template_type == NEW_VIDEO || $template->template_type == EDIT_VIDEO) {

                            $content = str_replace('<%category_name%>', $email_data['category_name'], $content);

                            $content = str_replace('&lt;%category_name%&gt;', $email_data['category_name'], $content);

                            $content = str_replace('<%video_name%>', $email_data['video_name'], $content);

                            $content = str_replace('&lt;%video_name%&gt;', $email_data['video_name'], $content);

                            $subject = $subject ? str_replace('<%video_name%>', $email_data['video_name'], $subject) : '';

                            $subject = $subject ? str_replace('&lt;%video_name%&gt;', $email_data['video_name'], $subject) : '';

                        }

                    }

                    Log::info(print_r($email_data, true));

                    $email_data['content'] = $content;

                    // if (Mail::queue($page, array('email_data' => $email_data,'site_url' => $site_url),
                    //         function ($message) use ($email, $subject) {

                    //             $message->to($email)->subject($subject);
                    //         }
                    // ))
                    if (Mail::to($email)->send(new CommonMail($page, $subject, $email_data))) {

                        return Helper::get_message(106);

                    } else {

                        throw new Exception(Helper::get_error_message(123));

                    }

                } else {

                    return Helper::get_message(106);

                }

            } catch (\Exception $e) {

                Log::info($e->getMessage());

                return $e->getMessage();

            }

        } else {

            return Helper::get_error_message(123);

        }
    }

    public static function get_error_message($code)
    {
        switch ($code) {
            case 3000:
                $string = tr('user_record_deleted_contact_admin');
                break;
            case 101:
                $string = tr('invalid_input');
                break;
            case 102:
                $string = tr('email_address_already_use');
                break;
            case 103:
                $string = tr('token_expiry');
                break;
            case 104:
                $string = tr('invalid_token');
                break;
            case 105:
                $string = tr('username_password_donot_match');
                break;
            case 106:
                $string = tr('all_fields_required');
                break;
            case 107:
                $string = tr('current_password_incorrect');
                break;
            case 108:
                $string = tr('password_not_correct');
                break;
            case 109:
                $string = tr('application_encountered_unknown');
                break;
            case 111:
                $string = tr('email_not_activated');
                break;
            case 115:
                $string = tr('invalid_refresh_token');
                break;
            case 123:
                $string = tr('something_went_wrong_error');
                break;
            case 124:
                $string = tr('email_not_registered');
                break;
            case 125:
                $string = tr('not_valid_social_register');
                break;
            case 130:
                $string = tr('no_result_found');
                break;
            case 131:
                $string = tr('old_password_wrong_password_doesnot_match');
                break;
            case 132:
                $string = tr('provider_id_not_found');
                break;
            case 133:
                $string = tr('user_id_not_found');
                break;
            case 141:
                $string = tr('something_went_wrong_paying_amount');
                break;
            case 144:
                $string = tr('please_verify_your_account');
                break;
            case 145:
                $string = tr('video_already_added_history');
                break;
            case 146:
                $string = tr('something_wrong_please_try_again');
                break;

            case 147:
                $string = tr('redeem_disabled_by_admin');
                break;
            case 148:
                $string = tr('minimum_redeem_not_have');
                break;
            case 149:
                $string = tr('redeem_wallet_empty');
                break;
            case 150:
                $string = tr('redeem_request_status_mismatch');
                break;
            case 151:
                $string = tr('redeem_not_found');
                break;

            case 152:
                $string = tr('coupon_not_found');
                break;

            case 153:
                $string = tr('coupon_inactive_status');
                break;

            case 154:
                $string = tr('subscription_not_found');
                break;

            case 155:
                $string = tr('subscription_inactive_status');
                break;

            case 156:
                $string = tr('subscription_amount_should_be_grater');
                break;

            case 157:
                $string = tr('video_not_found');
                break;

            case 158:
                $string = tr('video_amount_should_be_grater');
                break;

            case 159:
                $string = tr('expired_coupon_code');
                break;

            case 162:
                $string = tr('failed_to_upload');
                break;

            case 163:
                $string = tr('user_payment_details_not_found');
                break;
            case 164:
                $string = tr('subscription_autorenewal_already_cancelled');
                break;
            case 165:
                $string = tr('subscription_autorenewal_already_enabled');
                break;

            case 166:
                $string = tr('publish_time_should_not_lesser');
                break;

            case 167:
                $string = tr('video_not_saving');
                break;

            case 901:
                $string = tr('default_card_not_available');
                break;
            case 902:
                $string = tr('something_went_wrong_error_payment');
                break;
            case 903:
                $string = tr('payment_not_completed_pay_again');
                break;
            case 904:
                $string = tr('flagged_video');
                break;
            case 905:
                $string = tr('user_login_decline');
                break;

            case 906:
                $string = tr('video_data_not_found');
                break;

            case 3001:
                $string = tr('verification_code_title');
                break;

            default:
                $string = tr('unknown_error_occured');
        }
        return $string;
    }

    public static function get_message($code)
    {
        switch ($code) {
            case 101:
                $string = tr('success');
                break;
            case 102:
                $string = tr('password_change_success');
                break;
            case 103:
                $string = tr('successfully_logged_in');
                break;
            case 104:
                $string = tr('successfully_logged_out');
                break;
            case 105:
                $string = tr('successfully_sign_up');
                break;
            case 106:
                $string = tr('mail_sent_successfully');
                break;
            case 107:
                $string = tr('payment_successful_done');
                break;
            case 108:
                $string = tr('favourite_provider_delete');
                break;
            case 109:
                $string = tr('payment_mode_changed');
                break;
            case 110:
                $string = tr('payment_mode_changed');
                break;
            case 111:
                $string = tr('service_accepted');
                break;
            case 112:
                $string = tr('provider_started');
                break;
            case 113:
                $string = tr('arrived_service_location');
                break;
            case 114:
                $string = tr('service_started');
                break;
            case 115:
                $string = tr('service_completed');
                break;
            case 116:
                $string = tr('user_rating_done');
                break;
            case 117:
                $string = tr('request_cancelled_successfully');
                break;
            case 118:
                $string = tr('wishlist_added');
                break;
            case 119:
                $string = tr('payment_confirmed_successfully');
                break;
            case 120:
                $string = tr('history_added');
                break;
            case 121:
                $string = tr('history_deleted_successfully');
                break;
            case 122:
                $string = tr('autorenewal_enable_success');
                break;
            case 123:
                $string = tr('ppv_not_set');
                break;
            case 124:
                $string = tr('watch_video_success');
                break;
            case 125:
                $string = tr('pay_and_watch_video');
                break;
            default:
                $string = "";

        }

        return $string;
    }

    public static function get_push_message($code)
    {

        switch ($code) {
            case 601:
                $string = tr('no_provider_available');
                break;
            case 602:
                $string = tr('no_provider_available_take_service');
                break;
            case 603:
                $string = tr('request_complted_successfully');
                break;
            case 604:
                $string = tr('new_request');
                break;
            default:
                $string = "";
        }

        return $string;

    }

    public static function generate_password()
    {
        $new_password = time();
        $new_password .= rand();
        $new_password = sha1($new_password);
        $new_password = substr($new_password, 0, 8);
        return $new_password;
    }

    public static function upload_video_image($image, $video_id, $position)
    {

        $check_video_image = AdminVideoImage::where('admin_video_id', $video_id)->where('position', $position)->first();

        if ($check_video_image) {

            $video_image = $check_video_image;

            Helper::delete_picture($video_image->image, "/uploads/images/video");

        } else {
            $video_image = new AdminVideoImage;
        }

        $video_image->admin_video_id = $video_id;

        $video_image->image = Helper::normal_upload_picture($image, '', "video_" . $video_id . "_001" . $position);

        if ($position == 1) {
            $video_image->is_default = DEFAULT_TRUE;
        } else {
            $video_image->is_default = DEFAULT_FALSE;
        }

        $video_image->position = $position;

        $video_image->save();

        Log::info('VIDEO IMAGE SAVED : ' . $video_image->id);

    }

    public static function upload_picture($picture)
    {
        Helper::delete_picture($picture, "/uploads/");

        $s3_url = "";

        $file_name = Helper::file_name();

        $ext = $picture->getClientOriginalExtension();
        $local_url = $file_name . "." . $ext;

        if (config('filesystems')['disks']['s3']['key'] && config('filesystems')['disks']['s3']['secret']) {

            Storage::disk('s3')->put($local_url, file_get_contents($picture), 'public');

            $s3_url = Storage::url($local_url);
        } else {
            $ext = $picture->getClientOriginalExtension();
            $picture->move(public_path() . "/uploads", $file_name . "." . $ext);
            $local_url = $file_name . "." . $ext;

            $s3_url = Helper::web_url() . '/uploads/' . $local_url;
        }

        return $s3_url;
    }

    public static function img_exists($url)
    {
        $headers = get_headers($url);
        return stripos($headers[0], "200 OK") ? true : false;
    }
    public static function copy_img_to_local($url, $path = null, $file_name = "")
    {
        $s3_url = "";
        if (Helper::img_exists($url)) {
            $file_name = $file_name ? $file_name : Helper::file_name();
            $filename = $file_name . ".jpg";
            $path = $path ? $path : '/uploads/images/';
            Image::make($url)->save(public_path($path . $filename));
            $s3_url = Helper::web_url() . $path . $filename;
        }

        return $s3_url;
    }
    public static function copy_video_to_local($url, $path = null, $rename = false, $upload_type = VIDEO_UPLOAD_TYPE_spaces)
    {
        if ($rename) {
            $file_name = Helper::file_name();
            $ext = substr($url, strrpos($url, '.') + 1);
            $filename = $file_name . ".$ext";
        } else {
            $filename = basename($url);
        }
        if ($upload_type == VIDEO_UPLOAD_TYPE_spaces) {
//                     $folder=env('DO_SPACES_FOLDER');
            $folder = env('DO_SPACES_FOLDER') . '/videos';
            $filename = "$folder/" . $filename;
            Storage::disk('spaces')->put("$filename", fopen($url, 'r'), 'public');
            $url = Storage::disk('spaces')->url($filename);
        } elseif ($upload_type == VIDEO_UPLOAD_TYPE_s3) {
//                     $folder=env('DO_SPACES_FOLDER');
            $folder = env('DO_SPACES_FOLDER') . '/videos';
            $filename = "$folder/" . $filename;
            Storage::disk('s3')->put("$filename", fopen($url, 'r'), 'public');
            $url = Storage::disk('s3')->url($filename);
        } else {
            $path = $path ? $path : '/uploads/videos/original/';
            copy($url, public_path($path . $filename));
            // Image::make($url)->save(base_path( $path . $filename));
            $url = Helper::web_url() . $path . $filename;
        }

        return $url;
    }

    public static function normal_upload_picture($picture, $path = null, $file_name = "")
    {

        $s3_url = "";

        $file_name = $file_name ? $file_name : Helper::file_name();

        $ext = $picture->getClientOriginalExtension();

        $local_url = $file_name . "." . $ext;

        $path = $path ? $path : '/uploads/images/';

        $inputFile = public_path($path . $local_url);

        // Convert bytes into MB

        list($width, $height) = getimagesize($picture);

        $bytes = convertMegaBytes($picture->getClientSize());

        if (intval($bytes) > intval(Setting::get('image_compress_size'))) {

            Log::info('inside FFmpeg');

            // Compress the video and save in original folder

            $img = Image::canvas($width ? $width : 960, $height ? $height : 720);

            $image = Image::make($picture->getPathname())->resize($width, $height, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });

            // insert resized image centered into background

            $img->insert($image, 'center');

            $img->encode($ext, 75);

            $img->save($inputFile);

            // $FFmpeg = new \FFmpeg;

            // $FFmpeg
            //     ->input($picture->getPathname())
            //     ->output($inputFile)
            //     ->ready();
        } else {

            $picture->move(public_path() . $path, $local_url);

        }

        $s3_url = Helper::web_url() . $path . $local_url;

        Log::info("s3_url" . $s3_url);

        return $s3_url;
    }
    public static function subtitle_upload($subtitle)
    {
        $s3_url = "";

        $file_name = Helper::file_name();

        $ext = $subtitle->getClientOriginalExtension();

        $local_url = $file_name . "." . $ext;

        $path = '/uploads/subtitles/';

        $subtitle->move(public_path() . $path, $local_url);

        $s3_url = Helper::web_url() . $path . $local_url;

        return $s3_url;
    }

    public static function video_upload($picture, $compress_type)
    {

        $s3_url = "";

        $file_name = Helper::file_name();

        $ext = $picture->getClientOriginalExtension();

        $local_url = $file_name . ".mp4";

        $path = '/uploads/videos/original/';

        // Convert bytes into MB
        $bytes = convertMegaBytes($picture->getClientSize());

        $inputFile = public_path() . $path . $local_url;

        if ($bytes > Setting::get('video_compress_size') && $compress_type == DEFAULT_TRUE) {

            // dispatch(new OriginalVideoCompression($picture->getPathname(), $inputFile));

            Log::info("Compress Video : " . 'Success');

            // Compress the video and save in original folder
            $FFmpeg = new \FFmpeg;

            $FFmpeg
                ->input($picture->getPathname())
                ->vcodec('h264')
                ->constantRateFactor('28')
                // ->forceFormat( 'mp4' )
                ->output($inputFile)
                ->ready();

        } else {
            Log::info("Original Video");

            // Compress the video and save in original folder
            /* $FFmpeg = new \FFmpeg;

            $FFmpeg
            ->input($picture->getPathname())
            ->vcodec('h264')
            ->forceFormat( 'mp4' )
            ->output($inputFile)
            ->ready();*/

            $picture->move(base_path() . $path, $local_url);
        }

        $s3_url = Helper::web_url() . $path . $local_url;

        Log::info("Compress Video completed");

        return ['db_url' => $s3_url, 'baseUrl' => $inputFile, 'local_url' => $local_url, 'file_name' => $file_name];
    }

    public static function delete_picture($picture, $path)
    {

        if (file_exists(base_path() . $path . basename($picture))) {

            File::delete(base_path() . $path . basename($picture));

        }
        return true;
    }

    public static function s3_delete_picture($picture)
    {
        Log::info($picture);

        Storage::Delete(basename($picture));
        return true;
    }
    public static function spaces_delete_picture($picture, $path = 'images')
    {
        $url = env('DO_SPACES_FOLDER') . '/'. $path .'/'. basename($picture);

        if (File::exists($url)) {
            File::delete($url);
        }
        if(Storage::disk('spaces')->exists($url)) {
           Storage::disk('spaces')->delete($url);
        }
        
        return true;
    }
    public static function spaces_delete_video($picture, $path = 'videos')
    {
        $url = env('DO_SPACES_FOLDER') . '/' . $path .'/'. basename($picture);
        if (Storage::disk('s3')->exists($url)) {
            Storage::disk('s3')->delete($url);
        }
        return true;
    }

    public static function file_name($prefix = "")
    {

        $prefix = $prefix ? $prefix : Setting::get('prefix_file_name');

        $current_time = date("Y-m-d-H-i-s");

        $random_name = rand(99,999999);

        $file_name = $prefix . "-" . $current_time . "-" . $random_name;

        return $file_name;
    }

    public static function recently_added($web = null, $skip = 0, $take = 12, $id = null)
    {

        $videos_query = AdminVideo::where('admin_videos.is_approved', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->where('admin_videos.status', 1)
            ->whereNotIn('admin_videos.is_banner', [1])
            ->videoResponse()
            ->orderby('admin_videos.created_at', 'desc');
        if ($id) {
            // Check any flagged videos are present
            $flagVideos = getFlagVideos($id);

            if ($flagVideos) {
                $videos_query->whereNotIn('admin_videos.id', $flagVideos);
            }
        }

        if ($web) {

            // Check any flagged videos are present

            $continue_watching_videos = continueWatchingVideos($id);

            if ($continue_watching_videos) {

                $videos_query->whereNotIn('admin_videos.id', $continue_watching_videos);

            }

            $videos = $videos_query->paginate(12);

        } else {

            $videos = $videos_query->skip($skip)->take($take)
                ->get();
        }

        return $videos;
    }

    public static function recently_video($count, $user_id = null)
    {

        $videos_query = AdminVideo::where('admin_videos.is_approved', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->where('admin_videos.status', 1)
            ->videoResponse()
            ->whereNotIn('admin_videos.is_banner', [1])
            ->orderby('admin_videos.created_at', 'desc');
        if ($user_id) {
            // Check any flagged videos are present
            $flagVideos = getFlagVideos($user_id);

            if ($flagVideos) {
                $videos_query->whereNotIn('admin_videos.id', $flagVideos);
            }

            // Check any flagged videos are present
            $continue_watching_videos = continueWatchingVideos($user_id);

            if ($continue_watching_videos) {

                $videos_query->whereNotIn('admin_videos.id', $continue_watching_videos);

            }
        }

        if ($count > 0) {

            $video = $videos_query->skip(0)->take($count)->get();

        } else {

            $video = $videos_query->first();

        }
        return $video;
    }

    public static function wishlist($user_id, $web = null, $skip = 0, $take = 12)
    {

        $videos_query = Wishlist::where('user_id', $user_id)
            ->leftJoin('admin_videos', 'wishlists.admin_video_id', '=', 'admin_videos.id')
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->where('wishlists.status', 1)
            ->select(
                'wishlists.id as wishlist_id', 'admin_videos.id as admin_video_id',
                'admin_videos.title', 'admin_videos.description',
                'default_image', 'admin_videos.watch_count', 'admin_videos.ratings',
                'admin_videos.duration', 'admin_videos.category_id',
                DB::raw('DATE_FORMAT(admin_videos.publish_time , "%e %b %y") as publish_time'), 'categories.name as category_name')
            ->orderby('wishlists.created_at', 'desc');

        // Check any flagged videos are present
        $flagVideos = getFlagVideos($user_id);

        if ($flagVideos) {
            $videos_query->whereNotIn('admin_video_id', $flagVideos);
        }

        if ($web) {

            // Check any flagged videos are present
            $continue_watching_videos = continueWatchingVideos($user_id);

            if ($continue_watching_videos) {

                $videos_query->whereNotIn('admin_videos.id', $continue_watching_videos);

            }

            $videos = $videos_query->paginate(12);

        } else {
            $videos = $videos_query->skip($skip)->take($take)
                ->get();
        }

        return $videos;

    }

    public static function watch_list($user_id, $web = null, $skip = 0, $take = 12)
    {

        $videos_query = UserHistory::where('user_id', $user_id)
            ->leftJoin('admin_videos', 'user_histories.admin_video_id', '=', 'admin_videos.id')
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->select('user_histories.id as history_id', 'admin_videos.id as admin_video_id',
                'admin_videos.title', 'admin_videos.description', 'admin_videos.duration',
                'default_image', 'admin_videos.watch_count', 'admin_videos.ratings',
                DB::raw('DATE_FORMAT(admin_videos.publish_time , "%e %b %y") as publish_time'), 'admin_videos.category_id', 'categories.name as category_name')
            ->orderby('user_histories.created_at', 'desc');

        // Check any flagged videos are present
        $flagVideos = getFlagVideos($user_id);

        if ($flagVideos) {
            $videos_query->whereNotIn('admin_videos.id', $flagVideos);
        }

        if ($web) {

            // Check any flagged videos are present
            $continue_watching_videos = continueWatchingVideos($user_id);

            if ($continue_watching_videos) {

                $videos_query->whereNotIn('admin_videos.id', $continue_watching_videos);

            }

            $videos = $videos_query->paginate(12);

        } else {

            $videos = $videos_query->skip($skip)->take($take)->get();
        }

        return $videos;

    }

    public static function watch_list_agian($user_id, $web = null, $skip = 0, $take = 12)
    {
        $videos_query = UserHistory::where('user_id', $user_id)->orderBy('updated_at', 'desc')->paginate(12);
        return $videos_query;

    }

    public static function banner_videos($user_id)
    {

        $videos_query = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->where('admin_videos.is_banner', 1)
            ->select(
                'admin_videos.id as admin_video_id',
                'admin_videos.title', 'admin_videos.ratings',
                'admin_videos.banner_image as default_image'
            )
            ->orderBy('created_at', 'desc');

        // Check any flagged videos are present
        $flagVideos = getFlagVideos($user_id);

        if ($flagVideos) {

            $videos_query->whereNotIn('admin_videos.id', $flagVideos);

        }

        $videos = $videos_query->get();

        return $videos;
    }

    public static function suggestion_videos($web = null, $skip = 0, $id = null, $user_id = null)
    {

        $history = UserHistory::where('user_id', $user_id)
            ->leftJoin('admin_videos', 'user_histories.admin_video_id', '=', 'admin_videos.id')
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'sub_categories.category_id', '=', 'categories.id')
            ->where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)->pluck('sub_categories.id')->toArray();

        $videos_query = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->whereIn('sub_categories.id', $history)
            ->videoResponse()
            ->orderByRaw('RAND()');
        if ($user_id) {
            // Check any flagged videos are present
            $flagVideos = getFlagVideos($user_id);

            if ($flagVideos) {
                $videos_query->whereNotIn('admin_videos.id', $flagVideos);
            }

        }

        if ($id) {

            $videos_query->where('admin_videos.id', '!=', $id);

        }

        if ($web) {

            // Check any flagged videos are present
            $continue_watching_videos = continueWatchingVideos($user_id);

            if ($continue_watching_videos) {

                $videos_query->whereNotIn('admin_videos.id', $continue_watching_videos);

            }

            $videos = $videos_query->paginate(16);

        } else {

            $videos = $videos_query->skip($skip)->take(Setting::get('admin_take_count', 12))
                ->get();
        }

        return $videos;

    }

    public static function trending($web = null, $skip = 0, $take = 12, $id = null)
    {

        $videos_query = AdminVideo::where('watch_count', '>', 0)
            ->where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->videoResponse()
            ->orderby('watch_count', 'desc');
        if ($id) {
            // Check any flagged videos are present
            $flagVideos = getFlagVideos($id);

            if ($flagVideos) {
                $videos_query->whereNotIn('admin_videos.id', $flagVideos);
            }

        }

        if ($web) {

            // Check any flagged videos are present
            $continue_watching_videos = continueWatchingVideos($id);

            if ($continue_watching_videos) {

                $videos_query->whereNotIn('admin_videos.id', $continue_watching_videos);

            }

            $videos = $videos_query->paginate(12);
        } else {
            $videos = $videos_query->skip($skip)->take($take)->get();
        }

        return $videos;

    }

    public static function category_videos($category_id, $web = null, $skip = 0)
    {

        $videos_query = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->where('admin_videos.category_id', $category_id)
            ->videoResponse()
            ->orderby('admin_videos.sub_category_id', 'asc');
        if (Auth::check()) {
            // Check any flagged videos are present
            $flagVideos = getFlagVideos(Auth::user()->id);

            if ($flagVideos) {
                $videos_query->whereNotIn('admin_videos.id', $flagVideos);
            }
        }

        if ($web) {
            $videos = $videos_query->paginate(16);
        } else {
            $videos = $videos_query->skip($skip)->take(Setting::get('admin_take_count', 12))->get();
        }

        return $videos;
    }

    public static function sub_category_videos($sub_category_id, $web = null, $skip = 0, $user_id = null)
    {

        $videos_query = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->where('admin_videos.sub_category_id', $sub_category_id)
            ->videoResponse()
            ->orderby('admin_videos.sub_category_id', 'asc');
        if ($user_id) {
            // Check any flagged videos are present
            $flagVideos = getFlagVideos($user_id);

            if ($flagVideos) {
                $videos_query->whereNotIn('admin_videos.id', $flagVideos);
            }
        }

        if ($web) {
            $videos = $videos_query->paginate(16);
        } else {
            $videos = $videos_query->skip($skip)->take(Setting::get('admin_take_count', 12))->get();
        }

        return $videos;
    }

    public static function genre_videos($id, $web = null, $skip = 0)
    {

        $videos_query = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->where('admin_videos.genre_id', $id)
            ->videoResponse()
            ->orderby('admin_videos.sub_category_id', 'asc');

        if ($web) {
            $videos = $videos_query->paginate(16);
        } else {
            $videos = $videos_query->skip($skip)->take(Setting::get('admin_take_count', 12))->get();
        }

        return $videos;
    }

    public static function get_video_details($video_id)
    {

        $videos = AdminVideo::where('admin_videos.id', $video_id)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
        // ->videoResponse()
            ->orderBy('admin_videos.created_at', 'desc')
            ->first();

        if (!$videos) {
            $videos = array();
        }

        return $videos;
    }
    public static function get_single_details($video_id)
    {

        $videos = AdminVideo::where('admin_videos.id', $video_id)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->videoResponse()
            ->orderBy('admin_videos.created_at', 'desc')
            ->first();

        if (!$videos) {
            $videos = array();
        }

        return $videos;
    }

    public static function video_ratings($video_id)
    {

        $ratings = UserReview::where('admin_video_id', $video_id)
            ->leftJoin('users', 'user_reviews.user_id', '=', 'users.id')
            ->select('users.id as user_id', 'users.name as username',
                'users.picture as user_picture',

                'user_reviews.rating', 'user_reviews.comment',
                'user_reviews.created_at')
            ->get();
        if (!$ratings) {
            $ratings = array();
        }

        return $ratings;
    }

    public static function wishlist_status($video_id, $user_id)
    {
        if ($wishlist = Wishlist::where('admin_video_id', $video_id)->where('user_id', $user_id)->first()) {
            if ($wishlist->status) {
                return $wishlist->id;
            } else {
                return 0;
            }

        } else {
            return 0;
        }
    }

    public static function history_status($user_id, $video_id)
    {
        if (UserHistory::where('admin_video_id', $video_id)->where('user_id', $user_id)->count()) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function like_status($user_id, $video_id)
    {

        $model = LikeDislikeVideo::where('admin_video_id', $video_id)

            ->where('sub_profile_id', $user_id)->first();

        if ($model) {

            if ($model->like_status == DEFAULT_TRUE) {

                return 1;

            } else if ($model->dislike_status == DEFAULT_TRUE) {

                return -1;

            } else {

                return 0;

            }

        } else {

            return 0;
        }
    }

    public static function likes_count($video_id)
    {

        $model = LikeDislikeVideo::where('admin_video_id', $video_id)->where('like_status', 1)->count();

        return $model ? $model : 0;

    }

    public static function search_video($key, $web = null, $skip = 0, $id = null)
    {

        $videos_query = AdminVideo::where('admin_videos.is_approved', '=', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->where('title', 'like', '%' . $key . '%')
            ->where('admin_videos.status', 1)
            ->whereNotIn('admin_videos.is_banner', [1])
            ->where('categories.is_approved', 1)
            ->where('sub_categories.is_approved', 1)
            ->videoResponse()
            ->orderBy('admin_videos.created_at', 'desc');
        // if ($id) {
        //     // Check any flagged videos are present
        //     $flagVideos = getFlagVideos($id);

        //     if($flagVideos) {
        //         $videos_query->whereNotIn('admin_videos.id',$flagVideos);
        //     }
        // }

        if ($web) {
            $videos = $videos_query->paginate(16);
        } else {
            $videos = $videos_query->skip($skip)->take(Setting::get('admin_take_count', 12))->get();
        }

        return $videos;
    }

    public static function get_user_comments($user_id, $web = null)
    {

        $videos_query = UserReview::where('user_id', $user_id)
            ->leftJoin('admin_videos', 'user_reviews.admin_video_id', '=', 'admin_videos.id')
            ->where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->select('admin_videos.id as admin_video_id',
                'admin_videos.title', 'admin_videos.description',
                'default_image', 'admin_videos.watch_count',
                'admin_videos.duration',
                DB::raw('DATE_FORMAT(admin_videos.publish_time , "%e %b %y") as publish_time'))
            ->orderby('user_reviews.created_at', 'desc')
            ->groupBy('admin_videos.id');

        if ($web) {
            $videos = $videos_query->paginate(16);
        } else {
            $videos = $videos_query->skip($skip)->take(Setting::get('admin_take_count', 12))->get();
        }

        return $videos;

    }

    public static function get_video_comments($video_id, $skip = 0, $web = null)
    {

        $videos_query = UserReview::where('admin_video_id', $video_id)
            ->leftJoin('admin_videos', 'user_reviews.admin_video_id', '=', 'admin_videos.id')
            ->leftJoin('users', 'user_reviews.user_id', '=', 'users.id')
            ->where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->select('admin_videos.id as admin_video_id',
                'user_reviews.user_id as rating_user_id',
                'user_reviews.rating as rating',
                'user_reviews.comment', 'user_reviews.created_at',
                'users.name as username', 'users.picture')
            ->orderby('user_reviews.created_at', 'desc');
        if ($web) {
            $videos = $videos_query->get();
        } else {
            $videos = $videos_query->skip($skip)->take(Setting::get('admin_take_count', 12));
        }

        return $videos;

    }

    public static function check_wishlist_status($user_id, $video_id)
    {

        $status = Wishlist::where('user_id', $user_id)
            ->where('admin_video_id', $video_id)
            ->where('status', 1)
            ->first();
        return $status;
    }

    public static function send_notification($id, $title, $message)
    {

        Log::info("Send Push Started");

        // Check the user type whether "USER" or "PROVIDER"
        if ($id == "all") {

            $users = User::where('push_status', 1)->get();
        } else {
            $users = User::find($id);
        }

        $push_data = array();

        $push_message = array('success' => true, 'message' => $message, 'data' => array());

        $push_notification = 1; // Check the push notifictaion is enabled

        if ($push_notification == 1) {

            Log::info('Admin enabled the push');

            if ($users) {

                Log::info('Check users variable');

                foreach ($users as $key => $user) {

                    Log::info('Individual User');

                    if ($user->device_type == 'ios') {

                        // Log::info("iOS push Started");

                        // // require_once app_path().'/ios/apns.php';

                        // $msg = array("alert" => $message,
                        //     "status" => "success",
                        //     "title" => $title,
                        //     "message" => $push_message,
                        //     "badge" => 1,
                        //     "sound" => "default",
                        //     "status" => "",
                        //     "rid" => "",
                        //     );

                        // if (!isset($user->device_token) || empty($user->device_token)) {
                        //     $deviceTokens = array();
                        // } else {
                        //     $deviceTokens = $user->device_token;
                        // }

                        // $apns = new \Apns();
                        // $apns->send_notification($deviceTokens, $msg);

                        // Log::info("iOS push end");

                    } else {

                        Log::info("Andriod push Started");

                        require_once app_path() . '/gcm/GCM_1.php';
                        require_once app_path() . '/gcm/const.php';

                        if (!isset($user->device_token) || empty($user->device_token)) {
                            $registatoin_ids = "0";
                        } else {
                            $registatoin_ids = trim($user->device_token);
                        }
                        if (!isset($push_message) || empty($push_message)) {
                            $msg = "Message not set";
                        } else {
                            $msg = $push_message;
                        }
                        if (!isset($title) || empty($title)) {
                            $title1 = "Message not set";
                        } else {
                            $title1 = trim($title);
                        }

                        $message = array(TEAM => $title1, MESSAGE => $msg);

                        $gcm = new \GCM();
                        $registatoin_ids = array($registatoin_ids);
                        $gcm->send_notification($registatoin_ids, $message);

                        Log::info("Andriod push end");

                    }

                }

            }

        } else {
            Log::info('Push notifictaion is not enabled. Please contact admin');
        }

        Log::info("*************************************");

    }

    public static function upload_language_file($folder, $picture)
    {

        $ext = $picture->getClientOriginalExtension();

        $local_url = "messages" . "." . $ext;

        $picture->move(base_path() . "/resources/lang/" . $folder . "/", $local_url);

    }

    public static function delete_language_files($folder, $boolean)
    {
        if ($boolean) {
            $path = base_path() . "/resources/lang/" . $folder;
            \File::cleanDirectory($path);
            \Storage::deleteDirectory($path);
            rmdir($path);
        } else {
            \File::delete(base_path() . "/resources/lang/" . $folder . "/messages.php");
        }
        return true;
    }

    /**
     * Used to generate index.php
     *
     *
     */

    public static function generate_index_file($folder)
    {

        $filename = base_path() . "/" . $folder . "/index.php";

        if (!file_exists($filename)) {

            $index_file = fopen($filename, 'w');

            $sitename = Setting::get("site_name");

            fwrite($index_file, '<?php echo "You Are trying to access wrong path!!!!--|E"; ?>');

            fclose($index_file);
        }

    }

    /**
     * Function name: RTMP Secure video url
     *
     * @description: used to convert the video to rtmp secure link
     *
     * @created: vidhya R
     *
     * @edited:
     *
     * @param string $video_name
     *
     * @param string $video_link
     *
     * @return RTMP SECURE LINK or Normal video link
     */

    public static function convert_rtmp_to_secure($video_name = "", $video_link = "")
    {

        if (Setting::get('RTMP_SECURE_VIDEO_URL') != "") {

            // HLS_STREAMING_URL

            // validity of the link in seconds (if rtmp and www are on two different machines, it is better to give a higher value, because there may be a time difference.

            $e = date('U') + 20;

            $secret_word = "cgshlockkey";

            $user_remote_address = $_SERVER['REMOTE_ADDR'];

            $md5 = base64_encode(md5($secret_word . $user_remote_address . $e, true));

            $md5 = strtr($md5, '+/', '-_');

            $md5 = str_replace('=', '', $md5);

            $rtmp = $video_name . "?token=" . $md5 . "&e=" . $e;

            $secure_url = Setting::get('RTMP_SECURE_VIDEO_URL') . $rtmp;

            return $secure_url;

        } elseif (Setting::get('streaming_url')) {

            $rtmp_video_url = Setting::get('streaming_url') . $video_name;

            return $rtmp_video_url;

        } else {

            return $video_link;

        }

    }

    /**
     * Function name: RTMP Secure video url
     *
     * @description: used to convert the video to rtmp secure link
     *
     * @created: vidhya R
     *
     * @edited:
     *
     * @param string $video_name
     *
     * @param string $video_link
     *
     * @return RTMP SECURE LINK or Normal video link
     */

    public static function convert_hls_to_secure($video_name = "", $video_link = "")
    {

        if (Setting::get('HLS_SECURE_VIDEO_URL') != "") {

            // HLS_STREAMING_URL

            // validity of the link in seconds (if rtmp and www are on two different machines, it is better to give a higher value, because there may be a time difference.

            $expires = date('U') + 20;

            // secure_link_md5 "$secure_link_expires$uri$remote_addr cgshlockkey";

            $secret_word = "cgshlockkey";

            $user_remote_address = $_SERVER['REMOTE_ADDR'];

            Log::info("user_remote_address" . $user_remote_address);

            $md5 = md5("$expires/$video_name$user_remote_address $secret_word", true);

            $md5 = base64_encode($md5);

            $md5 = strtr($md5, '+/', '-_');

            $md5 = str_replace('=', '', $md5);

            $hls = $video_name . "?md5=" . $md5 . "&expires=" . $expires;

            $secure_url = Setting::get('HLS_SECURE_VIDEO_URL') . $hls;

            return $secure_url;

        } elseif (Setting::get('HLS_STREAMING_URL')) {

            $hls_video_url = Setting::get('HLS_STREAMING_URL') . $video_name;

            return $hls_video_url;

        } else {

            return $video_link;

        }

    }

    /**
     * Function Name : continue_watching_videos
     *
     * Brief : Displayed partially seen videos by the users based on their profile
     *
     * @param object $request - USer id, token & sub profile id
     *
     * @return response of array
     */

    public static function continue_watching_videos($sub_profile_id, $device_type, $skip)
    {

        $query = ContinueWatchingVideo::where('continue_watching_videos.sub_profile_id', $sub_profile_id)
            ->leftJoin('admin_videos', 'admin_videos.id', '=', 'continue_watching_videos.admin_video_id')
            ->where('admin_videos.status', DEFAULT_TRUE)
            ->where('admin_videos.is_approved', DEFAULT_TRUE)
            ->orderby('continue_watching_videos.updated_at', 'desc');

        // Check any flagged videos are present

        $flagVideos = getFlagVideos($sub_profile_id);

        if ($flagVideos) {

            $query->whereNotIn('admin_video_id', $flagVideos);

        }

        if ($device_type == DEVICE_WEB) {

            $model = $query->paginate(16);

        } else {

            $model = $query->skip($skip)->take(Setting::get('admin_take_count', 12))->get();

        }

        return $model;

        /*$data = [];

    foreach ($model as $key => $value) {

    $data[] = $value;

    }

    return $data;*/

    }

    /* Function name: RTMP Secure video url
     *
     * @description: used to convert the video to rtmp secure link
     *
     * @created: vidhya R
     *
     * @edited:
     *
     * @param string $video_name
     *
     * @param string $video_link
     *
     * @return RTMP SECURE LINK or Normal video link
     */

    public static function convert_smil_to_secure($smil_file = "", $smil_link = "")
    {

        if (Setting::get('VIDEO_SMIL_URL') != "") {

            // validity of the link in seconds (if rtmp and www are on two different machines, it is better to give a higher value, because there may be a time difference.

            $expires = date('U') + 20;

            // secure_link_md5 "$secure_link_expires$uri$remote_addr cgshlockkey";

            $secret_word = "cgshlockkey";

            $user_remote_address = $_SERVER['REMOTE_ADDR'];

            Log::info("user_remote_address" . $user_remote_address);

            $md5 = md5("$expires/$smil_file$user_remote_address $secret_word", true);

            $md5 = base64_encode($md5);

            $md5 = strtr($md5, '+/', '-_');

            $md5 = str_replace('=', '', $md5);

            $smil = $smil_file . "?md5=" . $md5 . "&expires=" . $expires;

            $secure_url = Setting::get('VIDEO_SMIL_URL') . '/' . $smil;

            return $secure_url;

        } else {

            return $smil_link;

        }

    }

    /**
     * Function: upload_file_to_s3
     *
     * @usage used to upload files to S3 Bucket
     *
     * @created Vidhya R
     *
     * @edited Shobana
     *
     * @param file $picture
     *
     * @return uploaded file URL
     */

    public static function upload_file_to_s3($picture)
    {

        $s3_url = "";

        $file_name = Helper::file_name();

        $extension = $picture->getClientOriginalExtension();

        $local_url = $file_name . "." . $extension;

        // Check S3 bucket configuration

        if (config('filesystems')['disks']['s3']['key'] && config('filesystems')['disks']['s3']['secret']) {

            $bucket = config('filesystems')['disks']['s3']['bucket'];

            $keyname = $local_url;

            $filename = $picture;

            Log::info($bucket);

            Log::info($keyname);

            Log::info(envfile('S3_REGION'));

            $s3 = new S3Client([
                'version' => 'latest',
                'region' => envfile('S3_REGION'),
                'credentials' => array(
                    'key' => envfile('S3_KEY'),
                    'secret' => envfile('S3_SECRET'),
                ),

            ]);

            $result = $s3->createMultipartUpload([
                'Bucket' => $bucket,
                'Key' => $keyname,
                'StorageClass' => 'REDUCED_REDUNDANCY',
                'ACL' => 'public-read',
                'Metadata' => [
                    'param1' => 'value 1',
                    'param2' => 'value 2',
                    'param3' => 'value 3',
                ],
            ]);

            $uploadId = $result['UploadId'];

            // Upload the file in parts.

            $parts = [];

            try {

                $file = fopen($filename, 'r');

                $partNumber = 1;

                while (!feof($file)) {
                    $result = $s3->uploadPart([
                        'Bucket' => $bucket,
                        'Key' => $keyname,
                        'UploadId' => $uploadId,
                        'PartNumber' => $partNumber,
                        'Body' => fread($file, 5 * 1024 * 1024),
                    ]);

                    // $parts = [];

                    $parts['Parts'][$partNumber] = [
                        'PartNumber' => $partNumber,
                        'ETag' => $result['ETag'],
                    ];

                    $partNumber++;

                    Log::info("Uploading part {$partNumber} of {$filename}." . PHP_EOL);

                }

                fclose($file);

            } catch (S3Exception $e) {

                $result = $s3->abortMultipartUpload([
                    'Bucket' => $bucket,
                    'Key' => $keyname,
                    'UploadId' => $uploadId,
                ]);

                Log::info("Upload of {$filename} failed." . PHP_EOL);

            }

            // Complete the multipart upload.

            $result = $s3->completeMultipartUpload([
                'Bucket' => $bucket,
                'Key' => $keyname,
                'UploadId' => $uploadId,
                // 'MultipartUpload'    => $parts,
                'MultipartUpload' => array('Parts' => $parts ? $parts['Parts'] : []),
            ]);

            $url = $s3_url = $result['Location'];

            Log::info("Uploaded {$filename} to {$url}." . PHP_EOL);

        } else {

            $ext = $picture->getClientOriginalExtension();

            $picture->move(public_path() . "/uploads/", $file_name . "." . $ext);

            $local_url = $file_name . "." . $ext;

            $s3_url = Helper::web_url() . '/uploads/' . $local_url;

        }

        return $s3_url;
    }
    /**
     * Function: upload_file_to_digitalocean_spaces
     *
     * @usage used to upload files to spaces Bucket
     *
     * @created Vidhya R
     *
     * @edited Shobana
     *
     * @param file $picture
     *
     * @return uploaded file URL
     */

    public static function upload_file_to_digitalocean_spaces($picture, $filename)
    {

        $url = "";
        
        if ($filename != '') {
            $file_name = $filename;
        } else {
            $file_name = Helper::file_name();
        }

        $extension = $picture->getClientOriginalExtension();
//            $folder=env('DO_SPACES_FOLDER');
        $folder = env('DO_SPACES_FOLDER') . '/videos';
        $local_url = "$folder/" . $file_name . "." . $extension;

        // Check S3 bucket configuration

        if (config('filesystems')['disks']['spaces']['key'] && config('filesystems')['disks']['spaces']['secret']) {

            try {
                Storage::disk('spaces')->put("$local_url", fopen($picture, 'r'), 'public');
                $url = Storage::disk('spaces')->url($local_url);

            } catch (S3Exception $e) {

                Log::info("Upload of {$local_url} failed." . PHP_EOL);

            }

        } else {

            $ext = $picture->getClientOriginalExtension();

            //  $picture->move(public_path() . "/uploads/", $file_name . "." . $ext);
            $picture->move(public_path() . "$folder/", $file_name . "." . $ext);
            $local_url = $file_name . "." . $ext;

            $url = Helper::web_url() . '/uploads/' . $local_url;

        }
        if(env('CDN_SUB_DOMAIN_URL')){
            $url = env('CDN_SUB_DOMAIN_URL').'/'.$local_url;
        }
        return $url;
    }

    public static function upload_file_to_digitalocean_spaces_image($picture, $path, $type = '', $filename = '')
    {

        $url = "";
        if ($filename != '') {
            $file_name = $filename;
        } else {
            $file_name = Helper::file_name();
        }
        $extension = $picture->getClientOriginalExtension();
        $folder=env('DO_SPACES_FOLDER').'/'.$path;
        //$folder = env('DO_SPACES_FOLDER') . '/images';
        $local_url = "$folder/" . $file_name . "." . $extension;

        if ($type == 'movie') {
            $image = Image::make($picture);
            $resize_image = $image->resize(300,448)->encode($extension);
            $uploading_file = (string) $resize_image;
        } else {
            $uploading_file = fopen($picture, 'r');
        }

        // Check S3 bucket configuration
        if (config('filesystems')['disks']['spaces']['key'] && config('filesystems')['disks']['spaces']['secret']) {
            try {
                Storage::disk('spaces')->put("$local_url", $uploading_file, 'public');
                $url = Storage::disk('spaces')->url($local_url);
            } catch (S3Exception $e) {
                Log::info("Upload of {$local_url} failed." . PHP_EOL);
            }
        } else {
            $ext = $picture->getClientOriginalExtension();
            if ($type = 'movie') {
                Image::make($picture)->resize(300,448)->save(public_path() . "/uploads/images/", $file_name . "." . $ext);
            } else {
                $picture->move(public_path() . "/uploads/images/", $file_name . "." . $ext);
            }

            $local_url = $file_name . "." . $ext;
            $url = Helper::web_url() . '/uploads/images/' . $local_url;
        }
        if(env('CDN_SUB_DOMAIN_URL')){
            $url = env('CDN_SUB_DOMAIN_URL').'/'.$local_url;
        }
        return $url;
    }

    public static function copy_img_to_digitalocean($picture, $path, $type = '')
    {

        $file_name = Helper::file_name();
        $filename = $file_name . ".jpg";
//            $path = '/uploads/images/';
        $folder = env('DO_SPACES_FOLDER') . '/'. $path;

//            Image::make($picture)->save(public_path($path . $filename));
        Image::make($picture)->resize(300,448)->save(public_path($folder . $filename));

        // $url = Helper::web_url().$path.$filename;

        if ($type == 'cast') {
            $folder = env('DO_SPACES_FOLDER') . '/images/cast';
        } else {
            $folder = env('DO_SPACES_FOLDER') . '/images';
        }

        $local_url = "$folder/" . $filename;

        // Check S3 bucket configuration
        if (config('filesystems')['disks']['spaces']['key'] && config('filesystems')['disks']['spaces']['secret']) {
            try {
//                    Storage::disk('spaces')->put("$local_url", fopen(public_path($path . $filename), 'r'),'public');
                Storage::disk('spaces')->put("$local_url", fopen(public_path($folder . $filename), 'r'), 'public');
                $url = Storage::disk('spaces')->url($local_url);

            } catch (S3Exception $e) {
                Log::info("Upload of {$local_url} failed." . PHP_EOL);
            }
        }
        if(env('CDN_SUB_DOMAIN_URL')){
            $url = env('CDN_SUB_DOMAIN_URL').'/'.$local_url;
        }
        return $url;
    }

    public static function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false)
    {
        if ($considerHtml) {
            // if the plain text is shorter than the maximum length, return the whole text
            if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }

            // splits all html-tags to scanable lines
            preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);

            $total_length = strlen($ending);
            $open_tags = array();
            $truncate = '';

            foreach ($lines as $line_matchings) {
                // if there is any html-tag in this line, handle it and add it (uncounted) to the output
                if (!empty($line_matchings[1])) {
                    // if it’s an “empty element” with or without xhtml-conform closing slash (f.e.)
                    if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                        // do nothing
                        // if tag is a closing tag (f.e.)
                    } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                        // delete tag from $open_tags list
                        $pos = array_search($tag_matchings[1], $open_tags);
                        if ($pos !== false) {
                            unset($open_tags[$pos]);
                        }
                        // if tag is an opening tag (f.e. )
                    } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                        // add tag to the beginning of $open_tags list
                        array_unshift($open_tags, strtolower($tag_matchings[1]));
                    }
                    // add html-tag to $truncate’d text
                    $truncate .= $line_matchings[1];
                }

                // calculate the length of the plain text part of the line; handle entities as one character
                $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
                if ($total_length + $content_length > $length) {
                    // the number of characters which are left
                    $left = $length - $total_length;
                    $entities_length = 0;
                    // search for html entities
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                        // calculate the real length of all entities in the legal range
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entities_length <= $left) {
                                $left--;
                                $entities_length += strlen($entity[0]);
                            } else {
                                // no more characters left
                                break;
                            }
                        }
                    }
                    $truncate .= substr($line_matchings[2], 0, $left + $entities_length);
                    // maximum lenght is reached, so get off the loop
                    break;
                } else {
                    $truncate .= $line_matchings[2];
                    $total_length += $content_length;
                }

                // if the maximum length is reached, get off the loop
                if ($total_length >= $length) {
                    break;
                }
            }
        } else {
            if (strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = substr($text, 0, $length - strlen($ending));
            }
        }

        // if the words shouldn't be cut in the middle...
        if (!$exact) {
            // ...search the last occurance of a space...
            $spacepos = strrpos($truncate, ' ');
            if (isset($spacepos)) {
                // ...and cut the text in this position
                $truncate = substr($truncate, 0, $spacepos);
            }
        }

        // add the defined ending to the text
        $truncate .= $ending;

        if ($considerHtml) {
            // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }
        
        return $truncate;
    }

    public static function getStaticPages()
    {
        $pages = Page::all();
        
        return (!empty($pages)) ? $pages : [];
    }

    public static function checkSeoTag($current_url)
    {
        $uri_parts = explode('/', $current_url);
        $request_url = end($uri_parts);
        
        $seo_pages = DB::table('seo_pages')->where('path', 'Like', '%'.$request_url.'%')->get();

        if (!empty($seo_pages)) {
            foreach ($seo_pages as $key => $value) {
                $path = explode('/', $value->path);
                $page_path = rtrim(end($path), '/');
            
                if ($page_path == $request_url) {
                    return true;
                    break;
                } 
            }
        }

        return false;
    }

    // publish block for publish time
    public static function published_block($model)
    {
        $post_date = ( !empty($model->id) ) ? $model->created_at : date('Y-m-d H:i:s');  
        $jj        = ( !empty($model->id) ) ? date( 'd', strtotime($post_date)) : date( 'd' );
        $mm        = ( !empty($model->id) ) ? date( 'm', strtotime($post_date)) : date( 'm' );
        $aa        = ( !empty($model->id) ) ? date( 'Y', strtotime($post_date)) : date( 'Y' );
        $hh        = ( !empty($model->id) ) ? date( 'H', strtotime($post_date)) : date( 'H' );
        $mn        = ( !empty($model->id) ) ? date( 'i', strtotime($post_date)) : date( 'i' );
        $ss        = ( !empty($model->id) ) ? date( 's', strtotime($post_date)) : date( 's' );

        $multi = 0;
        $cur_jj = date( 'd' );
        $cur_mm = date( 'm' );
        $cur_aa = date( 'Y' );
        $cur_hh = date( 'H' );
        $cur_mn = date( 'i' );

        $month_names = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

        $month = '<label><select id="mm" name="mm">';
        for ( $i = 1; $i < 13; $i = $i + 1 ) {
            $monthnum  = str_pad($i, 2, '0', STR_PAD_LEFT);
            $monthtext = $month_names[$i-1];
            if ($monthnum == $mm) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            $month    .= '<option value="' . $monthnum . '" '. $selected .' data-text="' . $monthtext . '">';
            /* translators: 1: Month number (01, 02, etc.), 2: Month abbreviation. */
            $month .= sprintf( __( '%1$s-%2$s' ), $monthnum, $monthtext ) . "</option>\n";
        }
        $month .= '</select></label>';

        $month;

        $day    = '<label><input type="text" ' . ( $multi ? '' : 'id="jj" ' ) . 'name="jj" value="' . $jj . '" size="2" maxlength="2" autocomplete="off" /></label>';
        $year   = '<label><input type="text" ' . ( $multi ? '' : 'id="aa" ' ) . 'name="aa" value="' . $aa . '" size="4" maxlength="4" autocomplete="off" /></label>';
        $hour   = '<label><input type="text" ' . ( $multi ? '' : 'id="hh" ' ) . 'name="hh" value="' . $hh . '" size="2" maxlength="2" autocomplete="off" /></label>';
        $minute = '<label><input type="text" ' . ( $multi ? '' : 'id="mn" ' ) . 'name="mn" value="' . $mn . '" size="2" maxlength="2" autocomplete="off" /></label>';

        if (!empty($model->id)) {
            if(strtotime($model->created_at) > time()) {
                $stamp = '<label>Scheduled for: <span class="published_on">'. date('Y-m-d', strtotime($model->created_at)). ' at '. date('H:i', strtotime($model->created_at)) .'</span></label>';
                $created_at = $model->created_at;
            } else {
                $stamp = '<label>Published on: <span class="published_on">'. date('Y-m-d', strtotime($model->created_at)). ' at '. date('H:i', strtotime($model->created_at)) .'</span></label>';
                $created_at = $model->created_at;
            }
        } else {
            $stamp = '<label>Publish: <span class="published_on">immediately</span></label>';
            $created_at = date('Y-m-d H:i:s');
        }
        
        
        $html= '<div class="publish_item mb-20">
            <span id="timestamp">
                '.$stamp.'
            </span>
            <a href="javascript:void(0)" class="edit-timestamp hide-if-no-js btn" role="button">
            <span aria-hidden="true">Edit</span>
            </a>
            <fieldset id="timestampdiv" class="hide-if-js" style="display: none;">
                <div class="timestamp-wrap">';
                    
        $html .= sprintf('%1$s %2$s, %3$s at %4$s:%5$s' , $month, $day, $year, $hour, $minute );
        $html .= '</div>
                <p>
                    <a href="javascript:void(0)" class="save-timestamp hide-if-no-js button btn">OK</a>
                    <a href="javascript:void(0)" class="cancel-timestamp hide-if-no-js button-cancel">Cancel</a>
                </p>
            </fieldset>
        </div>
        <input type="hidden" name="created_at" id="created_at" value="'.$created_at.'"/>';

        echo $html;
    }

    public static function getSelectedTags($people_id)
    {
        $get_tags = PeopleTag::with('tag')->where('people_file_id', $people_id)->get();
        $tags = '';
        if ($get_tags->count() > 0) {
            foreach ($get_tags as $key => $value) {
                $tags .= trim($value->tag->name).',';
            }
        }
        
        return $tags;
    }

    public static function getReview($user_id, $video_id)
    {
        $reviews = UserReview::where(['user_id' => $user_id, 'admin_video_id' => $video_id])->first();
        
        return (!empty($reviews)) ? $reviews : '';
        //print_r($reviews);die;
    }

    public static function countTagVideos($tag_id)
    {
        $videos = PeopleTag::where(['tag_id' => $tag_id])->count();
        
        return ($videos > 0) ? $videos : 0;
    }

    public static function getUsername($user_id)
    {
        $username = 'Admin';
        $user = User::find($user_id);

        if (!empty($user)) {
            $username = $user->name;
        } else {
            $subadmin = Admin::find($user_id);

            if (!empty($subadmin)) {
                $username = $subadmin->name;
            } else {
                $username = 'Admin';
            }
        }

        return $username;
    }

    public static function getUserImage($userId){
       $user = People::where('id',$userId)->first();
       if(!empty($user) && !empty($user->profile_path)){
        $url = $user->profile_path;
       }else{
           $url = url('front/user_default.png');
       }
       return $url;
    }

    public static function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
