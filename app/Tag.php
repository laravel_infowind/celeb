<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Helper;

class Tag extends Model
{
	public function people()
    {
        return $this->hasMany('App\People');
    }
}
