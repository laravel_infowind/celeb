<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommonMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $page;
    public $subject;
    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($page, $subject, $data)
     {
         $this->data = $data;
         $this->subject = $subject;
         $this->page = $page;

     }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->page)
                    ->subject($this->subject)
                    ->with([
                        'email_data'    =>  $this->data, 
                    ]);
    }
}
