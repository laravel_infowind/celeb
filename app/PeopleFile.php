<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleFile extends Model
{
    public $table = "people_files";

    public function people()
    {
        return $this->belongsTo('App\People');
    }
    
    public function tags() 
    {
        return $this->hasMany('App\PeopleTag');
    }
}
