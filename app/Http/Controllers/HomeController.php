<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterUser;
use App\People;
use App\Favorite;
use Response;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function mail_test()
    {
        Mail::to('samjordan@mailinator.com')->send(new RegisterUser());
    }

    public function celebDetail(Request $request)
    {
        $people = People::where('id',$request->id)->first();
        $similarCelebs = People::where('known_for_department',$people->known_for_department)
        ->where('id','!=',$people->id)->inRandomOrder()
        ->take(4)->get();
        return view('guest.pages.celeb.detail',['celeb'=>$people,'similarCelebs'=>$similarCelebs]);
    }

    public function favoriteUser(Request $request)
    {
        try{

            $favObj = Favorite::where('user_id',$request->user_id)->where('celeb_id',$request->celeb_id)->first();
            if(!empty($favObj)){
                $favObj->delete();
                $message="Celeb unfavorated";
            }else{
                $favObj =  new Favorite();
                $favObj->user_id = $request->user_id;
                $favObj->celeb_id = $request->celeb_id;
                $favObj->save();
                $message="Celeb favorated";
            }
          return  Response::json(['success'=>true,'message'=>$message]);
       
        }catch(\Exception $ex){
            return Response::json(['success'=>true,'message'=>$ex->getMessage()]);
        }
    }

}
