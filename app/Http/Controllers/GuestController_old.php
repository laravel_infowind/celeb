<?php
namespace App\Http\Controllers;

use App\AdminVideo;
use App\AdminVideoImage;
use App\CastCrew;
use App\Category;
use App\ContactUs;
use App\Helpers\Helper;
use App\Moderator;
use App\Page;
use App\SubCategory;
use App\UserReview;
use App\VideoCastCrew;
use App\Tag;
use App\AdminVideoTag;
use Auth;
use DB;
use Validator;
use Exception;
use App\Rules\ValidRecaptcha;

// use App\Jobs\NormalPushNotification;

use Illuminate\Http\Request;
use Setting;

class GuestController extends Controller
{

    public function index(Request $request)
    {
        // View Video Code
        $videos_data = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->videoResponse()
            ->orderby('admin_videos.id', 'desc')
            ->take(36)->get();

        $videoss = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->select('admin_videos.id as video_id', 'admin_videos.title',
                'admin_videos.description', 'admin_videos.ratings',
                'admin_videos.reviews', 'admin_videos.created_at as video_date',
                'admin_videos.video', 'admin_videos.trailer_video',
                'admin_videos.default_image', 'admin_videos.banner_image', 'admin_videos.is_banner', 'admin_videos.video_type',
                'admin_videos.video_upload_type',
                'admin_videos.vote_rating_total',
                'admin_videos.amount',
                'admin_videos.type_of_user',
                'admin_videos.type_of_subscription',
                'admin_videos.category_id as category_id',
                'admin_videos.sub_category_id',
                'admin_videos.genre_id',
                'admin_videos.video_type',
                'admin_videos.uploaded_by',
                'admin_videos.ppv_created_by',
                'admin_videos.details',
                'admin_videos.watch_count',
                'admin_videos.admin_amount',
                'admin_videos.user_amount',
                'admin_videos.video_upload_type',
                'admin_videos.duration',
                'admin_videos.redeem_amount',
                'admin_videos.compress_status',
                'admin_videos.trailer_compress_status',
                'admin_videos.main_video_compress_status',
                'admin_videos.video_resolutions',
                'admin_videos.video_resize_path',
                'admin_videos.trailer_resize_path',
                'admin_videos.is_approved',
                'admin_videos.unique_id',
                'admin_videos.video_subtitle',
                'admin_videos.trailer_subtitle',
                'admin_videos.trailer_duration',
                'admin_videos.trailer_video_resolutions',
                'admin_videos.publish_time',
                'categories.name as category_name', 'sub_categories.name as sub_category_name',
                'genres.name as genre_name',
                'admin_videos.video_gif_image',
                'admin_videos.is_banner',
                'admin_videos.is_pay_per_view',
                'admin_videos.actors',
                'admin_videos.directors',
                'admin_videos.writers',
                'admin_videos.moderator_id',
                'admin_videos.age',
                'admin_videos.release_date',
                'admin_videos.alternative_titles',
                'admin_videos.is_home_slider',
                'admin_videos.is_banner',
                'admin_videos.status'
            )
            ->where('admin_videos.is_banner', 1)
            ->orderBy('admin_videos.created_at', 'desc')
            ->get();

        if($videoss->count() <= 0) {
            $videoss = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->select('admin_videos.id as video_id', 'admin_videos.title',
                'admin_videos.description', 'admin_videos.ratings',
                'admin_videos.reviews', 'admin_videos.created_at as video_date',
                'admin_videos.video', 'admin_videos.trailer_video',
                'admin_videos.default_image', 'admin_videos.banner_image', 'admin_videos.is_banner', 'admin_videos.video_type',
                'admin_videos.video_upload_type',
                'admin_videos.vote_rating_total',
                'admin_videos.amount',
                'admin_videos.type_of_user',
                'admin_videos.type_of_subscription',
                'admin_videos.category_id as category_id',
                'admin_videos.sub_category_id',
                'admin_videos.genre_id',
                'admin_videos.video_type',
                'admin_videos.uploaded_by',
                'admin_videos.ppv_created_by',
                'admin_videos.details',
                'admin_videos.watch_count',
                'admin_videos.admin_amount',
                'admin_videos.user_amount',
                'admin_videos.video_upload_type',
                'admin_videos.duration',
                'admin_videos.redeem_amount',
                'admin_videos.compress_status',
                'admin_videos.trailer_compress_status',
                'admin_videos.main_video_compress_status',
                'admin_videos.video_resolutions',
                'admin_videos.video_resize_path',
                'admin_videos.trailer_resize_path',
                'admin_videos.is_approved',
                'admin_videos.unique_id',
                'admin_videos.video_subtitle',
                'admin_videos.trailer_subtitle',
                'admin_videos.trailer_duration',
                'admin_videos.trailer_video_resolutions',
                'admin_videos.publish_time',
                'categories.name as category_name', 'sub_categories.name as sub_category_name',
                'genres.name as genre_name',
                'admin_videos.video_gif_image',
                'admin_videos.is_banner',
                'admin_videos.is_pay_per_view',
                'admin_videos.actors',
                'admin_videos.directors',
                'admin_videos.writers',
                'admin_videos.moderator_id',
                'admin_videos.age',
                'admin_videos.release_date',
                'admin_videos.alternative_titles',
                'admin_videos.is_home_slider',
                'admin_videos.is_banner',
                'admin_videos.status'
            )
            ->orderBy('admin_videos.created_at', 'desc')
            ->take(1)
            ->get();
        }

        foreach($videoss as $videos) {
            $videoPath = $video_pixels = $trailer_video_path = $trailer_pixels = $trailerstreamUrl = $videoStreamUrl = '';

            $ios_trailer_video = $videos->trailer_video;

            $ios_video = $videos->video;

            $data['directors'] = $this->get_cast_and_crew(json_decode($videos->directors));
            $data['writers'] = $this->get_cast_and_crew(json_decode($videos->writers));
            $data['actors'] = $this->get_cast_and_crew(json_decode($videos->actors));

            if ($videos->video_type == VIDEO_TYPE_UPLOAD && $videos->video_upload_type == VIDEO_UPLOAD_TYPE_DIRECT) {
                
                if (check_valid_url($videos->trailer_video)) {

                    if (Setting::get('streaming_url')) {
                        $trailerstreamUrl = Setting::get('streaming_url') . get_video_end($videos->trailer_video);
                    }

                    if (Setting::get('HLS_STREAMING_URL')) {
                        $ios_trailer_video = Setting::get('HLS_STREAMING_URL') . get_video_end($videos->trailer_video);
                    }

                }

                if (check_valid_url($videos->video)) {

                    if (Setting::get('streaming_url')) {
                        $videoStreamUrl = Setting::get('streaming_url') . get_video_end($videos->video);
                    }

                    if (Setting::get('HLS_STREAMING_URL')) {
                        $ios_video = Setting::get('HLS_STREAMING_URL') . get_video_end($videos->video);
                    }

                }

                if (\Setting::get('streaming_url')) {
                    
                    if ($videos->is_approved == 1) {
                        if ($videos->trailer_video_resolutions) {
                            $trailerstreamUrl = Helper::web_url() . '/uploads/smil/' . get_video_end_smil($videos->trailer_video) . '.smil';
                        }
                        if ($videos->video_resolutions) {
                            $videoStreamUrl = Helper::web_url() . '/uploads/smil/' . get_video_end_smil($videos->video) . '.smil';
                        }
                    }
                } else {

                    $videoPath = $videos->video_resize_path ? $videos->video . ',' . $videos->video_resize_path : $videos->video;
                    $video_pixels = $videos->video_resolutions ? 'original,' . $videos->video_resolutions : 'original';
                    $trailer_video_path = $videos->trailer_resize_path ? $videos->trailer_video . ',' . $videos->trailer_resize_path : $videos->trailer_video;
                    $trailer_pixels = $videos->trailer_video_resolutions ? 'original,' . $videos->trailer_video_resolutions : 'original';
                }

                $trailerstreamUrl = $trailerstreamUrl ? $trailerstreamUrl : "";
                $videoStreamUrl = $videoStreamUrl ? $videoStreamUrl : "";
            } else {
                
                $trailerstreamUrl = $videos->trailer_video;

                $videoStreamUrl = $videos->video;

                if ($videos->video_type == VIDEO_TYPE_YOUTUBE) {

                    $videoStreamUrl = $ios_video = get_youtube_embed_link($videos->video);

                    $trailerstreamUrl = $ios_trailer_video = get_youtube_embed_link($videos->trailer_video);

                }
            }

            $admin_video_images = AdminVideoImage::where('admin_video_id', $videos->id)
                ->orderBy('is_default', 'desc')
                ->get();

            $admin_video_more = AdminVideo::where('category_id', $videos->category_id)
                ->where('id', '!=', $videos->id)
                ->inRandomOrder()
                ->skip(0)->take(4)
                ->get();
            $page = 'videos';

            $sub_page = 'admin_videos_view';

            if ($videos->is_banner == 1) {

                $sub_page = 'view-banner-videos';
            }

            // Load Video Cast & crews

            // $video_cast_crews = VideoCastCrew::select('cast_crew_id', 'name')
            //     ->where('admin_video_id', $videos->id)
            //     ->leftjoin('cast_crews', 'cast_crews.id', '=', 'video_cast_crews.cast_crew_id')
            //     ->get()->pluck('name')->toArray();
            $videoStreamUrl = change_web_url_to_cdn($videoStreamUrl);
            $trailerstreamUrl = change_web_url_to_cdn($trailerstreamUrl);
            $videoPath = change_web_url_to_cdn($videoPath);
            $trailer_video_path = change_web_url_to_cdn($trailer_video_path);

        }

        //print_r($videoss);die;

        // View Vidwo Code

        // $sub_categories  = SubCategory::where('category_id', 8)->where('status', '!=',1)->orderby('sub_categories.name', 'asc')->get();
        $sub_categories = SubCategory::where('category_id', 8)->where('status', '!=', 1)->first();
        
        return view('guest.pages.home.home')
            ->with('sub_categories', $sub_categories)
            ->with('videos', $videoss)
            ->with('video_images', $admin_video_images)
            ->withPage($page)
            ->with('sub_page', $sub_page)
            ->with('videoPath', $videoPath)
            ->with('video_pixels', $video_pixels)
            ->with('ios_trailer_video', $ios_trailer_video)
            ->with('ios_video', $ios_video)
            ->with('trailer_video_path', $trailer_video_path)
            ->with('trailer_pixels', $trailer_pixels)
            ->with('videoStreamUrl', $videoStreamUrl)
            ->with('trailerstreamUrl', $trailerstreamUrl)
            ->with('video_cast_crews', $video_cast_crews)
            ->with('admin_video_more', $admin_video_more)
            ->with('videos_data', $videos_data)
            ->with('data', $data);
    }

    public function recently_added(Request $request)
    {

        // View Video Code
        $videos_data = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->videoResponse()
            ->orderby('admin_videos.id', 'desc')
            ->paginate(36);

        $videos = AdminVideo::leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->select('admin_videos.id as video_id', 'admin_videos.title',
                'admin_videos.description', 'admin_videos.ratings',
                'admin_videos.reviews', 'admin_videos.created_at as video_date',
                'admin_videos.video', 'admin_videos.trailer_video',
                'admin_videos.default_image', 'admin_videos.banner_image', 'admin_videos.is_banner', 'admin_videos.video_type',
                'admin_videos.video_upload_type',
                'admin_videos.amount',
                'admin_videos.type_of_user',
                'admin_videos.type_of_subscription',
                'admin_videos.category_id as category_id',
                'admin_videos.sub_category_id',
                'admin_videos.genre_id',
                'admin_videos.video_type',
                'admin_videos.uploaded_by',
                'admin_videos.ppv_created_by',
                'admin_videos.details',
                'admin_videos.watch_count',
                'admin_videos.admin_amount',
                'admin_videos.user_amount',
                'admin_videos.video_upload_type',
                'admin_videos.duration',
                'admin_videos.redeem_amount',
                'admin_videos.compress_status',
                'admin_videos.trailer_compress_status',
                'admin_videos.main_video_compress_status',
                'admin_videos.video_resolutions',
                'admin_videos.video_resize_path',
                'admin_videos.trailer_resize_path',
                'admin_videos.is_approved',
                'admin_videos.unique_id',
                'admin_videos.video_subtitle',
                'admin_videos.trailer_subtitle',
                'admin_videos.trailer_duration',
                'admin_videos.trailer_video_resolutions',
                'admin_videos.publish_time',
                'categories.name as category_name', 'sub_categories.name as sub_category_name',
                'genres.name as genre_name',
                'admin_videos.video_gif_image',
                'admin_videos.is_banner',
                'admin_videos.is_pay_per_view',
                'admin_videos.actors',
                'admin_videos.directors',
                'admin_videos.writers',
                'admin_videos.moderator_id',
                'admin_videos.age',
                'admin_videos.release_date',
                'admin_videos.alternative_titles',
                'admin_videos.is_home_slider',
                'admin_videos.is_banner',
                'admin_videos.status'
            )
            ->orderBy('admin_videos.created_at', 'desc')
            ->first();

        if ($request->ajax()) {
            return view('guest.pages.home.recently-render')->with('videos_data', $videos_data)->render();
        }

        return view('guest.pages.home.recently-added')
            ->with('video', $videos)
            ->with('videos_data', $videos_data);
    }

    public function get_movies_by_category($id)
    {
        $videos = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
        // ->where('admin_videos.writers','RLIKE', "[[:<:]]".$data_str."[[:>:]]")
            ->where('admin_videos.sub_category_id', 'RLIKE', "[[:<:]]" . $id . "[[:>:]]")
            ->videoResponse()
            ->orderby('admin_videos.id', 'desc')
            ->take(18)->get();

        return $videos;
    }

    public function ajax_movies_by_category(Request $request)
    {
        $sub_categories = SubCategory::where('id', 10)->first();
        $videos = $this->get_movies_by_category($sub_categories->id);
        $videos_trending = AdminVideo::where('watch_count', '>', 0)
            ->where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->videoResponse()
            ->orderby('watch_count', 'desc')
            ->take(18)->get();

        if ($videos->isNotEmpty()) {
            return view('guest.pages.home.render-category', compact('sub_categories', 'videos_trending'))->render();
        } else {
            return 'null';
        }
    }

    public function movie_details(Request $request)
    {

        $video = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->where('admin_videos.unique_id', $request->id)
            ->first();

        if (!empty($video)) {
            // get reviews
            $user_reviews = UserReview::with('adminVideo')
                ->with('user')
                ->where('admin_video_id', $video->id)
                ->where('status', 1)
                ->orderBy('created_at', 'desc')
                ->paginate(5);

            //print_r($user_reviews);die;

            foreach (explode(',', $video->sub_category_id) as $key => $id) {
                $subcategory_name = SubCategory::find($id)->name;
                $data['sub_category'][] = '<a href="' . url("sub-category/" . $subcategory_name) . '">' . $subcategory_name . '</a>';
                $data['related_video'][] = $this->get_movies_by_category($id);
            }

            // return $data['related_video'];
            $data['directors'] = [];
            $data['writers'] = [];
            $data['actors'] = [];

            $data['directors'] = $this->get_cast_and_crew(json_decode($video->directors));
            $data['writers'] = $this->get_cast_and_crew(json_decode($video->writers));
            $data['actors'] = $this->get_cast_and_crew(json_decode($video->actors));

            $data['seo'] = DB::table('admin_video_seo')->where('admin_video_id', $video->id)->first();

            $data['tags'] = [];

            $data['tags'] = AdminVideoTag::with('tag')->where('admin_video_id', $video->id)->get();

            //echo '<pre>';
             //print_r($data['writers']);die;
            $tab_name = '';
            return view('guest.pages.movie.movie-detail')->with('video', $video)->with('data', $data)->with('user_reviews', $user_reviews)->with('tab_name', 'tab_name');
        } else {
            return abort(404);
        }
    }

    public function get_cast_and_crew($cast_id)
    {
        if ($cast_id == null) {
            $return = null;
        } else {
            foreach ($cast_id as $key => $id) {
                $data = CastCrew::find($id);
                if (!empty($data)) {
                    $return[] = $data;
                }
            }
        }

        return $return;
    }

    public function search(Request $request)
    {

        $subcategory = '';
        if ($request->get('query') != null) {
            $query = $request->get('query');
            $sub_category_row = SubCategory::where('name', $query)->first();

            if (!empty($sub_category_row)) {
                $sub_category_id = $sub_category_row->id;
            }

            $actor_row = CastCrew::where('name', $query)->where('position', 1)->first();

            if (!empty($actor_row)) {
                $actor_id = $actor_row->id;

                $sub_categories = DB::table('sub_categories as sc')->select('sc.id as id', 'name')->join('admin_videos as av', 'sc.id', '=', 'av.sub_category_id')->where('av.actors', 'RLIKE', "[[:<:]]" . $actor_id . "[[:>:]]")->groupBy('sc.id')->get();
            }

            $writer_row = CastCrew::where('name', $query)->where('position', 3)->select('id')->get();

            if (!empty($writer_row)) {

                $data_id = array();
                if (!empty($writer_id)) {
                    foreach ($writer_id as $reply) {
                        $data_id[] = ($reply->id);
                    }
                }
                $data_str = implode("|", $data_id);
            }

            $director_row = CastCrew::where('name', $query)->where('position', 2)->first();

            if (!empty($director_row)) {
                $director_id = $director_row->id;
            }

            $sql = AdminVideo::where('admin_videos.is_approved', 1)
                ->where('admin_videos.status', 1)
                ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
                ->where('admin_videos.title', 'LIKE', "%$query%")
                ->orWhere('admin_videos.description', 'LIKE', "%$query%")
                ->orWhere('admin_videos.details', 'LIKE', "%$query%");

            if (!empty($sub_category_id)) {
                $sql = $sql->orWhere('admin_videos.sub_category_id', 'RLIKE', "[[:<:]]" . $sub_category_id . "[[:>:]]");
            }

            if (!empty($actor_id)) {
                $sql = $sql->orWhere('admin_videos.actors', 'RLIKE', "[[:<:]]" . $actor_id . "[[:>:]]");
            }

            if (!empty($director_id)) {
                $sql = $sql->orWhere('admin_videos.directors', 'RLIKE', "[[:<:]]" . $director_id . "[[:>:]]");
            }

            if (!empty($data_str)) {
                $sql = $sql->orWhere('admin_videos.writers', 'RLIKE', "[[:<:]]" . $data_str . "[[:>:]]");
            }

            $result = $sql->orderby('admin_videos.id', 'desc')
                ->paginate(24);
        } else if ($request->get('sub-category') != null) {
            // view all set
            $subcategory = SubCategory::with('subCategoryImage')->where('name', $request->get('sub-category'))->first();
            $sub_category_id = $subcategory->id;
            $result = $this->get_all_movies_by_category($sub_category_id);
        } else if ($request->get('actor') != null) {
            $actor_id = CastCrew::where('name', $request->get('actor'))->where('position', 1)->first()->id;

            $sub_categories = DB::table('sub_categories as sc')->select('sc.id as id', 'name')->join('admin_videos as av', 'sc.id', '=', 'av.sub_category_id')->where('av.actors', 'RLIKE', "[[:<:]]" . $actor_id . "[[:>:]]")->groupBy('sc.id')->get();

            $result = $this->get_all_movies_by_actor($actor_id);
        } else if ($request->get('writer') != null) {
            $writer_id = CastCrew::where('name', $request->get('writer'))->where('position', 3)->select('id')->get();
            $data_id = array();
            foreach ($writer_id as $reply) {
                $data_id[] = ($reply->id);
            }
            $data_str = implode("|", $data_id);

            $result = $this->get_all_movies_by_writer($data_str);
        } else if ($request->get('director') != null) {
            $director_id = CastCrew::where('name', $request->get('director'))->where('position', 2)->first()->id;
            $result = $this->get_all_movies_by_director($director_id);
        } else if ($request->get('tag') != null) {
            $tag_id = Tag::where('slug', $request->get('tag'))->first()->id;
            $result = $this->get_all_movies_by_tag($tag_id);
        } else {
            $result = null;
        }

        if ($request->ajax()) {
            return view('guest.pages.search.search-render')->with('videos', $result)->render();
        } else {
            $genres = SubCategory::orderBy('name', 'asc')->get();
            return view('guest.pages.search.search')->with('videos', $result)->with('subcategory', $subcategory)->with('request', $request)->with('genres', $genres);
        }

    }

    public function subcategory(Request $request, $subcategory_name)
    {
        $subcategory = SubCategory::with('subCategoryImage')->where('name', $subcategory_name)->first();
        $sub_category_id = $subcategory->id;
        $result = $this->get_all_movies_by_category($sub_category_id);
        if ($request->ajax()) {
            return view('guest.pages.subcategory.subcategory-render')->with('videos', $result)->render();
        } else {

            $genres = SubCategory::orderBy('name', 'asc')->get();
            return view('guest.pages.subcategory.index')->with('videos', $result)->with('subcategory', $subcategory)->with('request', $request)->with('genres', $genres);
        }
    }

    public function search_result($query)
    {
        // return AdminVideo::where('admin_videos.is_approved' , 1)
        //     ->where('admin_videos.status' , 1)
        //     ->where('admin_videos.title' , 'LIKE', "%$query%")
        //     ->orWhere('admin_videos.description' , 'LIKE', "%$query%")
        //     ->orderby('admin_videos.id' , 'asc')->paginate(12);

        return AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->where('admin_videos.title', 'LIKE', "%$query%")
            ->orWhere('admin_videos.description', 'LIKE', "%$query%")
            ->orderby('admin_videos.id', 'desc')
            ->paginate(24);

    }

    public function get_all_movies_by_category($id)
    {
        return AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->where('admin_videos.sub_category_id', 'RLIKE', "[[:<:]]" . $id . "[[:>:]]")
        // ->where('admin_videos.sub_category_id', 'LIKE', "%$id%")
            ->orderby('admin_videos.id', 'desc')
            ->paginate(24);

    }

    public function get_all_movies_by_actor($id)
    {
        // return AdminVideo::where('admin_videos.is_approved', 1)
        //     ->where('admin_videos.status', 1)
        //     ->where('admin_videos.actors', 'LIKE', "%$id%")
        //     ->paginate(12);
        return AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->where('admin_videos.actors', 'RLIKE', "[[:<:]]" . $id . "[[:>:]]")
        // ->where('admin_videos.actors', 'LIKE', "%$id%")
            ->orderby('admin_videos.id', 'desc')
            ->paginate(24);
    }

    public function get_all_movies_by_writer($data_str)
    {
        return AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->where('admin_videos.writers', 'RLIKE', "[[:<:]]" . $data_str . "[[:>:]]")
            ->orderby('admin_videos.id', 'desc')
            ->paginate(24);
    }

    public function get_all_movies_by_director($id)
    {
        // return AdminVideo::where('admin_videos.is_approved', 1)
        //     ->where('admin_videos.status', 1)
        //     ->where('admin_videos.directors', 'LIKE', "%$id%")
        //     ->paginate(12);
        return AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->where('admin_videos.directors', 'RLIKE', "[[:<:]]" . $id . "[[:>:]]")
        // ->where('admin_videos.directors', 'LIKE', "%$id%")
            ->orderby('admin_videos.id', 'desc')
            ->paginate(24);
    }

    public function get_all_movies_by_tag($tag_id)
    {
        $result = AdminVideoTag::with('adminVideo')->where('tag_id', $tag_id)
        ->orderby('admin_video_tags.admin_video_id', 'desc')
        ->paginate(24);
        
        if(!empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key] = $value->adminVideo;
            }
        }
        //print_r($result);die;
        return $result;
    }

    public function movies()
    {

        $genres = SubCategory::orderBy('name', 'asc')->get();
        $category = Category::where('category_slug', '=', 'movies')->first();

        // $sub_category  = SubCategory::where('category_id', 8)->first();
        // return $sub_categories;

        // $sub_categories  = SubCategory::where('category_id', 8)->where('status', '!=',1)->orderby('sub_categories.name', 'asc')->get();

        //  dd($sub_categories);
        $sub_categories = DB::table('sub_categories as sc')->select('sc.id as id', 'name')->join('admin_videos as av', 'sc.category_id', '=', 'av.category_id')->where('sc.category_id', 8)->whereRaw("FIND_IN_SET(`sc`.`id`, `av`.`sub_category_id`) > 0")->where('sc.status', '!=', 1)->groupBy('sc.id')->orderBy('name', 'asc')->get();

        $videos = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.category_id', '=', 'sub_categories.category_id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
            ->whereRaw("FIND_IN_SET(`sub_categories`.`id`, `admin_videos`.`sub_category_id`) > 0")
        // ->where('admin_videos.sub_category_id','RLIKE', "[[:<:]]".$sub_category->id."[[:>:]]")
            ->videoResponse()
            ->groupBy('admin_videos.id')
            ->orderby('admin_videos.id', 'desc')
            ->get();

        //echo '<pre>';
        //print_r($videos);die;
        if ($videos->isNotEmpty()) {
            return view('guest.pages.movie.index', compact('sub_categories', 'genres', 'videos', 'category'));
        } else {
            return 'null';
        }
        // return view('guest.pages.tv-shows.index');

    }
    public function music()
    {

        $sub_categories = SubCategory::where('category_id', 9)->where('status', '!=', 1)->orderby('sub_categories.name', 'asc')->get();

        $category = Category::where('category_slug', '=', 'music')->first();

        // return $sub_categories;
        $videos = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
        // ->where('admin_videos.category_id', 'LIKE', "$sub_categories->id%")
            ->videoResponse()
            ->orderby('admin_videos.id', 'desc')
            ->get();
        // $videos = $this->get_movies_by_category($sub_categories->id);
        if ($videos->isNotEmpty()) {
            return view('guest.pages.music.index', compact('sub_categories', 'videos', 'category'));
        } else {
            return 'null';
        }
        // return view('guest.pages.tv-shows.index');

    }

    public function tv_shows()
    {

        $sub_categories = SubCategory::where('category_id', 10)->where('status', '!=', 1)->orderby('sub_categories.name', 'asc')->get();

        $category = Category::where('category_slug', '=', 'tv-shows')->first();

        // $sub_categories  = SubCategory::where('category_id', 10)->first();
        // return $sub_categories;
        $videos = AdminVideo::where('admin_videos.is_approved', 1)
            ->where('admin_videos.status', 1)
            ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
            ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
            ->leftJoin('genres', 'admin_videos.genre_id', '=', 'genres.id')
        // ->where('admin_videos.sub_category_id', 'LIKE', "$sub_categories->id%")
            ->videoResponse()
            ->orderby('admin_videos.id', 'desc')
            ->get();
        // $videos = $this->get_movies_by_category($sub_categories->id);
        if ($videos->isNotEmpty()) {
            return view('guest.pages.tv-shows.index', compact('sub_categories', 'videos', 'category'));
        } else {
            return 'null';
        }
        // return view('guest.pages.tv-shows.index');

    }
    public function channels()
    {

        $channels = Moderator::where('is_activated', 1)->get();
        $category = Category::where('category_slug', '=', 'channels')->first();
        return view('guest.pages.channel.index', compact('channels', 'category'));
    }
    public function single_channels(Request $request, $slug)
    {

        $channels = Moderator::where('is_activated', 1)->get();
        $data = Moderator::where('slug', '=', $slug)->firstOrFail();
        $channel_name = $data->name;
        $skip = 0;
        $take = 24;
        $videos = AdminVideo::where('admin_videos.is_approved', 1)
            ->leftjoin('categories', 'categories.id', '=', 'admin_videos.category_id')
            ->leftjoin('sub_categories', 'sub_categories.id', '=', 'admin_videos.sub_category_id')
            ->where('admin_videos.moderator_id', '=', $data->id)
            ->where('admin_videos.status', 1)
            ->where('categories.is_approved', 1)
            ->where('sub_categories.is_approved', 1)
            ->orderBy('admin_videos.created_at', 'desc')
            ->select('admin_videos.*', 'categories.name', 'sub_categories.name')->paginate(30);

        if ($request->ajax()) {
            return view('guest.pages.channel.detail-render')->with('videos', $videos)->render();
        }

        return view('guest.pages.channel.detail', compact('data', 'videos', 'channels', 'channel_name'));

    }

    // public function browse_list(Request $request) {
    //     $sub_title = $request->category;
    //     $sub_id = $request->id;
    //     $settings = Settings::get();
    //     $site_logo = '';
    //     foreach( $settings as $key=>$value) {
    //         if($value->key == 'site_logo') {
    //             $site_logo = $value->value;
    //         }
    //         if($value->key == 'ANGULAR_SITE_URL') {
    //             $streaming_url = $value->value;
    //         }
    //     };
    //     $sub_videos = AdminVideo::where('admin_videos.is_approved' , 1)
    //         ->where('admin_videos.status' , 1)
    //         ->where('admin_videos.sub_category_id' , 'LIKE', "%$sub_id%")
    //         ->orderby('admin_videos.id' , 'desc')->get();
    //     return view('guest.browse_list')->with('site_logo', $site_logo)->with('streaming_url', $streaming_url)->with('sub_title', $sub_title)->with('videos', $sub_videos);
    // }

    public function contact_us()
    {
        return view('guest.pages.contact-us');
    }

    public function about_us()
    {
        return view('guest.pages.about-us');
    }

    public function save_contact_us(Request $request)
    {
        //print_r($request->all());die;
        if (!Auth::user()) {
            $validator = Validator::make($request->all(), 
                [
                    'name' => 'required',
                    'email' => 'required|email',
                    'message' => 'required',
                    'g-recaptcha-response' => ['required', new ValidRecaptcha]
                ],
                [
                    'g-recaptcha-response.required' => 'Please ensure that you are a human!'
                ]
            );
        } else {
            $validator = Validator::make($request->all(), 
                [
                    'name' => 'required',
                    'email' => 'required|email',
                    'message' => 'required'
                ]
            );
        }

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $contact = new ContactUs;

        $contact->name = trim(strip_tags($request->name, '<br>'));
        $contact->email = trim(strip_tags($request->email, '<br>'));
        $contact->subject = trim(strip_tags($request->subject, '<br>'));
        $contact->message = trim(strip_tags($request->message, '<br>'));

        if ($contact->save()) {
            try {
                Mail::send('guest.email',
                array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'user_message' => $request->get('message')
                ), function($message)
                {
                    $message->from('webmaster@flashington.com');
                    $message->to('craigb@entangledweb.com', 'Admin')->subject('Feedback');
                });
            } catch (\Throwable $th) {
                //throw $th;
            }

            return back()->with('success', 'Thanks for contacting us! We will be back to you shortly.');
        } else {
            return back()->with('error', 'There is something went wrong.');
        }
    }
    public function userlogout(Request $request)
    {
        Auth::logout();
        return back();

    }

    public function static_pages(Request $request, $page)
    {
        $data = Page::where('page_slug', $page)->first();
        if (empty($data)) {
            return view('errors.404');
        }
        //print_r($data);die;
        if ($data) {
            return view('guest.pages.static-pages', compact('data'));
        } else {
            return view('error');
        }
    }

    public function header_search(Request $request)
    {
        //echo $request->get('query');die;
        if ($request->get('query') != null) {
            $query = $request->get('query');
            //$result = $this->search_result($request->get('query'));
            $category_data_str = '';
            $subcategory_data_str = '';
            $actor_data_str = '';
            $writer_data_str = '';
            $director_data_str = '';

            $category = Category::where('name', 'Like', '%' . $query . '%')->get();

            $category_data_id = array();
            if (!empty($category)) {
                foreach ($category as $reply) {
                    $category_data_id[] = $reply->id;
                }
                $category_data_str = implode("|", $category_data_id);
            }

            $sub_category_row = SubCategory::where('name', 'Like', '%' . $query . '%')->get();

            $subcategory_data_id = array();
            if (!empty($sub_category_row)) {
                foreach ($sub_category_row as $reply) {
                    $subcategory_data_id[] = $reply->id;
                }
                $subcategory_data_str = implode("|", $subcategory_data_id);
            }

            $actor_row = CastCrew::where('name', 'Like', '%' . $query . '%')->get();

            $actor_data_id = array();
            if (!empty($actor_row)) {
                foreach ($actor_row as $reply) {
                    $actor_data_id[] = $reply->id;
                }

                $actor_data_str = implode("|", $actor_data_id);
                $sub_categories = DB::table('sub_categories as sc')->select('sc.id as id', 'name')->join('admin_videos as av', 'sc.id', '=', 'av.sub_category_id')->where('av.actors', 'RLIKE', "[[:<:]]" . $actor_data_str . "[[:>:]]")->groupBy('sc.id')->get();

            }

            $writer_row = CastCrew::where('name', 'Like', '%' . $query . '%')->select('id')->get();

            $data_id = array();
            if (!empty($writer_row)) {
                foreach ($writer_row as $reply) {
                    $data_id[] = ($reply->id);
                }
            }
            $data_str = implode("|", $data_id);

            $director_row = CastCrew::where('name', 'Like', '%' . $query . '%')->get();

            $director_data_id = array();
            if (!empty($director_row)) {
                foreach ($director_row as $reply) {
                    $director_data_id[] = ($reply->id);
                }
            }
            $director_data_str = implode("|", $director_data_id);

            $sql = AdminVideo::where('admin_videos.is_approved', 1)
                ->where('admin_videos.status', 1)
                ->leftJoin('categories', 'admin_videos.category_id', '=', 'categories.id')
                ->leftJoin('sub_categories', 'admin_videos.sub_category_id', '=', 'sub_categories.id')
                ->where('admin_videos.title', 'LIKE', "%$query%")
                ->orWhere('admin_videos.description', 'LIKE', "%$query%")
                ->orWhere('admin_videos.details', 'LIKE', "%$query%")
                ->orWhere('admin_videos.alternative_titles', 'LIKE', "%$query%");

            if (!empty($category_data_str)) {
                $sql = $sql->orWhere('admin_videos.category_id', 'RLIKE', "[[:<:]]" . $category_data_str . "[[:>:]]");
            }

            if (!empty($subcategory_data_str)) {
                $sql = $sql->orWhere('admin_videos.sub_category_id', 'RLIKE', "[[:<:]]" . $subcategory_data_str . "[[:>:]]");
            }

            if (!empty($actor_data_str)) {
                $sql = $sql->orWhere('admin_videos.actors', 'RLIKE', "[[:<:]]" . $actor_data_str . "[[:>:]]");
            }

            if (!empty($director_data_str)) {
                $sql = $sql->orWhere('admin_videos.directors', 'RLIKE', "[[:<:]]" . $director_data_str . "[[:>:]]");
            }

            if (!empty($data_str)) {
                $sql = $sql->orWhere('admin_videos.writers', 'RLIKE', "[[:<:]]" . $data_str . "[[:>:]]");
            }

            $result = $sql->orderby('admin_videos.id', 'desc')->paginate(24);

            //print_r($result);die;
            $output = '';
            if (!empty($result)) {
                $output = '<ul class="dropdown-menu" style="display:block;">';
                foreach ($result as $row) {
                    //$output .= '<li><a href="'. url('movies/'.$row->unique_id) .'">' . $row->title . '</a></li>';

                    $output .= '<li><a href="' . url('movies/' . $row->unique_id) . '">
                        <div class="heade-search-img">
                            <img src="' . $row->default_image . '" alt="' . Setting::get('site_name') . '"/>
                        </div>
                        <div class="heade-search-title">
                            <h5>' . strip_tags($row->title) . '</h5>
                            <p>' . strip_tags($row->details) . '</p>
                        </div>
                        </a></li>';
                }
                $output .= '</ul>';
                echo $output;
            }
        }
    }

    public function save_review(Request $request)
    {
        //print_r($request->all());die;
        try {

            $validator = Validator::make($request->all(), [
                'subject' => 'required',
                'comment' => 'required',
                'spoilers' => 'required',
                //'g-recaptcha-response' => ['required', new ValidRecaptcha]
            ],
            [
                'g-recaptcha-response.required' => 'Please ensure that you are a human!'
            ]);


            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput($request->input());

            } else {

                $tab_name = $request->get('tab_name');
                $review_id = $request->get('review_id');
                // $model = new UserReview;

                // $model->user_id = Auth::user()->id;

                // $model->admin_video_id = $request->admin_video_id;
                // $model->rating = (!empty($request->rating)) ? $request->rating : 0;
                // $model->spoilers = ($request->spoilers == '1') ? 'YES' : 'NO';
                // $model->subject = trim(strip_tags($request->subject));
                // $model->comment = trim(strip_tags($request->comment));

                // $model->status = DEFAULT_TRUE; // By default it will be 1, future it may vary

                // // get and update calculate vote rating data
                // $admin_video = AdminVideo::find($model->admin_video_id);

                
                // $vote_count = (!empty($admin_video->vote_count)) ? ($admin_video->vote_count+1) : 1;
                // $vote_total = (!empty($admin_video->vote_rating_total)) ? ($admin_video->vote_rating_total + $model->rating) : $model->rating;

                if (!empty($review_id)) {
                    $model = UserReview::find($review_id);
                    $model->user_id = $model->user_id;
        
                    $model->admin_video_id = $model->admin_video_id;
                    $model->rating = $request->rating;
                    $model->spoilers = ($request->spoilers == '1') ? 'YES' : 'NO';
                    $model->subject = trim(strip_tags($request->subject));
                    $model->comment = trim(strip_tags($request->comment));
        
                    // get and update calculate vote rating data
                    $admin_video = AdminVideo::find($model->admin_video_id);
                    
                    $vote_count = (!empty($admin_video->vote_count)) ? ($admin_video->vote_count) : 1;
                    $vote_total = ($admin_video->vote_rating_total - $model->rating) + $request->rating;
        
                    $model->rating = $request->rating;
        
                } else {
                    $model = new UserReview;
                    
                    $old_rating = UserReview::where('admin_video_id', $admin_video_id)->where('user_id', $user_id)->first();
        
                    
                        $model->user_id = Auth::user()->id;
                        $model->admin_video_id = $request->admin_video_id;
                        $model->rating = (!empty($request->rating)) ? $request->rating : 0;
                        $model->spoilers = ($request->spoilers == '1') ? 'YES' : 'NO';
                        $model->subject = trim(strip_tags($request->subject));
                        $model->comment = trim(strip_tags($request->comment));
                    
                    
        
                    $model->status = DEFAULT_TRUE; // By default it will be 1, future it may vary
        
                    // get and update calculate vote rating data
                    $admin_video = AdminVideo::find($model->admin_video_id);
                    
                    
                    $vote_count = (!empty($admin_video->vote_count)) ? ($admin_video->vote_count+1) : 1;
                    $vote_total = (!empty($admin_video->vote_rating_total)) ? ($admin_video->vote_rating_total + $model->rating) : $model->rating;
                }

                $vote_average = number_format($vote_total/$vote_count, 2);
                $ratings = round($vote_total/$vote_count);

                if ($model->save()) {
                    
                    //update admin video table by vote_average, vote_count, vote_total, and ratings
                    AdminVideo::where('id', $model->admin_video_id)
                        ->update([
                            'vote_average' => $vote_average,
                            'vote_count' => $vote_count,
                            'vote_rating_total' => $vote_total,
                            'ratings' => $ratings
                        ]);

                    return back()->with('flash_success', tr('rating_create_success'))
                    ->with('tab_name', 'tab_name');
                } else {

                    return back()->with('flash_error', tr('rating_not_saving'))
                    ->with('tab_name', 'tab_name');

                }
            }
        } catch (Exception $e) {

            return back()->with('flash_error', $e->getMessage());

        }
    }

    public function reviews(Request $request, $slug, $video_id)
    {
        $rating = $request->get('rating');
        $sort_by = $request->get('sort');
        $spoilers = $request->get('spoilers');

        if (!empty($video_id)) {
            $videos = AdminVideo::select('title', 'details', 'unique_id', 'default_image', 'id')->where('id', $video_id)->first();

            // get reviews
            $query = UserReview::with('user')
                ->where('admin_video_id', $video_id);
            
            if (!empty($rating)) {
                $query = $query->where('rating', $rating);
            }

            if (!empty($spoilers)) {
                $query = $query->where('spoilers', 'NO');
            }

            if (!empty($sort_by)) {
                if ($sort_by == 'review') {
                    $query = $query->orderBy('rating', 'desc');
                }

                if ($sort_by == 'submitted') {
                    $query = $query->orderBy('created_at', 'desc');
                }
            }

            $user_reviews = $query->paginate(10);

            //echo '<pre>';
            //print_r($user_reviews);die;
            if ($request->ajax()) {
                return view('guest.pages.reviews.review-render')->with('user_reviews', $user_reviews)->render();
            } else {
                return view('guest.pages.reviews.index')->with('user_reviews', $user_reviews)->with('videos', $videos);
            }
        } else {
            return abort(404);
        }
    }

    public function rated(Request $request)
    {
        $user_id = Auth::user()->id;
        $review_id = $request->review_id;
        $rating = $request->rating;
        $admin_video_id = $request->admin_video_id;
        $subject = '';
        $comment = '';
        $spoilers = '';

        if (!empty($review_id)) {
            $model = UserReview::find($review_id);
            $model->user_id = $model->user_id;

            $model->admin_video_id = $model->admin_video_id;
            $model->rating = $model->rating;
            $model->spoilers = $model->spoilers;
            $model->subject = $model->subject;
            $model->comment = $model->comment;

            // get and update calculate vote rating data
            $admin_video = AdminVideo::find($model->admin_video_id);
            
            $vote_count = (!empty($admin_video->vote_count)) ? ($admin_video->vote_count) : 1;
            $vote_total = ($admin_video->vote_rating_total - $model->rating) + $request->rating;

            $model->rating = $request->rating;

        } else {
            $model = new UserReview;
            
            $old_rating = UserReview::where('admin_video_id', $admin_video_id)->where('user_id', $user_id)->first();

            
                $model->user_id = Auth::user()->id;
                $model->admin_video_id = $request->admin_video_id;
                $model->rating = (!empty($request->rating)) ? $request->rating : 0;
                $model->spoilers = $spoilers;
                $model->subject = $subject;
                $model->comment = $comment;
            
            

            $model->status = DEFAULT_TRUE; // By default it will be 1, future it may vary

            // get and update calculate vote rating data
            $admin_video = AdminVideo::find($model->admin_video_id);
            
            
            $vote_count = (!empty($admin_video->vote_count)) ? ($admin_video->vote_count+1) : 1;
            $vote_total = (!empty($admin_video->vote_rating_total)) ? ($admin_video->vote_rating_total + $model->rating) : $model->rating;
        }

        $vote_average = number_format($vote_total/$vote_count, 2);
        $ratings = round($vote_total/$vote_count);

        if ($model->save()) {
            
            //update admin video table by vote_average, vote_count, vote_total, and ratings
            AdminVideo::where('id', $model->admin_video_id)
                ->update([
                    'vote_average' => $vote_average,
                    'vote_count' => $vote_count,
                    'vote_rating_total' => $vote_total,
                    'ratings' => $ratings
                ]);

            echo $model->id;
        } else {
            echo tr('rating_not_saving');
        }
    }
}
