<?php
namespace App\Http\Controllers;

use App\AdminVideo;
use App\AdminVideoImage;
use App\CastCrew;
use App\Category;
use App\ContactUs;
use App\Helpers\Helper;
use App\Moderator;
use App\Page;
use App\SubCategory;
use App\UserReview;
use App\VideoCastCrew;
use App\Tag;
use App\AdminVideoTag;
use Auth;
use DB;
// use Validator;
use Exception;
use App\Rules\ValidRecaptcha;
use Illuminate\Support\Facades\Validator;
// use App\Jobs\NormalPushNotification;

use Illuminate\Http\Request;
use Setting;
use App\People;
use View;
use Response;
use App\User;
use Hash;
use App\Subscription;
use Illuminate\Support\Facades\Cookie;
use App\Subject;

class GuestController extends Controller
{

    public function index(Request $request)
    {
        $sliders = People::where('is_slider',1)->get();
        $totalCount = People::where('status',1);
        if(!empty($request->char)){
            $totalCount->where('name', 'LIKE', $request->char.'%');
        }
        $totalCount =  $totalCount->count();
        $celebs =  People::where('status',1);
       
        $celebs =  $celebs->simplePaginate(18);
        return view('guest.pages.home.home',['celebs'=>$celebs,'totalCount'=>$totalCount,'char'=>(isset($request['char'])?$request['char']:''),'sliders'=>$sliders]);
        
    }

    public function getCelebList(Request $request){
        
        $celebs =  People::where('status',1);
        if(!empty($request->search)){
            $celebs->Where('name', 'like', '%' . $request->search . '%');
        }
        if(!empty($request->char)){
            $celebs->where('name', 'LIKE', $request->char.'%');
        }
        if(!empty($request->order)){
            if($request->order == 'asc'){
            $celebs->orderBy('id', 'ASC');
            }else{
             $celebs->orderBy('id', 'DESC');
            }
        }
        $totalCount = $celebs->count();
        $celebs = $celebs->orderBy('id', 'ASC')->simplePaginate(18);
        $html = View::make('guest.pages.home._home_celeb_list', ['celebs'=>$celebs])->render();
        return Response::json(['success'=>true,'html' => $html,'currentCount'=>count($celebs),'totalCount'=>$totalCount]);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function loginUser(Request $request)
    {
        $basicValidator = Validator::make(
            $request->all(),
            array(
                'email' => 'required|max:255|email',
                'password' => 'required'
            )
        );
        $remember_me = $request->has('remember_me') ? true : false; 
        if($basicValidator->fails()) {
            return redirect()->back()->withErrors($basicValidator->errors())->withInput($request->except(["password",'confirm_password']));
        }else{
            $user = User::where('email',$request->email)->first();

            if(!empty($user) && !$user->is_verified){
                return back()->with('error', 'Sorry , Please verify email Id first.');
            }

            if(Auth::attempt(['email'=>$request->email , 'password'=>$request->password],$remember_me))
            {
               if($remember_me){
                setcookie('email', $request->email, 120);
                return redirect('/')->with('success', 'Login successfully.')->withCookie(cookie('email', $request->email,12000))->withCookie(cookie('password', $request->password,12000));
               }else{
                Cookie::make('email', '', '');
                return redirect('/')->with('success', 'Login successfully.')->withCookie(cookie('email', '',''))->withCookie(cookie('password', '',''));
               }
            }else{
                return back()->with('error', 'Invalid credential.')->withInput($request->except(["password",'confirm_password']));;
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        \Session::flush();
        return redirect('/')->with(['success'=>'Logout seccessfully.']);
    }

    public function signup()
    {
        return view('auth.register');
    }

    public function forgotPassword()
    {
        return view('auth.forgot-password');
    }

    public function resetPasswordMail(Request $request)
    {
        
        $basicValidator = Validator::make(
            $request->all(),
            array(
                'email' => 'required|max:255|email',
            )
        );
        $remember_me = $request->has('remember_me') ? true : false; 
        if($basicValidator->fails()) {
            return redirect()->back()->withErrors($basicValidator->errors())->withInput($request->except(["password",'confirm_password']));
        }else{
            
           $user = User::where('email',$request->email)->first();
            if($user)
            {
                $password = Helper::generateRandomString();
                $user->password = Hash::make($password);
                $user->save();
                $email_data = array();
                $email_data['email']  = $user->email;
                $email_data['password'] = $password;
                $email_data['template_type'] = FORGOT_PASSWORD;
                $page = "emails.forgot-password";
                Helper::send_email($page, $subject = null, $request->email, $email_data);

                return back()->with('success', 'Password has been sent to your email.');
            }else{
                return back()->with('error', 'Sorry, email does not exist in my record');
            }
        }
        
    }

    public function resetPasswordForm($token){
       $user = User::where('reset_password_token',$token)->first();
       if(!empty($user)){
        return view('auth.reset-password',['user'=>$user]);
       }else{
        return back()->with('error', 'Sorry,password reset token has been expired.');
       }
    }

    public function resetPassword(Request $request)
    {
        $basicValidator = Validator::make(
            $request->all(),
            array(
                'password' => 'required',
                'confirm_password' => 'required|same:password'
            )
        );
        $remember_me = $request->has('remember_me') ? true : false; 
        if($basicValidator->fails()) {
            return redirect()->back()->withErrors($basicValidator->errors())->withInput($request->except(["password",'confirm_password']));
        }else{
           $user = User::where('reset_password_token',$request->token)->first();
           $user->password = Hash::make($request->password);
           $user->reset_password_token = '';
           $user->save();
            if($user)
            {
                return redirect('/login')->with('success', 'Password reset successfully , please login.');
            }else{
                return back()->with('error', 'Sorry, Somthing went wrong.');
            }
        }
    }

    public function sendForgotMail(Request $request)
    {

        $email_data['name'] = $user->name;
        $email_data['password'] = $new_password;
        $email_data['email'] = $user->email;
        $email_data['template_type'] = ADMIN_USER_WELCOME;

        // $subject = tr('user_welcome_title').' '.Setting::get('site_name');
        $page = "emails.admin_user_welcome";
        $email = $user->email;
        Helper::send_email($page, $subject = null, $email, $email_data);
    }

    public function checkout()
    {
        return view('guest.pages.check-out');
    }

    public function signupUser(Request $request)
    {
        try{

            $basicValidator = Validator::make(
                $request->all(),
                array(
                    'email' => 'required|max:255|email|unique:users,email',
                    'password' => 'required',
                    'confirm_password'=>'required|same:password',
                    'name'=>'required',
                )
            );
            if($basicValidator->fails()) {
                return redirect()->back()->withErrors($basicValidator->errors())->withInput($request->except(["password",'confirm_password']));
            }else{
                $post = $request->all();
               
                $post['user_ip_address'] = $request->getClientIp(true);
                $code = Helper::generateRandomString();
                $post['password'] = Hash::make($request->password);
                $post['verification_code'] = $code;
                $user = User::create($post);
                if($user)
                {
                            $email_data = [];

                            $email_data['user_id'] = $user->id;

                            $email_data['verification_code'] = $code;

                            $email_data['template_type'] = USER_WELCOME;

                            $email = $user->email;
                            $page = "emails.welcome";
                            Helper::send_email($page,$subject = null,$email,$email_data);

                    return redirect('/login')->with('success', 'Signup successfully.');
                }else{
                    return back()->with('error', 'Invalid credential.');
                }
            }
       }catch(\Exceprion $ex){
            return back()->with('error', $ex->getMessage());
       }
    }

    public function subscription()
    {
        $trialPlan = Subscription::where('status',1)->where('title','Basic')->first();
        $standard = Subscription::where('status',1)->where('title','Standard')->first();
        $premium = Subscription::where('status',1)->where('title','Premium')->first();
        return view('guest.pages.subscription',['trialPlan' => $trialPlan ,'standard'=>$standard , 'premium'=>$premium]);
    }
                   
    public function contact_us()
    {
        $subjects = Subject::where('status',1)->get();
        return view('guest.pages.contact-us',['subjects'=>$subjects]);
    }

    public function about_us()
    {
        return view('guest.pages.about-us');
    }

    public function save_contact_us(Request $request)
    {
        if (!Auth::user()) {
            $validator = Validator::make($request->all(), 
                [
                    'name' => 'required',
                    'email' => 'required|email',
                    'message' => 'required',
                    'g-recaptcha-response' => ['required', new ValidRecaptcha]
                ],
                [
                    'g-recaptcha-response.required' => 'Please ensure that you are a human!'
                ]
            );
        } else {
            $validator = Validator::make($request->all(), 
                [
                    'name' => 'required',
                    'email' => 'required|email',
                    'message' => 'required'
                ]
            );
        }

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $contact = new ContactUs;

        $contact->name = trim(strip_tags($request->name, '<br>'));
        $contact->email = trim(strip_tags($request->email, '<br>'));
        $contact->subject = trim(strip_tags($request->subject, '<br>'));
        $contact->message = trim(strip_tags($request->message, '<br>'));

        if ($contact->save()) {
            try {
                Mail::send('guest.email',
                array(
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'user_message' => $request->get('message')
                ), function($message)
                {
                    $message->from('yunush.infowind@gmail.com');
                    $message->to('yunush.infowind@gmail.com', 'Admin')->subject('Feedback');
                });
            } catch (\Throwable $th) {
                //throw $th;
            }

            return back()->with('success', 'Thanks for contacting us! We will be back to you shortly.');
        } else {
            return back()->with('error', 'There is something went wrong.');
        }
    }
    public function static_pages(Request $request, $page)
    {
        $data = Page::where('page_slug', $page)->first();
        if (empty($data)) {
            return view('errors.404');
        }
        //print_r($data);die;
        if ($data) {
            return view('guest.pages.static-pages', compact('data'));
        } else {
            return view('error');
        }
    }

    public function cardList(){
        return view('guest.pages.card-list');
    }

    public function addCard(){
        return view('guest.pages.add-card');
    }

    public function invoice(){
        return view('guest.pages.invoice');
    }

    public function accountSetting(){
        return view('guest.pages.account-setting');
    }

    public function paymentSuccess(){
        return view('guest.pages.payment-success');
    }
    
    public function editProfile(){
        return view('guest.pages.edit-profile');
    }
    public function changePassword(){
        return view('guest.pages.change-password');
    }
}
