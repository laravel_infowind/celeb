<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
  
    protected $hidden = [
        'user_id', 'celeb_id',
    ];
}
