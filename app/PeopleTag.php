<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Setting;

use DB;

class PeopleTag extends Model
{
    public function people()
    {
        return $this->belongsTo('App\PeopleFile');
    }

    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }
}
