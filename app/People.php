<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    public $table = "peoples";

    protected $fillable = ['id', 'cast_id', 'name', 'biography', 'birthday', 'deathday', 'gender','known_for_department', 'imdb_id', 'place_of_birth', 'profile_image', 'also_known_as', 'popularity', 'homepage', 'cast_credits', 'crew_credits'];
    

    public function peopleFiles() {
        return $this->hasMany('App\PeopleFile');
    }

    public function videos()
    {
        return $this->hasMany('App\PeopleFile','people_id','id')->where('file_type',2);
    }

    public function images()
    {
        return $this->hasMany('App\PeopleFile','people_id','id')->where('file_type',1);
    }
}
