jQuery(document).ready(function($) {
    jQuery('#toggle-btn-filter').click(function (e) { 
        e.preventDefault();
        jQuery('.right-filter-dropdowns').toggleClass('d-flex');
        jQuery('body').toggleClass('open-filter-dropdowns');
    });
    jQuery('#toggle-btn-alft').click(function (e) { 
        e.preventDefault();
        jQuery('body').toggleClass('open-filter-celebrities');
    });
})
